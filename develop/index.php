<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Технические работы!</title>
  <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

  <style>
  *{
      font-family: 'PT Sans', sans-serif;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -moz-user-select: -moz-none;
      -ms-touch-action: none;
  }
  html {
      width: 100%;
      height: 100%;
      background: rgba(43, 0, 137, 0.2);
  }
  body {
      padding: 0;
      margin: 0;
      background-image: url('../template/dest/images/dsmed.png');
      background-position: center;
      background-repeat: no-repeat;
  }
  .flex-container {
      width: 100%;
      height: 100vh;
      display: flex;
      flex-direction: column;
      display: -webkit-flex;
      justify-content: center;
      -webkit-justify-content: center;
      align-items: center;
      -webkit-align-items: center;
  }
  .container{
      width: 333px;
      height: 333px;
  }
  .container *{
    text-align: center;
  }
  h2{
    padding-top: 68px;
    padding-left: 40px;
    color: white;
    text-shadow: 0px 0px 1px grey;
  }
  </style>
 </head>
 <body>
     <div class="flex-container">
         <div class="container">
             <h2>Технические работы!</h2>
         </div>
     </div>
 </body>
</html>
