module.exports = function(grunt) {

  grunt.initConfig({
      // Склеиваем файлы в один файл
    concat: {
        css:{
            src: [
                'template/src/css/*.css',
                'template/src/css/fontello/css/*.css',
            ],
            dest: 'template/tmp/css/dealer.css'
        },
        js:{
            src: [
                'template/src/js/*.js',
            ],
            dest: 'template/tmp/js/dealer.js'
        },
    },

    // Минификация файла
    uglify: {
        js: {
            files:{
                    'template/dest/js/dealer.min.js' : 'template/tmp/js/dealer.js'
            }
        }
    },
    cssmin: {
        css: {
            src: 'template/tmp/css/dealer.css',
            dest: 'template/dest/css/dealer.min.css'
        }
    },
    imagemin:{
        dynamic:{
            files: [{
                expand: true,
                cwd: 'template/src/images/',
                src: ['**/*.{png,jpg,gif,svg}'],
                dest: 'template/dest/images/',
            }]
        }
    },
    babel: {
        options: {
            sourceMap: false,
            presets: ['@babel/preset-env']
        },
        dist: {
            files: {
              'template/tmp/js/dealer.js': 'template/tmp/js/dealer.js'
            }
        }
    },
    watch: {
        files: [
            'vendor/twbs/bootstrap/dist/css/bootstrap.min.css',
            'template/src/css/*.css',
            'template/src/fontello/css/*.css',
            'vendor/twbs/bootstrap/dist/js/bootstrap.min.js',
            'template/src/js/*.js',
        ],
        tasks: [ 'concat', 'babel', 'uglify', 'cssmin'],
    },
  });

  require('load-grunt-tasks')(grunt);

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);
};
