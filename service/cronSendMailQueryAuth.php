<?
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('log_errors', 'On');
ini_set('error_log', 'log/php_errors.log');

CONST ROOT='/home/i/infods5i/dealer.dsmed.ru/public_html';
//CONST ROOT='/home/i/infods5i/infods5i.beget.tech/public_html';

require ROOT.'/vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Db {
	public static function getConnection () {
		$paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath);
		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=UTF8";
		$db = new PDO($dsn, $params['user'], $params['password']);
		return $db;
	}
}

/*
*
*/

$db = DB::getConnection();

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT id, addresses, theme, body_mail, file FROM dealer_send_mail WHERE type = 1 AND send = 1 LIMIT 10";
$result = $db->prepare($sql);
$result->execute();
$mailsParams = array();
while($row = $result->fetch()){
    $mailsParams[]=[
		'id' => intval($row['id']),
		'address' => $row['addresses'],
		'theme' => $row['theme'],
		'body_mail' => $row['body_mail'],
		'file' => $row['file']
	];
}
if(!empty($mailsParams)){
	foreach ( $mailsParams as $id => $mParams) {
		require_once ( ROOT.'/config/mail_config.php' );
		$mail = new PHPMailer(true);
		try {
			$mail->isSMTP();
			$mail->Host = $senders['beget']['host'];
			$mail->SMTPAuth = true;
			$mail->Username = $senders['beget']['user'];
			$mail->Password = $senders['beget']['pass'];
			$mail->SMTPSecure = 'ssl';
			$mail->Port = 465;
			$mail->CharSet = 'UTF-8';
			$mail->setFrom($senders['beget']['user'], 'Отдел продаж ООО "ДС-Мед"' );
			$mail->addReplyTo($senders['project']['user'] , 'Отдел продаж ООО "ДС-Мед"' );
			$mail->addAddress($mParams['address']);
			$mail->addBCC($senders['project']['user']);
			$mail->addBCC('bron@ds-med.ru');
			$mail->addBCC($senders['beget']['user']);
			$mail->isHTML(true);
			$mail->Subject = $mParams['theme'];
			$mail->Body = $mParams['body_mail'];
			$mail->AddAttachment($mParams['file']);
			$mail->send();
			try{
				$date = date( 'd.m.y H:i:s' );
				$db->beginTransaction();
				$sql = "UPDATE dealer_send_mail SET d_send = :date, send = 2 WHERE id = :id";
				$res = $db->prepare($sql);
				$res->bindParam(':id', $mParams['id'], PDO::PARAM_INT);
				$res->bindParam(':date', $date, PDO::PARAM_STR);
				$res->execute();
				$db->commit();
			} catch(Exception $e){
				$db->rollBack();
			}
		}catch(Exception $e){

		}
	}
}
else{
	echo 'Нечего отправлять. $mailsParams - пуст!';
}
