<?
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('ROOT',dirname(__FILE__));
require ROOT.'/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style;

class Db {
	public static function getConnection () {
		$paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath);
		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=UTF8";
		$db = new PDO($dsn, $params['user'], $params['password']);
		return $db;
	}
}

class Data {

	public static function getProjects () {
		$sql = "SELECT p.id, p.date, p.brend_name, p.ur_name, p.inn, p.comment
				,dd.dealer_brend_name,dd.dealer_ur_name,ddm.manager_id,ddm.manager_second_name,ddm.manager_name,ddm.manager_patronymic
				,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
				,(SELECT GROUP_CONCAT(dpt.id, ';',dpt.tools_id,';',dpt.status_id SEPARATOR '+')FROM dealer_projects_tools dpt WHERE dpt.project_id = p.id)AS brons_params
				FROM `dealer_projects` AS p
				INNER JOIN dealers_dealer AS dd ON (dd.id = p.dealer)
				INNER JOIN dealers_dealer_manager AS ddm ON (ddm.manager_id = p.dealer_manager)
				WHERE p.id in (SELECT project_id FROM dealer_projects_tools AS dpt WHERE dpt.status_id = 1)
				ORDER BY p.id DESC";
		$db = Db::getConnection ();
		$projectList=[];
		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		$result = $db->prepare($sql);
		$result->execute();
		while ($row = $result->fetch()){
			$projectList[] = [
				'p_id' => $row['id'],
				'date' => date ( 'd-m-Y', strtotime ( $row['date'] ) ),
				'time' => date ( 'H:i', strtotime ( $row['date'] ) ),
				'cl_ur_name' => str_replace(' (  )', '' , $row['brend_name'].' ('.$row['ur_name'].')'),
				'inn' => $row['inn'],
				'comment' => $row['comment'],
				'dealer' => $row['dealer_brend_name'].' ('.$row['dealer_ur_name'].')',
				'manager_id' => $row['manager_id'],
				'manager' => $row['manager_second_name'].' '.$row['manager_name'].' '.$row['manager_patronymic'],
				'address' =>  str_replace( ', корпус block_null', '', str_replace(', офис office_null', '', (str_replace('region_null, ', '', $row['address'])))),
				'brons_params' => $row['brons_params']
			];
		}

		foreach ($projectList as $key => $projectitem) {
			$brons_params = explode('+', $projectitem['brons_params']);
		 	foreach ($brons_params as $id => $bron_params) {
		 		$projectList[$key]['brons'][$id] = explode(';', $bron_params);
				$sql = "SELECT dt.tool_name, ds.id as status_id, ds.status
						FROM dealer_projects_tools AS dpt
						INNER JOIN dealer_status AS ds ON (ds.id=dpt.status_id)
						INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id)
						WHERE dpt.id = :id";
				$result = $db->prepare($sql);
				$result->bindParam(':id', $projectList[$key]['brons'][$id][0], PDO::PARAM_STR);
				$result->execute();
				while ($row = $result->fetch()){
					$projectList[$key]['brons'][$id][1] = $row['tool_name'];
					$projectList[$key]['brons'][$id][2] = $row['status_id'];
					$projectList[$key]['brons'][$id][3] = $row['status'];
				}
		 	}
		}
		return $projectList;
	}
}
class Mytable {

	public static function createXlsx ( $projectList ) {

		$Spreadsheet = new Spreadsheet();
		$Spreadsheet->getProperties()
				    ->setCreator("Polyakov Andrey")
				    ->setLastModifiedBy("Polyakov Andrey")
				    ->setTitle("Список просроченых авторизаций (броней)")
				    ->setSubject("Office 2007 XLSX")
				    ->setDescription("Система бронирования Дс-Мед")
				    ->setKeywords("Дс-Мед Ds-Med")
				    ->setCategory("Просроченые авторизации");
		$header_style = [
			'font' => [ 'bold' => true ],
			'alignment' => [
			    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
			'borders' => [
				'top' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
				'bottom' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
				'left' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
				'right' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
			],
			'fill' => [
			    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
			    'rotation' => 90,
			    'startColor' => [ 'argb' => '2b0089' ],
			    'endColor' => [ 'argb' => '2b0089' ],
			],
		];
		$all_style = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
			'borders' => [
				'top' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
				'bottom' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
				'left' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
				'right' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
			'font' => [
				'size' => 10,
			],
		];

		$Spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'ID')
					->setCellValue('B1', 'Дата')
					->setCellValue('C1', 'Дилер')
					->setCellValue('D1', 'Менеджер')
					->setCellValue('E1', 'Название клиники и ЮЛ')
					->setCellValue('F1', 'ИНН клиники и ЮЛ')
					->setCellValue('G1', 'Адрес')
					->setCellValue('H1', 'Комментарий')
					->setCellValue('I1', 'Оборудование')
					->setCellValue('J1', 'Статус');
		$Spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($header_style);
		$Spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->getColor()->applyFromArray( ['rgb' => 'ffffff'] );
		$i=2;
		foreach ( $projectList as $row => $data ) {
		    $Spreadsheet->setActiveSheetIndex(0)
    				->setCellValue( "A".$i, $data['p_id'] )
    				->setCellValue( "B".$i, $data['date'] )
    				->setCellValue( "C".$i, $data['dealer'] )
    				->setCellValue( "D".$i, $data['manager'] )
    				->setCellValue( "E".$i, $data['cl_ur_name'] )
    				->setCellValue( "F".$i, $data['inn'] )
    				->setCellValue( "G".$i, $data['address'] )
    				->setCellValue( "H".$i, $data['comment'] );
					$tool='';
					$status='';
					foreach ($data['brons'] as $id => $value) {
						$tool.=$value[1].';';
						$status.=$value[3].';';
					}
			$Spreadsheet->setActiveSheetIndex(0)
					->setCellValue("I".$i, $tool)
					->setCellValue("J".$i, $status);
			$Spreadsheet->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray($all_style);
			$i++;
		}
		// foreach( range ( 'A','J' ) as $columnID ) {
		// 	$Spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		// }
		$writer = new Xlsx($Spreadsheet);
		$writer->save(ROOT.'/uploads/all_103 - '.date('Y-m-d H:i:s').'.xlsx');
	}
}

$projectList = Data::getProjects();
$Spreadsheet = Mytable::createXlsx($projectList);
