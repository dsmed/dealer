<?
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('log_errors', 'On');
ini_set('error_log', 'log/php_errors.log');
CONST ROOT = '/home/i/infods5i/infods5i.beget.tech/public_html';
require '/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style;
CONST STATUS = 8;

class Db {
	public static function getConnection () {
		$paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath);
		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=UTF8";
		$db = new PDO($dsn, $params['user'], $params['password']);
		return $db;
	}
}

class Data {
	protected $mas_brons = array ();
	protected $bronList = array ();
	protected $status_id;
	protected $i = 2;
	public function __construct ( $status_id ) {
		$this->status_id = $status_id;
	}

	public function getAuthorizations () {
		$sql = "SELECT pt.id
					   ,p.date
					   ,d.dealer_brend_name
					   ,d.dealer_ur_name
					   ,(SELECT GROUP_CONCAT(dm.manager_second_name, ' ', dm.manager_name, ' ', IFNULL(dm.manager_patronymic, 'null_patronymic')) FROM dealers_dealer_manager dm WHERE dm.manager_id = p.dealer_manager) AS manager
					   ,p.brend_name
					   ,p.ur_name
					   ,p.inn
					   ,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
					   ,p.comment
					   ,t.tool_name AS tool
					   ,s.status AS status
					   ,p.dealer AS project_user
				FROM dealer_projects_tools AS pt
				INNER JOIN dealer_projects AS p ON (p.id = pt.project_id)
				INNER JOIN dealers_dealer AS d ON (d.id = p.dealer)
				INNER JOIN dealer_tools AS t ON (t.id = pt.tools_id)
				INNER JOIN dealer_status AS s ON (s.id = pt.status_id)
				WHERE pt.status_id = :status_id
				GROUP BY pt.id
				ORDER BY pt.id DESC";
		$db = Db::getConnection ();
		$result = $db->prepare($sql);
		$result->bindParam(':status_id', $this->status_id , PDO::PARAM_INT);
		$result->execute();

		while ( $row = $result->fetch() ) {
				$this->mas_brons [] = $row;
			}

		foreach ($this->mas_brons as $key => $value) {
			$this->bronList [$this->i] = [
				'id' => $value['id'],
				'date' => $value['date'],
				'diler' => $value['dealer_brend_name'].' '.$value['dealer_ur_name'],
				'manager' => trim ( str_replace ( ' null_patronymic', '', $value['manager'] ) ),
				'clinic_full_name' => $value['brend_name'].' '.$value['ur_name'],
				'inn' => $value['inn'],
				'address' => trim ( str_replace ( ', корпус block_null', '', str_replace ( ', офис office_null', '', ( str_replace( 'region_null, ', '', $value['address'] ) ) ) ) ),
				'comments' => ( $value['comment'] ? $value['comment'] : 'отсутствует' ),
				'tool' => $value['tool'],
				'status' => $value['status']
			];
			$this->i ++;
		}
		return $this->bronList;
	}

	public function printer ( ) {
		print_r ( $this->bronList );
	}
}

class Table {

	public static function createXlsx ( $brons ) {

		$Spreadsheet = new Spreadsheet();
		$Spreadsheet->getProperties()
				    ->setCreator("Polyakov Andrey")
				    ->setLastModifiedBy("Polyakov Andrey")
				    ->setTitle("Список просроченых авторизаций (броней)")
				    ->setSubject("Office 2007 XLSX")
				    ->setDescription("Система бронирования Дс-Мед")
				    ->setKeywords("Дс-Мед Ds-Med")
				    ->setCategory("Просроченые авторизации");
		$header_style = [
			'font' => [ 'bold' => true ],
			'alignment' => [
			    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
			'borders' => [
				'top' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
				'bottom' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
				'left' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
				'right' => [
			        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			    ],
			],
			'fill' => [
			    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
			    'rotation' => 90,
			    'startColor' => [ 'argb' => '2b0089' ],
			    'endColor' => [ 'argb' => '2b0089' ],
			],
		];
		$all_style = [
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			],
			'borders' => [
				'top' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
				'bottom' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
				'left' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
				'right' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
				],
			],
			'font' => [
				'size' => 10,
			],
		];

		$Spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'ID')
					->setCellValue('B1', 'Дата')
					->setCellValue('C1', 'Дилер')
					->setCellValue('D1', 'Менеджер')
					->setCellValue('E1', 'Название клиники и ЮЛ')
					->setCellValue('F1', 'ИНН клиники и ЮЛ')
					->setCellValue('G1', 'Адрес')
					->setCellValue('H1', 'Комментарий')
					->setCellValue('I1', 'Оборудование')
					->setCellValue('J1', 'Статус');
		$Spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($header_style);
		$Spreadsheet->getActiveSheet()->getStyle('A1:J1')->getFont()->getColor()->applyFromArray( ['rgb' => 'ffffff'] );
		foreach ( $brons as $row => $data ) {
		    $Spreadsheet->setActiveSheetIndex(0)
    				->setCellValue( "A".$row, $data['id'] )
    				->setCellValue( "B".$row, $data['date'] )
    				->setCellValue( "C".$row, $data['diler'] )
    				->setCellValue( "D".$row, $data['manager'] )
    				->setCellValue( "E".$row, $data['clinic_full_name'] )
    				->setCellValue( "F".$row, $data['inn'] )
    				->setCellValue( "G".$row, $data['address'] )
    				->setCellValue( "H".$row, $data['comments'] )
    				->setCellValue( "I".$row, $data['tool'] )
    				->setCellValue( "J".$row, $data['status'] );
			$Spreadsheet->getActiveSheet()->getStyle('A'.$row.':J'.$row)->applyFromArray($all_style);
		}
		foreach( range ( 'A','J' ) as $columnID ) {
			$Spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}
		$writer = new Xlsx($Spreadsheet);
		$writer->save(ROOT.'/uploads/report_by_status - '.date('Y-m-d H:i:s').'.xlsx');
	}
}
$objBrons = new Data ( STATUS );
$brons = $objBrons->getAuthorizations ();
$Spreadsheet = new Table ();
$Spreadsheet->createXlsx ( $brons );
