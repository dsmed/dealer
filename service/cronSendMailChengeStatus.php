<?
ini_set('error_reporting',E_ALL);
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
ini_set('log_errors','On');
ini_set('error_log','log/php_errors.log');
CONST ROOT='/home/i/infods5i/dealer.dsmed.ru/public_html';
require ROOT.'/vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;

class Db{
	public static function getConnection(){
		$paramsPath=ROOT.'/config/db_params.php';
		$params=include($paramsPath);
		$dsn="mysql:host={$params['host']};dbname={$params['dbname']};charset=UTF8";
		$db=new PDO($dsn,$params['user'],$params['password']);
		return $db;
	}
}
class Data{
    public static function getMailParams(){
        $db=Db::getConnection();
        $sql="SELECT id,manager_id,body_mail FROM dealer_send_mail WHERE type=2 AND send=1 LIMIT 10";
        $result=$db->prepare($sql);
        $result->execute();
        $mailsParams=array();
        while ($row=$result->fetch()){
            $mailsParams[]=[
                'id'=>$row['id'],
                'manager_id'=>$row['manager_id'],
                'body_mail'=>$row['body_mail']
            ];
        }
        return $mailsParams;
    }
    public static function getManagerMail($managerId){
        $db=Db::getConnection();
        $sql="SELECT manager_email FROM dealers_dealer_manager WHERE manager_id=:manager_id";
        $result=$db->prepare($sql);
        $result->bindParam(':manager_id',$managerId,PDO::PARAM_INT);
        $result->execute();
        return $result->fetch()['manager_email'];
    }
}

$mailsParams=Data::getMailParams();
$date=date('d-m-Y H:i:s');
if(!empty($mailsParams)){
    foreach ($mailsParams as $id=>$mailParams) {
        $email=Data::getManagerMail($mailParams['manager_id']);
        try {
            require_once(ROOT.'/config/mail_config.php');
            $mail=new PHPMailer(true);
            $mail->isSMTP();
            $mail->Host=$senders['opt']['host'];
            $mail->SMTPAuth=true;
            $mail->Username=$senders['opt']['user'];
            $mail->Password=$senders['opt']['pass'];
            $mail->SMTPSecure='ssl';
            $mail->Port=465;
            $mail->CharSet='UTF-8';
            $mail->setFrom($senders['opt']['user'],'Отдел по работе с дилерами ООО "ДС-Мед"');
            $mail->addReplyTo($senders['opt']['user'],'Отдел по работе с дилерами ООО "ДС-Мед"');
            $mail->addAddress($senders['opt']['user']);
            $mail->addAddress($email);
            $mail->addBCC($senders['beget']['user']);
            $mail->isHTML(true);
            $mail->Subject='Смена статуса';
            $mail->Body=$mailParams['body_mail'];
            $mail->send();
            error_log("[$date][CRON-sendMailByDealer]Письмо успешно отправлено на адрес:".$email."\n",3,ROOT."/log/cron_send_mail.log");
            echo "[CRON-STATUS-SM][$date]Письмо успешно отправлено на адрес:".$email."\n";
            try {
                $db=Db::getConnection();
                $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $db->beginTransaction();
                $sql="UPDATE dealer_send_mail SET send=2,d_send=:date WHERE id=:id";
                $result=$db->prepare($sql);
                $result->bindParam(':date',$date,PDO::PARAM_INT);
                $result->bindParam(':id',$mailParams['id'],PDO::PARAM_INT);
                $result->execute();
                $db->commit();
                error_log("[$date][CRON-sendMailByDealer] Успешно установлен флаг отправки письма у записи №".$mailParams['id']."\n",3,ROOT."/log/cron_send_mail.log");
                echo "[CRON-STATUS-DB][$date]Успешно установлен флаг отправки письма у записи №".$mailParams['id']."\n";
            }catch(Exception $e){
                $db->rollBack();
                error_log("[$date][CRON-sendMailByDealer] Флаг отправки письма не изменён! Запись №".$mailParams['id']."Доп. инф.:".$e->getMessage()."\n",3,ROOT."/log/cron_send_mail.log");
                echo "[CRON-STATUS-DB][$date]Флаг отправки письма не изменён! Запись №".$mailParams['id']."Доп. инф.:".$e->getMessage()."\n";
            }
        }catch(Exception $e){
            error_log("[$date][CRON-sendMailByDealer]На почтовый адрес: ".$email." письмо не отправлено. PHPMailerERROR: ".$mail->ErrorInfo."\n",3,ROOT."/log/cron_send_mail.log");
            echo "[$date][CRON-sendMailByDealer]На почтовый адрес: ".$email." письмо не отправлено. PHPMailerERROR: ".$mail->ErrorInfo."\n";
        }
    }
}else{
    echo "Список для отправки писем пуст!\n";
}
