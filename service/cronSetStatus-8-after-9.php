<?
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('log_errors', 'On');
ini_set('error_log', 'log/php_errors.log');

CONST ROOT = '/home/i/infods5i/dealer.dsmed.ru/public_html';
// Test
//CONST ROOT = '/home/i/infods5i/infods5i.beget.tech/public_html';

class Db {
	public static function getConnection () {
		$paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath);
		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=UTF8";
		$db = new PDO($dsn, $params['user'], $params['password']);
		return $db;
	}
}

$db = DB::getConnection();
$brons = array ();

# 9 - Авторизация просрочена

$STATUS = 8;
$statusComment = 'Статус установлен автоматически.';
$success_c = 0;
$fail_c = 0;
$all_c = 0;

$now = new DateTime();
$commentDate = $now->format('d.m.Y H:i:s');

# Выбираем все авторизации со статусом "Авторизовано" и вытаскиваем дату когда он был установлен

# Пояснение: Вся информация об изменении статуса авторизации хранится в таблице dealer_status_comments
# и для того чтобы выбрать из неё тот, статус который был установлен последним последним, сначала берём максимальный id
# всех изменений статусов у запрашиваемой брони, а затем по этому id берём дату.

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT dpt.id AS bronId, dpt.project_id AS projectId, dpt.status_id AS statusId, dp.type,
			   dp.brend_name AS brendName, dp.ur_name AS urName, dp.inn,
			   dd.dealer_rights AS dealerRights, dd.dealer_brend_name AS dealerBrend, dd.dealer_ur_name AS dealerName,
			   ddm.manager_id AS managerId, ddm.manager_name AS name, ddm.manager_patronymic AS patronymic, ddm.manager_email AS managerEmail,
			   (SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = dp.id) AS address,
			   dt.tool_name AS toolName,
			   dsc2.comment_date AS commentDate
		FROM dealer_projects_tools AS dpt
		INNER JOIN ( SELECT MAX(dsc.id) AS com_id, dsc.tools_id
    				FROM dealer_status_comments dsc
    				GROUP BY dsc.tools_id )
				   	dsc ON ( dsc.tools_id = dpt.id )
		INNER JOIN ( SELECT dsc2.id, dsc2.tools_id, dsc2.comment_date
		       		FROM dealer_status_comments dsc2)
		   			dsc2 ON ( dsc2.id =  dsc.com_id )
		INNER JOIN dealer_projects AS dp ON ( dp.id = dpt.project_id )
		INNER JOIN dealers_dealer_manager AS ddm ON ( ddm.manager_id = dp.dealer_manager )
		INNER JOIN dealers_dealer AS dd ON ( dd.id = dp.dealer )
		INNER JOIN dealer_tools AS dt ON ( dt.id = dpt.tools_id )
		WHERE dpt.status_id = 9
		ORDER BY dpt.id";

$result = $db->prepare($sql);
$result->execute();

while ( $row = $result->fetch() ) {
    $brons [] = [
		'bronId' => intval ( $row['bronId'] ),
		'projectId' => intval ( $row['projectId'] ),
		'statusId' => intval ( $row['statusId'] ),
		'brendName' => $row['brendName'],
		'urName' => $row['urName'],
		'inn' => $row['inn'],
		'type' => intval ( $row['type'] ),
		'dealerRights' => intval ( $row['dealerRights'] ),
		'dealerBrend' => $row['dealerBrend'],
		'dealerName' => $row['dealerName'],
		'managerId' => intval ( $row['managerId'] ),
		'name' => $row['name'],
		'patronymic' => $row['patronymic'],
		'managerEmail' => $row['managerEmail'],
		'address' => $row['address'],
		'toolName' => $row['toolName'],
		'commentDate' => $row['commentDate'],
	];

}

$displayInfo = '';

foreach ( $brons as $id => $bron ) {

	$update = new DateTime ( $bron['commentDate'] );

	$interval = intval ( $update->diff($now)->format('%a') );

	$bodyMail = '';

	if ( $interval > 10 ) {

		$displayInfo  = ' Авторизация №' . $bron['bronId'] . ' из проекта №' . $bron['projectId'] . ' со статусом №'  . $bron['statusId'];
		$displayInfo .= '. Дата последнего изменения / установлена: [' . $update->format( 'd.m.Y H:i:s' ) . '] - [' . $commentDate;
		$displayInfo .= "]. Установленн статус - Авторизация просрочена (№" . $STATUS .")\n";

		try {

			$db->beginTransaction();

			$sql = "UPDATE dealer_projects_tools SET status_id = :statusId WHERE id = :bronId";
			$result = $db->prepare($sql);
			$result->bindParam(':statusId', $STATUS, PDO::PARAM_INT);
			$result->bindParam(':bronId', $bron['bronId'], PDO::PARAM_INT);
			$result->execute();

			$sql = "INSERT INTO dealer_status_comments ( tools_id, status_id, comment, comment_date )
			 			   VALUES ( :bronId, :statusId, :statusComment, :commentDate)";
			$result = $db->prepare($sql);
			$result->bindParam(':bronId',$bron['bronId'], PDO::PARAM_INT);
			$result->bindParam(':statusId', $STATUS, PDO::PARAM_INT);
			$result->bindParam(':statusComment', $statusComment, PDO::PARAM_STR);
			$result->bindParam(':commentDate', $commentDate, PDO::PARAM_STR);
			$result->execute();

			error_log( "[CRON_CHANGE_STATUS] - отработал успешно (TRUE):  [".date("d.m.Y")."-".date("H:i:s")." ] " . $displayInfo . "\n", 3, ROOT."/log/cron_success.log");

			$bron['address'] = str_replace ( ' корпус block_null,', '', $bron['address']);
			$bron['address'] = str_replace ( ' офис office_null', '', $bron['address']);
			$bron['address'] = str_replace ( 'region_null, ', '', $bron['address']);
			$bron['address'] = str_replace ( ' , ', '', $bron['address']);

			$bodyMail  = 'Уважаемый(ая) ' . ucfirst( $bron['name'] ) . ' ' . ( ! empty( $bron['patronymic'] ) ? ucfirst( $bron['patronymic'] ) : '' ). '!<br>';
			$bodyMail .= 'По проекту ' . $bron['brendName'] . ' ('. $bron['brendName'] .'), ИНН ' . $bron['inn'] . ', по адресу(ам): ' . $bron['address'] . ' у следующего оборудования изменился статус:<br>';
			$bodyMail .=  $bron['toolName'] . ' — Авторизация просрочена.<br>';
			$bodyMail .= '---<br>По всем вопросам и пожеланиям просим обращаться:<br>Антон Брагинец<br>к.э.н., руководитель отдела по работе с дилерами ООО "ДС-Мед"<br>+7(495)230-11-55, +7(926)637-11-55,<br>opt@ds-med.ru<br>';

			try {

				$TYPE = 2;
				$THEME = 'Смена статуса';
				$FLAGSEND = 1;
				$VISIBILITY = 1;
				$FILE = false;

				$sql = "INSERT INTO dealer_send_mail(type, d_create, manager_id, addresses, theme, body_mail, file, send, visibility)
						VALUES (:type, :dcreate, :managerId, :addresses, :theme, :bodymail, :file, :flagsend, :visibility)";
				$result = $db->prepare($sql);
				$result->bindParam(':type', $TYPE, PDO::PARAM_INT);
				$result->bindParam(':dcreate', $commentDate, PDO::PARAM_STR);
				$result->bindParam(':managerId', $bron['managerId'], PDO::PARAM_INT);
				$result->bindParam(':addresses', $bron['managerEmail'], PDO::PARAM_STR);
				$result->bindParam(':theme',$THEME, PDO::PARAM_STR);
				$result->bindParam(':bodymail', $bodyMail, PDO::PARAM_STR);
				$result->bindParam(':file', $FILE, PDO::PARAM_STR);
				$result->bindParam(':flagsend', $FLAGSEND, PDO::PARAM_INT);
				$result->bindParam(':visibility', $VISIBILITY, PDO::PARAM_INT);

				$result->execute();

				$db->commit();

				$success_c ++;

			} catch ( Exception $e ) {

				error_log( "[CRON_CHANGE_STATUS] - отработал не корректно (FALSE): [".date("d.m.Y")."-".date("H:i:s")." ] Добавление письма в таблицу DEALER_SEND_MAIL. Доп. информация об ошибке: ".$e->getMessage()."\n", 3, ROOT."/log/cron_errors.log");

			}

		} catch ( Exception $e ) {

			$db->rollBack();

			error_log( "[CRON_CHANGE_STATUS] - отработал не корректно (FALSE): [".date("d.m.Y")."-".date("H:i:s")." ] Необходимо проверить связи брони №  - ".$bron['statusId']." Доп. информация об ошибке: ".$e->getMessage()."\n", 3, ROOT."/log/cron_errors.log");

		}

	}

	else $fail_c ++;

	$all_c ++;

}

echo $displayInfo;

echo "Общее число обработанных авторизаций $all_c из них успешно изменено: $success_c, не изменены $fail_c.";

error_log( "[CRON_CHANGE_STATUS] Отработал : [ " . $commentDate . " ] Всего обработано авторизаций: " . $all_c . ". Из них успешно обработано: " . $success_c . ". Из них завершились ошибкой: " . $fail_c . ".\n", 3, ROOT."/log/cron_work.log");
