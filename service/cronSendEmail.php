<?
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('log_errors', 'On');
ini_set('error_log', 'log/php_errors.log');
CONST ROOT='/home/i/infods5i/dealer.dsmed.ru/public_html';
require ROOT.'/vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Db {
	public static function getConnection () {
		$paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath);
		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=UTF8";
		$db = new PDO($dsn, $params['user'], $params['password']);
		return $db;
	}
}

/*
* Описнаниче переменных:
* $mas_brons - массив, который хранит все брони, со статусами: Авторизовано и Авторизация продлена
* $mas_status - массив, который
* $STATUS - переменная, которая хранит id статуса ( 9 - Запрос актуальности )
*/

$mas_brons = array ();
$mas_status = array ();
$STATUS = 9;
$countChangeBron = $countChangeBronTrue = $countChangeBronFalse = 0;
$db = DB::getConnection();

/*
* Получаем все брони с статусами 2 и 7 ( авторизовано и авторизация продлена) и все данные по брони.
*/

$sql = "SELECT pt.id
		,(SELECT GROUP_CONCAT(dm.manager_name, ' ', IFNULL(dm.manager_patronymic, 'null_patronymic')) FROM dealers_dealer_manager dm WHERE dm.manager_id = p.dealer_manager) AS manager
		,(SELECT dm.manager_email FROM dealers_dealer_manager dm WHERE dm.manager_id = p.dealer_manager) AS manager_email
		,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
		,p.brend_name
		,p.ur_name
		,t.tool_name AS tool
		,s.id AS status_id
		,s.status AS status
	FROM dealer_projects_tools AS pt
	INNER JOIN dealer_projects AS p ON (p.id = pt.project_id)
	INNER JOIN dealers_dealer AS d ON (d.id = p.dealer)
	INNER JOIN dealer_tools AS t ON (t.id = pt.tools_id)
	INNER JOIN dealer_status AS s ON (s.id = pt.status_id)
	WHERE status_id = 2 OR status_id = 7";
$result = $db->prepare($sql);
$result->execute();

while ( $row = $result->fetch() ) {
        $mas_brons [] = $row;
    }

foreach ( $mas_brons as $id => $bron ) {

	// Выбираем все изменения статусов.
	$sql = "SELECT * FROM dealer_status_comments WHERE tools_id = :bron_id ORDER BY id DESC";
	$result = $db->prepare($sql);
	$result->bindParam(':bron_id', $bron['id'], PDO::PARAM_INT);
	if (  $result->execute() ) {
		// Забираем статус, который был установлен последним.
		$mas_status [0] = $result->fetch();
	}

	$now_date_hash = md5 ( date ( 'Y-m-d' ) );

	// Отправляем письмо, если текущая дата равна дате отправки письма
	if ( $now_date_hash === $mas_status[0]['date_send_mail_hash'] ) {

		$text = 'Здравствуйте, '.str_replace( ' null_patronymic', '', $bron['manager'] ).'! За вашей компанией было авторизовано оборудование '.$bron['tool'].' по проекту: '
		.$bron['brend_name'].' ( ЮЛ - '.$bron['ur_name'].' ), '.str_replace( ', корпус block_null', '', str_replace ( ', офис office_null', '', ( str_replace ( 'region_null, ', '', $bron['address'] ) ) ) ).'. Просим
		вас предоставить актуальную информацию по проекту.<br><br>-----<br>
		<table style="width: auto; background-color: #f5f5f5;" border="0" width="100%" cellspacing="7" cellpadding="0">
			<tbody>
				<tr>
					<td>
						<table style="width: 740px; background-color: #f5f5f5; float: left;" border="0" cellspacing="2" cellpadding="0">
							<tbody>
								<tr style="height: 130px;">
									<td style="width: 127px; height: 125px;"><img style="border: none; width: 110px; height: 110px; display: block; margin-left: auto; margin-right: auto;" src="https://portfoliomaxbal.files.wordpress.com/2018/07/110x110_braginets.png" alt="Braginets" /></td>
									<td style="width: 31px; height: 125px;">&nbsp;</td>
									<td style="width: 311px; height: 125px;">
										<table style="width: 277px;" border="0" cellspacing="0" cellpadding="2">
											<tbody>
												<tr style="height: 34px;">
													<td style="width: 412px; height: 34px;"><h2 style="font-family: "Arial",sans-serif; font-size: 22px; margin-bottom: 7px;"><strong><a style="border: none; text-decoration: none; color: #000000;" title="Ответить на email" href="mailto:opt@ds-med.ru">Антон Брагинец</a></strong></h2></td>
												</tr>
												<tr style="height: 11px;">
													<td style="color: #696969; font-family: Arial, sans-serif; font-size: 12px; margin-top: 10px; width: 412px; height: 11px;">к.э.н., руководитель отдела по работе с дилерами ООО "ДС-Мед"</td>
												</tr>
												<tr style="height: 25px;">
													<td style="color: #000000; font-size: 12px; width: 412px; height: 25px;">Тел.: +7 495 230-11-55<br /> <span style="color: #000000; font-size: 12px;">Моб: +7 926 637-11-55</span></td>
												</tr>
											</tbody>
										</table>
										</td>
										<td style="width: 251px;">
											<table style="width: 251px; margin-left: auto; margin-right: auto;" border="0" cellspacing="0" cellpadding="0">
												<tbody>
													<tr style="height: 62px;">
														<td style="color: #696969; font-family: Arial, sans-serif; font-size: 11px; margin-top: 0px; height: 62px; width: 247px; text-align: center;"><a style="background-attachment: scroll; background-clip: border-box; background-color: #bfe6ff; background-image: none; background-origin: padding-box; background-position-x: 0%; background-position-y: 0%; background-repeat: repeat; background-size: auto; box-sizing: border-box; color: #23527c; font-family: Verdana,Arial,Helvetica,sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; letter-spacing: normal; orphans: 2; outline-color: invert; outline-style: none; outline-width: 0px; text-align: center; text-indent: 0px; text-transform: none; -webkit-text-stroke-width: 0px; white-space: normal; word-spacing: 0px;" title="Перейти на сайт" href="http://ds-med.ru" target="_blank" rel="noopener"><img style="border: none; width: 140px; height: 39px; float: left;" src="https://portfoliomaxbal.files.wordpress.com/2018/07/logo1.png" alt="Logo_DSMED" align="center" /></a></td>
													</tr>
													<tr style="height: 32px;">
														<td style="height: 32px; width: 247px; font-family: Arial, sans-serif; margin-top: 10px; color: #696969; font-size: 12px;">авторизованный поставщик на российский рынок медицинского диагностического оборудования ведущих мировых брендов.</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>';

		require_once ( ROOT.'/config/mail_config.php' );

		$mail = new PHPMailer ( true );

		try {
			$mail->isSMTP();
			$mail->SMTPDebug  =  2 ;
			$mail->Host = $senders['beget']['host'];
			$mail->SMTPAuth = true;
			$mail->Username = $senders['beget']['user'];
			$mail->Password = $senders['beget']['pass'];
			$mail->SMTPSecure = 'ssl';
			$mail->Port = 465;
			$mail->CharSet = 'UTF-8';
			$mail->setFrom ( $senders['beget']['user'], 'Отдел по работе с дилерами ООО "ДС-Мед"' );
			$mail->addReplyTo( $senders['opt']['user'] , 'Отдел по работе с дилерами ООО "ДС-Мед"' );
			$mail->addAddress ( $senders['opt']['user'] );
			$mail->addAddress( $bron['manager_email'] );
			$mail->addBCC ( $senders['beget']['user'] );
			$mail->isHTML(true);
			$mail->Subject = 'Предоставление данных об авторизации';
			$mail->Body = $text;
			$mail->send ();

			/*
			* $comment_date - текущая дата комментария (системного)
			* $date_send_mail_hash - перезаписываем хэш даты, т.к. эта дата устанавливается при проверке даты отправки
			* $date_change_status_hash - создаём заново. Смена статуса происходит через три дня после уведомления дилера и если менеджер компании ничего не предпринял.
			* $STATUS - переменная, которая хранит id статуса. Добавляем запись об изменении статуса с его сохранением.
			*/

			try {
				$db->beginTransaction();
				$sql = "UPDATE dealer_projects_tools SET status_id = :statusId WHERE id = :id";
				$res = $db->prepare($sql);
				$res->bindParam(':statusId', $STATUS, PDO::PARAM_INT);
				$res->bindParam(':id', $bron['id'], PDO::PARAM_INT);
				$res->execute();

				$statusComment = 'Статус установлен автоматически, по истечению 30 дней бездействия. Отправленно email дилеру.';
				$comment_date = date('Y-m-d H:i:s');
				$date_send_mail_hash = 'no date to send mail';
				$date_change_status_hash = $mas_status[0]['date_change_status_hash'];

				$sql = "INSERT INTO dealer_status_comments(tools_id, status_id, comment, comment_date, date_send_mail_hash, date_change_status_hash)
				VALUES (:toolsId, :statusId, :statusComment, :comment_date, :date_send_mail_hash, :date_change_status_hash)";
				$result = $db->prepare($sql);
				$result->bindParam(':toolsId', $mas_status[0]['tools_id'], PDO::PARAM_INT);
				$result->bindParam(':statusId', $STATUS, PDO::PARAM_INT);
				$result->bindParam(':statusComment', $statusComment, PDO::PARAM_STR);
				$result->bindParam(':comment_date', $comment_date, PDO::PARAM_STR);
				$result->bindParam(':date_send_mail_hash', $date_send_mail_hash, PDO::PARAM_STR);
				$result->bindParam(':date_change_status_hash', $date_change_status_hash, PDO::PARAM_STR);
				$result->execute();

				$db->commit();
				error_log( "[CRON_SEND_MAIL] - отработал успешно (TRUE): [ ".date("m.d.y")."-".date("H:i:s")." ] У брони №: ".$bron['id']." сменился статус на: ".$STATUS." в таблице DEALER_PROJECTS_TOOLS. В таблице DEALER_STATUS_COMMENTS к брони № ".$mas_status[0]['tools_id']." внесена запись об изменении статуса.\n Бронь: ".$mas_status[0]['tools_id']."\n Статус: ".$STATUS."\n Комментарий: ".$statusComment."\n Дата комментария: ".$comment_date."\n Новый хэш уведомления/информация: ".$date_send_mail_hash."\n Хэш смены статуса: ".$date_change_status_hash."\n", 3, ROOT."/log/cron_success.log");
				$countChangeBronTrue ++;
			} catch ( Exception $e ) {
				$db->rollBack();
				error_log( "[CRON_SEND_MAIL] - отработал не корректно (FALSE): [ ".date("m.d.y")."-".date("H:i:s")." ] Необходимо проверить связи брони №  - ".$mas_status[0]['tools_id']." Доп. информация об ошибке: ".$e->getMessage()."\n", 3, ROOT."/log/cron_errors.log");
				$countChangeBronFalse ++;
			}
		} catch (  Exception $e  ) {
			error_log( "[CRON_SEND_MAIL] - отработал не корректно (FALSE): [ ".date("m.d.y")."-".date("H:i:s")." ]\n[CRON_SEND_MAIL] Ошибка отправки почты! В Стутус не изменён!\nПисьмо(а) не отправлено(ы):\nБронь №: ".$bron['id']."\nPHPMailerERROR: ".$mail->ErrorInfo."\n\n", 3, ROOT."/log/cron_errors.log");
			$countChangeBronFalse ++;
		}
	}
	$countChangeBron ++;
}
error_log( "[CRON_SEND_MAIL] Отработал : [ ".date("m.d.y")."-".date("H:i:s")." ] Всего обработано броней: ".$countChangeBron.". Из них успешно обработано: ".$countChangeBronTrue.". Из них завершились ошибкой: ".$countChangeBronFalse.".\n", 3, ROOT."/log/cron_work.log");
