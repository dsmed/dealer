
(function($){
	$(function(){
		"use strict";

		var popup = {
			sUrl:'',
			result: '',
			getVal:function(elements){
				var res = [], i = 0;
				for(i;i<elements.length;i++){
					res[i] = $(elements[i]).val();
				}
				this.result=res;
		 	},
		 	post:function(ps){
				$.post(this.sUrl,{ps:ps},function(data){
					// console.log('Popup send post');
					$('.pagination').hide();
					$('.projects-container').html(data);
					$('.loader-container').fadeOut(300,function(){$('section.projects-container').ready().ajaxCompvare();});
				});
		 	},
			post_export:function(ps){
				$.post(this.sUrl,{ps:ps},function(data){

					window.open(`/project/exportdownload/${data}`);

				});
			},
			send:function( typePost ){
				this.getVal($('select.search-required'));
				var required = this.result;
				this.getVal($('div.d-block select.search-select'));
				var fields = this.result;
				var searchStr = $('input[name="search-input"]').val();
				var ps = [required,fields,searchStr];
				switch ( typePost ) {
					case 'post':
							this.post(ps);
						break;
					case 'post_export':
							this.post_export(ps);
						break;
					default:
						this.post(ps);
				}

		 	}
		};
		$('select[name="project-or-tools"]').on('change',function(){
			// console.log('project-or-tools - change');
			switch(this.value){
				case 'project':
					if($('select[name="project-params"]').val()==='manager'){
						$('input[name="search-input"]').val('').prop("disabled",true);
						$('.manager-block').addClass('d-block').removeClass('d-none');
					}
					else{$('input[name="search-input"]').prop("disabled",false);}
					$('.project-block').addClass('d-block').removeClass('d-none');
					$('.toolblock-block').removeClass('d-block').addClass('d-none');
					$('.tool-block').removeClass('d-block').addClass('d-none');
					$('.status-block').removeClass('d-block').addClass('d-none');
					break;
				case 'toolblock':
					$('input[name="search-input"]').val('').prop("disabled",true);
					$('.toolblock-block').addClass('d-block').removeClass('d-none');
					$('.tool-block').addClass('d-block').removeClass('d-none');
					$('.status-block').addClass('d-block').removeClass('d-none');
					$('.project-block').removeClass('d-block').addClass('d-none');
					$('.manager-block').removeClass('d-block').addClass('d-none');

					break;
				default:
					$('input[name="search-input"]').prop("disabled",false);
					$('.toolblock-block').removeClass('d-block').addClass('d-none');
					$('.tool-block').removeClass('d-block').addClass('d-none');
					$('.status-block').removeClass('d-block').addClass('d-none');
					$('.project-block').removeClass('d-block').addClass('d-none');
					$('.manager-block').removeClass('d-block').addClass('d-none');
					break;
			}
		});
		$('select[name="project-params"]').on('change',function(){
			// console.log('project-params - change');
			if(this.value==='manager'){
				$('input[name="search-input"]').val('').prop("disabled",true);
				$('.manager-block').addClass('d-block').removeClass('d-none');
			}else{
				$('input[name="search-input"]').prop("disabled",false);
				$('.manager-block').removeClass('d-block').addClass('d-none');
			}
		});
		$('select[name="toolblock-params"]').on('change',function(){
			// console.log('toolblock-params - change');
			if($('select[name="toolblock-params"]').val()!=='none'){
				var toolblock=this.value;
				var url='/project/searchgetparams/';
				$.post(url,{toolblock:toolblock},function(data){
					$('select[name="tool-params"]').html(data);
				});
			}else{
				$('select[name="tool-params"]').html('<option value="none" selected>Выберите тип оборудования</option>');
			}

		});
		$('button#search').on('click',function(){
			var by=$('select[name="project-or-tools"]').val();
			if(by==='project'||by==='toolblock'){
				if(by==='project'){
					if($('select[name="project-params"]').val()==='none'){
						alert('Необходимо выбрать поле для поиска!');
					} else {
						popup.sUrl = '/project/searchAjax';
						popup.send();
						$('#search-modal').modal('hide');
					}
				} else {
					popup.sUrl = '/project/searchAjax';
					popup.send();
					$('#search-modal').modal('hide');
				}
			}else{alert('Вы не выбрали "Где искать"!');}
		});

		// ЭКСПОРТ ТАБЛИЦЫ -->

		$('a#exportxlsx').on('click',function(){
			var by=$('select[name="project-or-tools"]').val();
			if(by==='project'||by==='toolblock'){
				if(by==='project'){
					if($('select[name="project-params"]').val()==='none'){
						alert('Необходимо выбрать поле для поиска!');
					} else {
						popup.sUrl = 'project/exportsearchtable';
						popup.send('post_export');
					}
				} else {
					popup.sUrl = 'project/exportsearchtable';
					popup.send('post_export');
				}
			}else{alert('Вы не выбрали "Где искать"!');}
		});

    });
})(jQuery);
