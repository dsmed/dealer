function convertNewLinesToBr ( str ) {

    "use strict";

    return str.replace(/(?:\r\n|\r|\n)/g, '<br>');

}
function getComments(btnId, flagCount){
    "use strict";
    if($('div#project-'+btnId+' div.comment-block').length<1){
        let brons=$(`.brons-container[data-projectId=${btnId}]`);
        for (let i=0;i<brons.length;i++){
            (function(i){
                $.post('/project/getComment',{bron_id:$(brons[i]).attr('data-brone'),flagCount:flagCount},function(data){
                    $(brons[i]).after(data);
                });
            })(i);
        }
    }
}

(function($) {
	$(function() {

        "use strict";

        const authorizations = new Authorizations();

        let project, projectId;

        // БЛОК СОБЫТИЙ - РАБОТА С АВТОРИЗАЦИЯМИ -->

        // Выбор свех авторизаций в проекте

        $(document).on('click','input.check-all',function(){

            projectId = Number( $(this).attr('data-check-all') );

            let checked = $(this).prop('checked'),
                inputs = $(`#project-${projectId} input.input-bron`);

            for ( let i=0; i<inputs.length; i++ ) {
                $(inputs[i]).prop('checked',checked);
            }

            if ( checked ) $(`button#btn-change-${projectId}`).prop('disabled',false);
            else $(`button#btn-change-${projectId}`).prop('disabled',true);

        });

        // Клик по блоку с параметрами авторизации

        $(document).on('click','.brons-container',function(e){

            projectId = Number( $(this).attr('data-projectId') );

            let bronId = $(this).attr('data-brone'),
                checked = $(`input[type="checkbox"]#input-bron-${bronId}`).prop('checked');

            if ( e.target.tagName === 'DIV' ) $(`input[type="checkbox"]#input-bron-${bronId}`).prop('checked',!checked);
            if ( e.target.tagName === 'LABLE' ) {
                e.stopPropagation();
                e.preventDefault();
            }

            if ( !checked) $(`button#btn-change-${projectId}`).prop('disabled',false);

            let checkBrons = $(`.brons-container[data-projectId=${projectId}] input[type="checkbox"]:checked`);

            if ( checkBrons.length > 0 ) $(`button#btn-change-${projectId}`).prop('disabled',false);
            else {
                $(`input[data-check-all="${projectId}"]`).prop('checked', false);
                $(`button#btn-change-${projectId}`).prop('disabled',true);
            }

        });

        // Сброс значений полей всплавающего окна

        $(document).on('click','.btn-change-status',function(){
            $('select[name="select-status"]').val(0);
            $('textarea[name="status-comment"]').val('');
            $('.change-brons-by-project').html(projectId);
        });

        // Сбор и отправка данных на сервер

        $('.btn-change-status-apply').on('click',function(){

            authorizations.resetAll();

            let checkBrons = $(`#project-${projectId} input.input-bron:checked`),
                statusId = Number( $('select[name="select-status"]').val() ),
                comment = $('textarea[name="status-comment"]').val(),
                url = '/project/changestatus/';

            authorizations.setProjectId ( projectId );
            authorizations.setStatusId ( statusId );
            authorizations.setComment ( comment );

            for ( let i=0; i<checkBrons.length; i++ ) {
                authorizations.addAuthorizations (
                    Number( $(checkBrons[i]).attr('data-check-brone') ) );
            }

            $.post(url,{authorizations:authorizations.getAllJSON()},function(data){
                $(`#btn-change-${projectId}`).prop('disabled', true);
                //$(`button.btn-change-status-apply`).prop('disabled', true);
                $(`#project-${projectId} input.check-all`).prop('checked', false);
                $(`#project-${projectId} .alert`).remove();
                $(`#project-${projectId} .brons-container`).remove();
                $(`#project-${projectId} .brons-header`).after(data);
            });

        });

        // БЛОК СОБЫТИЙ - РАБОТА С АВТОРИЗАЦИЯМИ <--

        // БЛОК СОБЫТИЙ - ВЫВОД КОММЕНТАРИЕВ -->

        $(document).on('click', '.btn-get-comment', function(){
            getComments($(this).attr('data-btn-id'), "one");
        });
        $(document).on('click', '.btn-get-all-comments', function(){
            getComments($(this).attr('data-btn-id'), "all");
        });

        // БЛОК СОБЫТИЙ - ВЫВОД КОММЕНТАРИЕВ <--

        // БЛОК СОБЫТИЙ - СМЕНА ОТВЕТСТВЕННОГО -->

        $(document).on('click', '.apply-responsible', function(){
            projectId = $(this).attr('data-applay-responsible');
            let responsible = $(`#project-${projectId} select[name="select-responsible"]`).val(),
                self = $(this).parent().parent();
            $.ajax({
                url: '/project/changeresponsible',
                type: 'post',
                data: {projectId:projectId, responsible:responsible},
                success: function(data){
                    $(self).remove();
                    $(`#project-${projectId} .pc-ec-comment`).after(data);
                    $(`#project-${projectId} a.responsible__text`).attr('id',`responsible__text-${projectId}`).attr('data-applay-responsible',projectId);
                },
                error: function(data){
                    $(`#project-${projectId} .pc-ec-comment`).after(data.responseText);
                }
            });
        });

        // Смена ответственного после назанчения

        $(document).on('click', '.responsible__text', function(){
            projectId = $(this).attr('data-applay-responsible');
            $('#change-responsible #modal-title-change-responsible').attr('data-project', projectId);
            $('#change-responsible #modal-title-change-responsible #mtcr-span').html(projectId);
        });

        // Модальное окно

        $(document).on('click', '.apply-change-responsible', function(){
            projectId = $('#change-responsible #modal-title-change-responsible').attr('data-project');
            let responsible = $(`#modal-select-change-responsible`).val(),
                self = $(`#responsible__text-${projectId}`).parent();
            $.ajax({
                url: '/project/changeresponsible',
                type: 'post',
                data: {projectId:projectId, responsible:responsible},
                success: function(data){
                    $(self).remove();
                    $(`#project-${projectId} .pc-ec-comment`).after(data);
                    $(`#project-${projectId} a.responsible__text`).attr('id',`responsible__text-${projectId}`).attr('data-applay-responsible',projectId);
                },
                error: function(data){
                    $(`#project-${projectId} .pc-ec-comment`).after(data.responseText);
                }
            });
        });

        // БЛОК СОБЫТИЙ - СМЕНА МЕНЕДЖЕРА ПРОЕКТА <--

        // Смена ответственного после назанчения

        $(document).on('click', '.manager__text', function(){
            projectId = $(this).attr('data-change-project-manager');
            $('#change-project-manager #modal-title-change-project-manager').attr('data-project', projectId);
            $('#change-project-manager #modal-title-change-project-manager #mtcr-span').html(projectId);
        });

        // Модальное окно

        $(document).on('click', '.apply-change-project-manager', function(){
            projectId = $('#change-project-manager #modal-title-change-project-manager').attr('data-project');
            let selected_new_manager = $(`#modal-select-change-project-manager`).val(),
                self = $(`#manager__text-${projectId}`).parent();
            $.ajax({
                url: '/project/changeprojectmanager',
                type: 'post',
                data: {projectId:projectId, selected_new_manager:selected_new_manager},
                success: function(data){
                    $(self).remove();
                    $(`#project-${projectId} .pc-ec-dealer-name`).after(data);
                    $(`#project-${projectId} a.manager__text`).attr('id',`manager__text-${projectId}`).attr('data-change-project-manager',projectId)
                        .parent().attr('id', `dm-${projectId}`);
                },
                error: function(data){
                    $(`#project-${projectId} .pc-ec-comment`).after(data.responseText);
                }
            });
        });

        // БЛОК СОБЫТИЙ - СМЕНА МЕНЕДЖЕРА ПРОЕКТА <--

        // БЛОК СОБЫТИЙ - ATTENTIONS -->

        $(document).on('click', '.attentions', function(){

            $.post('project/getattentions', {'attention-type': this.value}, function(data) {
                $('.pagination').hide();
                $('.projects-container').html(data);
            });
        });

        $(document).on('click', '.myattentions', function(){
            $.post('/project/getmyattentions', {'attention-type': this.value, 'managerid': $(this).attr('data-manager-id')}, function(data) {
                $('.projects-container').html(data);
            });
        });

        // БЛОК СОБЫТИЙ - ATTENTIONS <--

        // БЛОК СОБЫТИЙ - RESPONSIBLE -->

        $(document).on('click', '.myresponsible', function(){
            $.post('/project/getmyresponsible', {'responsible-type': this.value, 'responsibleid': $(this).attr('data-responsible-id')}, function(data) {
                $('.projects-container').html(data);
            });
        });

        $(document).on('change', '.project-responsible-status', function(){
            $.post('/project/getmyresponsible', {'responsible-type': this.value, 'responsibleid': $(this).attr('data-responsible-id')}, function(data) {
                $('.projects-container').html(data);
            });
        });
        // БЛОК СОБЫТИЙ - RESPONSIBLE <--

		$('body').on('click', '.btn-query-auth', function(){
			projectId = {
                projectId: Number( $(this).attr('data-btn-id') ),
            }
            $.post('/project/beforeQueryAuth', {projectId:JSON.stringify(projectId)}, function(data){
                console.log('1');
                project = JSON.parse(data);
                let addresses = project.project.address.split(';'), c = 1, addressesHtml = '';
                for (let address of addresses) {
                    addressesHtml = addressesHtml + `<span class="qac-mail__item">${c++}. ${address};</span><br>`;
                }
                for (let receiver of project.receivers){
                    let tools = '', i = 1;
                    for (let tool of receiver.tools) {
                        tools = tools + `<span class="qac-mail__item">${i++}. ${tool};</span><br>`;
                    }
                    console.log('2');
                    let html = `
                        <div class="qa-container p-3 mb-3 rounded" data-qa-from="${receiver.id}">
                            <div id="accordion-${receiver.id}">
                                <div class="card mb-2">
                                    <div class="card-header" id="heading-${receiver.id}">
                                        <div class="btn btn-link btn-block" data-toggle="collapse" data-target="#collapse-${receiver.id}" aria-expanded="true" aria-controls="collapse-${receiver.id}">
                                            Редактировать письмо
                                        </div>
                                        <div id="collapse-${receiver.id}" class="collapse tools-block-content" aria-labelledby="heading-${receiver.id}" data-parent="#accordion-${receiver.id}">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label class="sr-only" for="from-send-mail-${receiver.id}">Кому</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">Кому:</div>
                                                        </div>
                                                        <input type="text" class="form-control" id="from-send-mail-${receiver.id}-" name="from-send-mail-${receiver.id}" value="${receiver.email}"  data-id="${receiver.id}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="sr-only" for="theme-send-mail-${receiver.id}">Тема</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">Тема:</div>
                                                        </div>
                                                        <input type="text" class="form-control" id="theme-send-mail-${receiver.id}" name="theme-send-mail-${receiver.id}" value="Запрос авторизации"  data-id="${receiver.id}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="sr-only" for="header-send-mail-${receiver.id}">Заголовок</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">Заголовок:</div>
                                                        </div>
                                                        <input type="text" class="form-control header-send-mail" id="header-send-mail-${receiver.id}" name="header-send-mail-${receiver.id}" value="Здравствуйте, ${receiver.fio}!"  data-id="${receiver.id}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="sr-only" for="comment-send-mail-${receiver.id}">Комментарий</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">Комментарий:</div>
                                                        </div>
                                                        <textarea class="form-control comment-send-mail" id="comment-send-mail-${receiver.id}" name="comment-send-mail-${receiver.id}" data-id="${receiver.id}"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="sr-only" for="footer-send-mail-${receiver.id}">Подпись</label>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text">Подпись:</div>
                                                        </div>
                                                        <textarea class="form-control footer-send-mail" id="footer-send-mail-${receiver.id}" name="footer-send-mail-${receiver.id}" data-id="${receiver.id}" style="display:block;min-height:170px;">---\n${project.signature.title}\n${project.signature.author}\n${project.signature.post}\n${project.signature.numbers}\n${project.signature.emails}\n</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="result-mail-${receiver.id}" class="alert alert-dark use-select-none" role="alert">
                                    <p class="receiver-name m-0">
                                        <span class="qac-mail__item">Здравствуйте, ${receiver.fio}!</span>
                                    </p>
                                    <p class="after-receiver-name m-0">
                                        <span class="qac-mail__item">Внесите, пожалуйста, в реестр проектов поставку следующих позиций оборудования:</span>
                                    </p>
                                    <p class="qac-mail-receiver-tool m-0">
                                        ${tools}
                                    </p>
                                    <p class="after-receiver-tools m-0">
                                        <span class="qac-mail__item">Оборудование планируется к поставке в ${project.project.brend_name} (${project.project.ur_name}) по адресу(ам):</span>
                                    </p>
                                    <p class="qac-mail-project-address m-0">
                                        ${addressesHtml}
                                    </p>
                                    <p class="qac-mail-project-inn m-0">
                                        <span class="qac-mail__item">ИНН: ${project.project.inn}</span>
                                    </p>
                                    <p class="qac-mail-comment m-0 m-b-1">
                                        <span class="qac-mail__item qac-mail-comment-warning"></span>
                                        <span class="qac-mail__item qac-mail-comment-content"></span>
                                    </p>
                                    <p class="qac-mail-signature">
                                        <span class="qac-mail__item">
                                            ---<br>
                                            ${project.signature.title}<br>
                                            ${project.signature.author}<br>
                                            ${project.signature.post}<br>
                                            ${project.signature.numbers}<br>
                                            ${project.signature.emails}<br>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>`;

                        console.log(html);

                    if($('#modal-body-query-auth .form-group').length===0){
                        $('#modal-body-query-auth').html(html);
                    }else{
                        $('#modal-body-query-auth').append(html);
                    }

                }

                if($('#modal-body-query-auth .form-group').length !== 0){
                    $('button[name="modal-query-auth"]').prop('disabled',false);
                }else{
                    $('button[name="modal-query-auth"]').prop('disabled',true);
                }
            });

		});

        $('body').on('hidden.bs.modal', '#modal-query-auth', function(){
          $('#modal-body-query-auth').html('');
        })

        $('.modal-body').on('keyup','input.header-send-mail',function(){
			let id = $(this).attr('data-id');
			$('#result-mail-'+id+' p.receiver-name>span').html(convertNewLinesToBr($(this).val()));
		});

		$('.modal-body').on('keyup','textarea.footer-send-mail',function(){
			let id = $(this).attr('data-id');
			$('#result-mail-'+id+' p.qac-mail-signature>span').html(convertNewLinesToBr($(this).val()));
		});
		$('.modal-body').on('keyup','textarea.comment-send-mail',function(){
			let id = $(this).attr('data-id');
			if($(this).val().length>0){
				if($('#result-mail-'+id+' p.qac-mail-comment>span.qac-mail-comment-warning').text().length===0){
					$('#result-mail-'+id+' p.qac-mail-comment>span.qac-mail-comment-warning').html('ВНИМАНИЕ: ');
				}
			}else{
				$('#result-mail-'+id+' p.qac-mail-comment>span.qac-mail-comment-warning').text('');
			}
			$('#result-mail-'+id+' p.qac-mail-comment>span.qac-mail-comment-content').html(convertNewLinesToBr($(this).val()));
		});

        $('button[name="modal-query-auth"]').on('click', function(){
            let qac = $('#modal-body-query-auth .qa-container'), mails = [];
            if(qac.length !== 0){
                for (let i = 0; i < qac.length; i++) {
                    let rId = +$(qac[i]).attr('data-qa-from');
                    let comment = ($(`textarea[name="comment-send-mail-${rId}"]`).val()).length>0?'ВНИМАНИЕ: ' + $(`textarea[name="comment-send-mail-${rId}"]`).val():'';
                    mails[i]={
                        id:rId,
                        theme: $(`input[name="theme-send-mail-${rId}"]`).val(),
                        header: $(`input[name="header-send-mail-${rId}"]`).val(),
                        comment: convertNewLinesToBr(comment),
                        signature: convertNewLinesToBr($(`textarea[name="footer-send-mail-${rId}"]`).val())
                    }
                }
                $.post('/project/queryauth', {mails:JSON.stringify(mails),projectId: projectId.projectId}, function(data){

                });
            }
        });

	});
})(jQuery);
