(function($){
	$(function(){
		"use strict";

		function hasClass(element, className) {
		    var rx = new RegExp('(?:^| )' + className + '(?: |$)');
		    return rx.test(element.className);
		}

		var validate = {
			// Проверка поля "Название клиники"
			nameClient : function(){
				$(this).val($(this).val().replace(/([^a-zа-яё\d\- ])*/ig,''));
				if($(this).val().length<3){
					$(this).removeClass('validate').addClass('invalidate');
				}else{
					$(this).removeClass('invalidate').addClass('validate');
				}
			},
			// Проверка поля "ИНН"
			inn : function(){
				var val = this.value.replace(/[^0-9]/g,'').substr(0,12);
				if(val.length<10){
					$(this).removeClass('validate').addClass('invalidate');
				}else{
					$(this).removeClass('invalidate').addClass('validate');
				}
				if(val.length<=11){
					this.value=val;
					$(this).inputmask('999 999 999 9');
				}else{
					this.value=val;
					$(this).inputmask('999 999 999 999');
				}
			},
			// Проверка поля "Наименование юр. лица"
			urName : function(){
				$(this).val($(this).val().replace(/([^А-яё0-9 "'-\.])*/ig,''));
				if($(this).val().length<3){
					$(this).removeClass('validate').addClass('invalidate');
				}else{
					$(this).removeClass('invalidate').addClass('validate');
				}
			},
			region : function(){
				$(this).val($(this).val().replace(/([^А-яё -])*/ig,''));
				if($(this).val().length<3){
					$(this).removeClass('validate');
				}else{
					$(this).addClass('validate');
				}
			},
			// Проверка полей регион, город, улица
			address : function(){
				$(this).val($(this).val().replace(/([^А-яё0-9 -])*/ig,''));
				if($(this).val().length<3){
					$(this).removeClass('validate').addClass('invalidate');
				}else{
					$(this).removeClass('invalidate').addClass('validate');
				}
			},
			// Проверка полей дом
			dom : function(){
				$(this).val($(this).val().replace(/([^А-яё0-9\/])*/ig,''));
				if($(this).val().length<1){
					$(this).removeClass('validate').addClass('invalidate');
				}else{
					$(this).removeClass('invalidate').addClass('validate');
				}
			},
			dob : function(){
				$(this).val($(this).val().replace(/([^А-яё0-9])*/ig,''));
				if($(this).val().length<1){
					$(this).removeClass('validate');
				}else{
					$(this).addClass('validate');
				}
			},
			comment : function(){
				$(this).val($(this).val().replace(/([^A-ZА-яё0-9\-., @\n\/:])*/ig,''));
				if($(this).val().length<1){
					$(this).removeClass('validate');
				}else{
					$(this).addClass('validate');
				}
			},
			fio : function(){
				$(this).val($(this).val().replace(/([^А-яё-])*/ig,''));
				if($(this).val().length<2){
					$(this).removeClass('validate').addClass('invalidate');
				}else{
					$(this).removeClass('invalidate').addClass('validate');
				}
			},
			pat : function(){
				$(this).val($(this).val().replace(/([^А-яё-])*/ig,''));
				if($(this).val().length<2){
					$(this).removeClass('validate');
				}else{
					$(this).addClass('validate');
				}
			},
			fonNumber : function(){
				var val=this.value.replace(/[^0-9]/g,'').substr(0,11);
				if(val.length<11){
					$(this).removeClass('validate').addClass('invalidate');
				}else{
					$(this).removeClass('invalidate').addClass('validate');
				}
				this.value=val;
				$(this).inputmask("+9(999) 999-9999");
			},
			email : function(){
				var val = this.value.replace(/[^0-9a-z@]/ig,'');
				if(val.length>7 && /@/.test(val)){
					$(this).removeClass('invalidate').addClass('validate');
				}else{
					$(this).removeClass('validate').addClass('invalidate');
				}
			}
		};

		var url, manager_id;
		// Инициализация валидации для страницы "Добавить проект" >>
		// CLINICS-NAME

		$('#infotmation').modal('show');


		if($('body #clinics-name').length>0){
			if($('body #clinics-name').val().length>0){
				$('body #clinics-name').each(validate.nameClient);
			}
		}
		$(document).on('keyup','#clinics-name',validate.nameClient);
		// INN
		if($('body #inn').length>0){
			if($('body #inn').val().length>0){
				$('body #inn').inputmask('999 999 999 999');
				$('body #inn').each(validate.inn);
			}
		}
		$(document).on('focus','#inn',function(){
			$('body #inn').inputmask('999 999 999 999');
		});
		$(document).on('focusout','#inn',function(){
			$(this).inputmask('remove');
			$(this).each(validate.inn);
		});
		// UR_NAME
		if($('body .ur-name').length>0){
			if($('body .ur-name').val().length>0){
				$('body .ur-name').each(validate.urName);
			}
		}
		$(document).on('keyup','.ur-name',validate.urName);
		// REGION
		if($('body .region').length>0){
			if($('body .region').val().length>0){
				$('body .region').each(validate.region);
			}
		}
		$(document).on('keyup focus','.region',validate.region);
		// CITY
		if($('body .city').length>0){
			if($('body .city').val().length>0){
				$('body .city').each(validate.address);
			}
		}
		$(document).on('keyup','.city',validate.address);
		// STREET
		if($('body .street').length>0){
			if($('body .street').val().length>0){
				$('body .street').each(validate.address);
			}
		}
		$(document).on('keyup','.street',validate.address);
		// HOUSE
		if($('body .house').length>0){
			if($('body .house').val().length>0){
				$('body .house').each(validate.dom);
			}
		}
		$(document).on('keyup','.house',validate.dom);
		// OFFICE
		if($('body .office').length>0){
			if($('body .office').val().length>0){
				$('body .office').each(validate.dob);
			}
		}
		$(document).on('keyup','.office',validate.dob);
		// COMMENT
		if($('body #comment').length>0){
			if($('body #comment').val().length>0){
				$('body #comment').each(validate.comment);
			}
		}
		$(document).on('keyup','#comment',validate.comment);
		// SECOND NAME
		if($('body .fam').length>0){
			if($('body .fam').val().length>0){
				$('body .fam').each(validate.fio);
			}
		}
		$(document).on('keyup','.fam',validate.fio);
		// NAME
		if($('body .name').length>0){
			if($('body .name').val().length>0){
				$('body .name').each(validate.fio);
			}
		}
		$(document).on('keyup','.name',validate.fio);
		// PATRONUMIC
		if($('body .pat').length>0){
			if($('body .pat').val().length>0){
				$('body .pat').each(validate.pat);
			}
		}
		$(document).on('keyup','.pat',validate.pat);
		// PHONE
		if($('body .phone').length>0){
			if($('body .phone').val().length>0){
				$('body .phone').each(validate.fonNumber);
			}
		}
		$('body .phone').inputmask("+9(999) 999-9999");
		$(document).on('focusout','.phone',function(){
			$(this).inputmask('remove');
			$(this).each(validate.fonNumber);
		});
		// EMAIL
		if($('body .email').length>0){
			if($('body .email').val().length>0){
				$('body .email').each(validate.email);
			}
		}
		$(document).on('keyup','.email',validate.email);
		// << Конец инициализации

		//Если все поля данного класса будут валидны, включаем кнопку
		$('body div#add-personal').addClass('disabled');

		$(document).on('keyup','input.pd-field',function()
		{
			if ($('input.fam.validate').length === 1 && $('input.name.validate').length === 1 && $('input.phone.validate').length === 1 && $('input.email.validate').length === 1)
			{
				$('body div#add-personal').removeClass('disabled');
			}
			else
			{
				$('body div#add-personal').addClass('disabled');
			}
		});

		$(document).on('click','#add-personal',function(){
			if($('.personal-data input.pd-field.invalidate').length === 0){
				manager_id = $(this).val();
				url = '/project/createManager/';
				$.post(url,{
						manager_id:manager_id,
						new_dealer_manager_name:$('[name = new_dealer_manager_name]').val(),
						new_dealer_manager_fam:$('[name = new_dealer_manager_fam]').val(),
						new_dealer_manager_patr:$('[name = new_dealer_manager_patr]').val(),
						new_dealer_manager_phone:$('[name = new_dealer_manager_phone]').val(),
						new_dealer_manager_email:$('[name = new_dealer_manager_email]').val()
					},function(data){
						$('.company-personal').html(data);
						$('.fam').each(validate.fio);
						$('.name').each(validate.fio);
						$('.pat').each(validate.pat);
						$('body .phone').each(validate.fonNumber);
						$('.email').each(validate.email);
				});
			}
		});

		$(document).on('click', '#add-address-group-item', function(event){
			event.preventDefault();
			var par = $(this).parents('.address-group'),
				index = parseInt(par.attr('data-group-index'))+1;
			$(this).parent().parent().after('<div class="address-group-item mb-2">'
								+'<div class="form-group mb-2">'
									+'<label class="sr-only" for="region">Регион</label>'
									+'<input id="region" type="text" name="address['+index+'][region]" class="region form-control mb-2" value="" placeholder="Регион"  autocomplete="off" required>'
									+'<label class="sr-only" for="city">Город</label>'
									+'<input id="city" type="text" name="address['+index+'][city]" class="city form-control mb-2" value="" placeholder="Город"  autocomplete="off" required>'
									+'<label class="sr-only" for="street">Улица</label>'
									+'<input id="street" type="text" name="address['+index+'][street]" class="street form-control mb-2" value="" placeholder="Улица"  autocomplete="off" required>'
									+'<label class="sr-only" for="house">Дом</label>'
									+'<input id="house" type="text" name="address['+index+'][house]" class="house form-control mb-2" value="" placeholder="Дом"  autocomplete="off" required>'
									+'<label class="sr-only" for="office">Офис</label>'
									+'<input id="office" type="text" name="address['+index+'][office]" class="office form-control" value="" placeholder="Офис/Помещение"  autocomplete="off">'
								+'</div>'
								+'<div class="btn btn-danger btn-block remove-address-group-item"><i class="icon-cancel"></i></div>'
							+'</div>');
			par.attr('data-group-index', index);
			$('input[name="address['+index+'][region]"]').on('keyup',validate.address);
			$('input[name="address['+index+'][city]"]').on('keyup',validate.address);
			$('input[name="address['+index+'][street]"]').on('keyup',validate.address);
			$('input[name="address['+index+'][house]"]').on('keyup',validate.dob);
			$('input[name="address['+index+'][office]"]').on('keyup',validate.dob);
		});

		$('.add-phone').on('click', function(event){
			var par = $('.dealer_phone').last(), index = parseInt(par.attr('data-group-index'))+1;
			$('.dealer_phone').last().after('<div class="row padding_bottom5px dealer_phone" data-group-index="'+index+'">'
								+'<h4 class="padding_bottom5px">Дополнительный телефон</h4>'
								+'<div class="dealer_phone_container">'
									+'<div class="dealer_phone-item">'
											+'<input type="text" name="dealer_phone['+index+']" class="phone" value="" placeholder="+7(495) 111-1122" autocomplete="off" required>'
										+'</div>'
									+'<div class="remove-phone" class=""><i class="icon-cancel"></i></div>'
								+'</div>'
							+'</div>');
			par.first().attr('data-group-index', index);
			$('input[name="dealer_phone['+index+']"]').on('keyup',validate.fonNumber);
		});

		$('.add-site').on('click', function(event){
			var par = $('.dealer_site').last(), index = parseInt(par.attr('data-group-index'))+1;
			$('.dealer_site').last().after('<div class="padding_bottom5px row dealer_site" data-group-index="'+index+'">'
								+'<h4 class="padding_bottom5px">Дополнительный сайт</h4>'
									+'<div class="dealer_site_container">'
										+'<div class="dealer_site-item">'
											+'<input type="text" name="dealer_site['+index+']" value=""  placeholder="example.com" autocomplete="off" required>'
										+'</div>'
									+'<div class="remove-site" class=""><i class="icon-cancel"></i></div>'
								+'</div>'
							+'</div>');
			par.first().attr('data-group-index', index);
		});

		$('body').on('click', '.remove-phone', function(event){
			$(this).parents('.dealer_phone').remove();
			var par = $('.dealer_phone').last(), index = parseInt(par.attr('data-group-index'))-1;
			par.attr('data-group-index', index);
		});
		$('body').on('click', '.remove-site', function(event){
			$(this).parents('.dealer_site').remove();
			var par = $('.dealer_site').last(), index = parseInt(par.attr('data-group-index'))-1;
			par.attr('data-group-index', index);
		});

		$(document).on('click', '.remove-address-group-item', function(event){
			event.preventDefault();
			if ($('.address-group-item').length===1){
				alert("Вы не можете удалить единственный адрес.");
			}else{
				$(this).parents('.address-group-item').remove();
			}
		});

		$('body').on('click', '#project-add', function(){
			if ($('#accordion input:checked').length===0){
				alert("Отметьте хотя бы одно оборудование.");
				return false;
			}
			if ($('.invalidate').length!==0){
				alert("Поля заполнены не корректно!");
				return false;
			}
		});

		// Если изменено состояние select (выбор сотрудника)
		$(document).on('change','.company-personal select[name="select-personal"]',function() {
			manager_id = $(this).val();
			url = '/project/gettingManager/';
			$.post(url, {manager_id:manager_id}, function(data){
				$( '.company-personal' ).html(data);
				$('.phone').each(validate.fonNumber);
				$('.email').each(validate.email);
			});
		});

	});
})(jQuery);
