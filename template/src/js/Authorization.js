class Authorizations {

    constructor () {

        if ( arguments.length !== 0 ) throw ' Authorization:constructor - Не верное кол-во аргументов! ';

        this.projectId = null;
        this.tools = [];
        this.statusId = null;
        this.comment = null;
    }
    addAuthorizations ( toolsId ) {

        if ( arguments.length !== 1 ) throw ' AddAuthorizations - Не верное кол-во аргументов! ';
        if ( typeof toolsId !== 'number' ) throw ' AddAuthorizations - Не верный тип данных у поля TOOLSID! ';

        this.tools.push(toolsId);

        return this;
    }
    setProjectId ( projectId ) {

        if ( arguments.length !== 1 ) throw ' setProjectId - Не верное количество аргументов! ';
        if ( typeof projectId !== 'number' ) throw ' setProjectId - Не верный тип данных у свойства ID! ';

        this.projectId = projectId;

        return this;

    }
    setStatusId ( statusId ) {

        if ( arguments.length !== 1 ) throw ' setComments - Не верное количество аргументов! ';
        if ( typeof statusId !== 'number' ) throw ' setComments - Не верный тип данных у свойства STATUSID! ';

        this.statusId = statusId;

        return this;

    }
    setComment ( comment ) {

        if ( arguments.length !== 1 ) throw ' setComments - Не верное количество аргументов! ';
        if ( typeof comment !== 'string' ) throw ' setComments - Не верный тип данных у свойства COMMENTS! ';

        this.comment = comment;

        return this;

    }
    getProjectId () {
        return this.projectId;
    }
    getStatusId () {
        return this.statusId;
    }
    getComment () {
        return this.comment;
    }
    getTools () {
        return this.tools;
    }
    getAll () {
        return this;
    }
    getAllJSON () {
        return JSON.stringify(this);
    }
    resetAuthorizations () {

        this.tools = [];
        this.statusId = null;
        this.comment = null;

        return this;

    }
    resetAll () {

        this.projectId = null;
        this.tools = [];
        this.statusId = null;
        this.comment = null;

        return this;

    }
}
