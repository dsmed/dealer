<?php

ini_set('log_errors', 'On');
ini_set('error_log', 'log/php_errors.log');

// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);

define('ROOT',dirname(__FILE__));
define('DEVELOPE_MODE', FALSE);
define('APPVERSION', '5.2.4');

require_once ROOT.'/components/Autoload.php';

$router=new Router();
$router->run();
