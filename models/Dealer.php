<?php

/**
* Dealer
*/
class Dealer {

    public static function createDealer ( $login, $password, $options ) {

		 $db = DB::getConnection();

		 $dealer_sql = "
         INSERT INTO dealers_dealer (
             dealer_type,
             dealer_login,
             dealer_password,
             dealer_brend_name,
             dealer_ur_name,
             dealer_inn,
             dealer_region,
             dealer_city,
             dealer_street,
             dealer_house,
             dealer_house_block,
             dealer_office,
             dealer_company_phone,
             dealer_company_email,
             dealer_site )
         VALUES (
             :dealer_type,
             :dealer_login,
             :dealer_password,
             :dealer_brend_name,
             :dealer_ur_name,
             :dealer_inn,
             :dealer_region,
             :dealer_city,
             :dealer_street,
             :dealer_house,
             :dealer_house_block,
             :dealer_office,
             :dealer_company_phone,
             :dealer_company_email,
             :dealer_site )";

		 $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		 $insert_dealer_data = $db->prepare($dealer_sql);
         $password_md5 = md5 ( $password );
         $search_simbol = [' ', '_'];
         $options['dealer_inn'] = str_replace ( $search_simbol, '', trim ( $options['dealer_inn'] ) );

         $insert_dealer_data->bindParam(':dealer_type',          $options['dealer_type'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_login',         $login, PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_password',      $password_md5, PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_brend_name',    $options['dealer_brend_name'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_ur_name',       $options['dealer_ur_name'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_inn',           $options['dealer_inn'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_region',        $options['region'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_city',          $options['city'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_street',        $options['street'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_house',         $options['house'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_house_block',   $options['block'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_office',        $options['office'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_company_phone', $options['dealer_phone'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_company_email', $options['dealer_email'], PDO::PARAM_STR);
         $insert_dealer_data->bindParam(':dealer_site',          $options['dealer_site'], PDO::PARAM_STR);


		 if ( $insert_dealer_data->execute ( ) ) {

             $lastInsertId = $db->lastInsertId();
             $manager_sql = "
             INSERT INTO dealers_dealer_manager (
                 dealer_id,
                 manager_name,
                 manager_second_name,
                 manager_patronymic,
                 manager_phone,
                 manager_email
             )
             VALUES (
                 :dealer_id,
                 :manager_name,
                 :manager_second_name,
                 :manager_patronymic,
                 :manager_phone,
                 :manager_email )";
             $insert_manager_data = $db->prepare($manager_sql);
             $insert_manager_data->bindParam(':dealer_id',           $lastInsertId, PDO::PARAM_INT);
             $insert_manager_data->bindParam(':manager_name',        $options['dealer_manager_name'], PDO::PARAM_STR);
             $insert_manager_data->bindParam(':manager_second_name', $options['dealer_manager_fam'], PDO::PARAM_STR);
             $insert_manager_data->bindParam(':manager_patronymic',  $options['dealer_manager_patr'], PDO::PARAM_STR);
             $insert_manager_data->bindParam(':manager_phone',       $options['dealer_manager_phone'], PDO::PARAM_STR);
             $insert_manager_data->bindParam(':manager_email',       $options['dealer_manager_email'], PDO::PARAM_STR);

            if ( $insert_manager_data->execute() ) {
                return true;
            } else {
                error_log( "[ ".date("m.d.y")."-".date("H:i:s")." ]
                            [DEALER/ADD_MANAGER] Ошибка добавления менеджера!\n
                            Письмо не отправлено!\n
                            PDO_ERROR: ".PDO::errorInfo()."\n\n", 3, ROOT."/log/db-error.log");
                return false;
            }
		} else {
            error_log( "[ ".date("m.d.y")."-".date("H:i:s")." ]
                        [DEALER/ADD_DEALER] Ошибка добавления дилера!\n
                        PDO_ERROR: ".PDO::errorInfo()."\n\n", 3, ROOT."/log/db-error.log");
            return false;
        }
	}
    public static function checkDealerManager ( $manager_id ) {
        $db = DB::getConnection();
        $managers_id = array ();
        $sql = "SELECT manager_id FROM dealers_dealer_manager WHERE dealer_id = :dealer_id";
        $stmt = $db->prepare($sql);
        $stmt->bindValue( ':dealer_id', User::getUserId(), PDO::PARAM_STR);
        if ( $stmt->execute() ) {
            while ( $row = $stmt->fetch() ) {
      			array_push ( $managers_id, $row['manager_id'] );
      		}
            if ( in_array ( $manager_id, $managers_id ) ) { return true; }
            else { return false; }
        } else {
            error_log( "[ ".date("m.d.y")."-".date("H:i:s")." ]
                        [Method - checkDealerManager] Ошибка проверки менеджера!\n
                        PDO_ERROR: ".PDO::errorInfo()."\n\n", 3, ROOT."/log/dealer.log");
            return false;
        }
    }
    public static function createDealerNewManager ( $new_manager ) {

        $db = DB::getConnection();
        $sql = "INSERT INTO dealers_dealer_manager (dealer_id, manager_name, manager_second_name, manager_patronymic, manager_phone, manager_email)
                        VALUES (:dealer_id, :manager_name, :manager_second_name, :manager_patronymic, :manager_phone, :manager_email)";
        $stmt = $db->prepare($sql);
        $stmt->bindValue( ':dealer_id', User::getUserId(), PDO::PARAM_INT);
        $stmt->bindValue( ':manager_name', $new_manager['new_dealer_manager_name'], PDO::PARAM_STR);
        $stmt->bindValue( ':manager_second_name', $new_manager['new_dealer_manager_fam'], PDO::PARAM_STR);
        $stmt->bindValue( ':manager_patronymic', $new_manager['new_dealer_manager_patr'], PDO::PARAM_STR);
        $stmt->bindValue( ':manager_phone', $new_manager['new_dealer_manager_phone'], PDO::PARAM_STR);
        $stmt->bindValue( ':manager_email', $new_manager['new_dealer_manager_email'], PDO::PARAM_STR);

        if ( $stmt->execute() ) {
            return true;
        } else {
            error_log( "[ ".date("m.d.y")."-".date("H:i:s")." ]
                        [DEALER/ADD_NEW_Manager] Ошибка добавления нового менеджера!\n
                        PDO_ERROR: ".PDO::errorInfo()."\n\n", 3, ROOT."/log/db-error.log");
            return false;
        }
    }
    public static function getDealerManagers ( $userId ) {

  		$db = DB::getConnection();
  		$sql = "SELECT * FROM dealers_dealer_manager WHERE dealer_id = :userid";
  		$dealer_managers = array();
  		$result = $db->prepare($sql);
        $result->bindValue( ':userid', $userId, PDO::PARAM_INT);
  		$result->execute();
  		while ( $row = $result->fetch() ) {
  			$dealer_managers [] = [
  				'id' 			=> $row['manager_id'],
  				'name' 			=> $row['manager_name'],
  				'second_name' 	=> $row['manager_second_name'],
  				'patronymic' 	=> $row['manager_patronymic'],
  				'phone' 		=> $row['manager_phone'],
  				'email' 		=> $row['manager_email']
  			];
  		}
        return $dealer_managers;
	}
    public static function getDsmedManagers () {
        $dsmedManagers = array ();
        $db = DB::getConnection();
        $sql = "SELECT * FROM dealers_dealer_manager WHERE dealer_id = 2";
  		$result = $db->prepare($sql);
        $result->execute();
  		while ( $row = $result->fetch() ) {
  			$dsmedManagers [] = [
  				'id' 			=> $row['manager_id'],
  				'name' 			=> $row['manager_name'],
  				'second_name' 	=> $row['manager_second_name'],
  				'patronymic' 	=> $row['manager_patronymic'],
  				'phone' 		=> $row['manager_phone'],
  				'email' 		=> $row['manager_email']
  			];
  		}
        return $dsmedManagers;
    }
    public static function getDsmedManager ( int $responsibleId ) {
        $dsmedManager = array ();
        $db = DB::getConnection();
        $sql = "SELECT * FROM dealers_dealer_manager WHERE dealer_id = 2 AND manager_id = :responsibleId";
        $result = $db->prepare($sql);
        $result->bindValue( ':responsibleId', $responsibleId, PDO::PARAM_INT);
        $result->execute();
        while ( $row = $result->fetch() ) {
            $dsmedManager [] = [
                'id' 			=> $row['manager_id'],
                'name' 			=> $row['manager_name'],
                'second_name' 	=> $row['manager_second_name'],
                'patronymic' 	=> $row['manager_patronymic'],
                'phone' 		=> $row['manager_phone'],
                'email' 		=> $row['manager_email']
            ];
        }
        return $dsmedManager;
    }
    public static function getDealerOneManager ( $managerId ) {

  		$db = DB::getConnection();
  		$sql = "SELECT * FROM dealers_dealer_manager WHERE manager_id = :managerid";
  		$dealer_manager = array();
  		$result = $db->prepare($sql);
        $result->bindValue( ':managerid', $managerId, PDO::PARAM_INT);
  		$result->execute();
        $one_manager = array();
  		while ( $row = $result->fetch() ) {
  			$one_manager = [
  				'id' 			=> $row['manager_id'],
  				'name' 			=> $row['manager_name'],
  				'second_name' 	=> $row['manager_second_name'],
  				'patronymic' 	=> $row['manager_patronymic'],
  				'phone' 		=> $row['manager_phone'],
  				'email' 		=> $row['manager_email']
  			];
  		}
		return $one_manager;
    }
    public static function getManagerFromProject ( int $projectId ) {

        $db = DB::getConnection();

        $manager = array();

        $sql = "SELECT DDM.manager_id, DDM.dealer_id, DDM.manager_name, DDM.manager_second_name, DDM.manager_patronymic, DDM.manager_phone, DDM.manager_email
                FROM dealer_projects AS DP
                INNER JOIN dealers_dealer_manager AS DDM ON (DP.dealer_manager = DDM.manager_id)
                WHERE DP.id = :ptojectId";

        $result = $db->prepare($sql);
        $result->bindParam(':ptojectId', $projectId , PDO::PARAM_INT);
        $result->execute();

        while ($row = $result->fetch()){
			$manager = [
				'id' => $row['manager_id'],
				'dealer_id' => $row['dealer_id'],
				'name' => $row['manager_name'],
				'second_name' => $row['manager_second_name'],
				'patronymic' => $row['manager_patronymic'],
                'phone' => $row['manager_phone'],
				'email' => $row['manager_email'],
			];
		}

        return $manager;

    }
    public static function createLogin ( $value ) {

        $random = rand ( 0, 10000 );
        $value = substr ( $value, 0, 4 ).$random;
        return $value;
    }
    public static function createPassword ( $password_size ) {

        $nabor = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size_str = strlen( $nabor );
        $password = '';
        for ( $i = 0; $i < $password_size; $i++ ) {
            $password .= $nabor[rand( 0, $size_str - 1 )];
        }
        return $password;
    }
    public static function getBlockBlockDealerUpdate ( int $bronId ) {

        $db = DB::getConnection();

		$sql = "SELECT can_dealer_update FROM dealer_projects_tools WHERE id = :bronId";

        $result = $db->prepare($sql);

        $result->bindParam(':bronId', $bronId, PDO::PARAM_INT);

        $result->execute();

		$cDU = $result->fetch ();

		return $cDU ? intval ( $cDU['can_dealer_update'] ) : false;

    }
    public static function setBlockDealerUpdate ( int $bronId ) {

        $db = DB::getConnection();

        $sql = "UPDATE dealer_projects_tools SET can_dealer_update = 0 WHERE id = :bronId";

        $result = $db->prepare($sql);

        $result->bindParam(':bronId', $bronId, PDO::PARAM_INT);

        $result->execute();

        return true;

    }
    public static function setAllowDealerUpdate ( int $bronId ) {

        $db = DB::getConnection();

        $sql = "UPDATE dealer_projects_tools SET can_dealer_update = 1 WHERE id = :bronId";

        $result = $db->prepare($sql);

        $result->bindParam(':bronId', $bronId, PDO::PARAM_INT);

        $result->execute();

        return true;

    }
}
