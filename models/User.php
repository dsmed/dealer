<?php

/**
* User
*/
class User {

	public static function getCurrentUser(){
		if (isset($_SESSION['user'])) {
			return $_SESSION['user'];
		}else{
			return false;
		}
	}

	public static function isAdmin(){

		if (isset($_SESSION['user'])) {
			return intval($_SESSION['user']['group']) === 1 ?: false;
		}else{
			return false;
		}
	}

	public static function getUserGroup(){

		if (isset($_SESSION['user'])) {
			return intval($_SESSION['user']['group']);
		}else{
			return false;
		}
	}

	public static function getUserId(){

		if (isset($_SESSION['user'])) {
			return intval($_SESSION['user']['id']);
		}else{
			return false;
		}
	}
	public static function getUserBrendName () {

		if (isset($_SESSION['user'])) {
			return $_SESSION['user']['dealer_brend_name'];
		}else{
			return false;
		}
	}

	public static function getUserPhone () {

		if (isset($_SESSION['user'])) {
			return $_SESSION['user']['dealer_company_phone'];
		}else{
			return false;
		}
	}

	public static function getUserEmail () {

		if (isset($_SESSION['user'])) {
			return $_SESSION['user']['dealer_company_email'];
		}else{
			return false;
		}
	}

	public static function getUserProjectCount(){

		$userId = self::getUserId();

		if($userId){
			$db = DB::getConnection();

			$sql = "SELECT count(id) AS projects_count FROM dealer_projects WHERE dealer = :dealer";

			$result = $db->prepare($sql);
			$result->bindParam(':dealer', $userId, PDO::PARAM_INT);
			$result->setFetchMode(PDO::FETCH_ASSOC);
			$result->execute();

			return $result->fetch()['projects_count'];
		}
	}
	public static function getUserMyProjectCount( int $managerId ){

		$userId = self::getUserId();

		if ( $userId ) {

			$db = DB::getConnection();

			$sql = "SELECT count(id) AS projects_count FROM dealer_projects WHERE dealer = :dealer AND dealer_manager = :managerId";

			$result = $db->prepare($sql);
			$result->bindParam(':dealer', $userId, PDO::PARAM_INT);
			$result->bindParam(':managerId', $managerId , PDO::PARAM_INT);
			$result->setFetchMode(PDO::FETCH_ASSOC);
			$result->execute();

			return $result->fetch()['projects_count'];
		}
	}

	public static function getUserById($userId){

		if($userId){
			$db = DB::getConnection();

			$sql = "SELECT id, dealer_login, dealer_brend_name, dealer_inn FROM dealers_dealer WHERE id = :id";

			$result = $db->prepare($sql);
			$result->bindParam(':id', $userId, PDO::PARAM_INT);
			$result->setFetchMode(PDO::FETCH_ASSOC);
			$result->execute();

			return $result->fetch();
		}
	}

	public static function checkUserData($login, $password){

		$db = DB::getConnection();

		$md5password = md5($password);

		$sql = "
			SELECT dd.id,
				   dd.dealer_rights,
				   dd.dealer_login,
				   dd.dealer_password,
				   dd.dealer_brend_name,
				   dd.dealer_ur_name,
				   dd.dealer_inn,
				   dealer_company_phone,
				   dealer_company_email
			FROM dealers_dealer dd
			INNER JOIN dealer_rights dr ON dr.id = dd.dealer_rights
			WHERE dd.dealer_login = :login AND dd.dealer_password = :password
		";

		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->bindParam(':password', $md5password, PDO::PARAM_STR);
		$result->execute();

		$user = $result->fetch();

		return $user ? array(
			'id'=> $user['id'],
			'group' => $user['dealer_rights'],
			'login'=> $user['dealer_login'],
			'dealer_brend_name'=> $user['dealer_brend_name'],
			'dealer_ur_name'=> $user['dealer_ur_name'],
			'dealer_inn' => $user['dealer_inn'],
			'dealer_company_phone' => $user['dealer_company_phone'],
			'dealer_company_email' => $user['dealer_company_email']
		) : false;
	}
	public static function checkUserToken ($login, $password){

		$db = DB::getConnection();

		$md5password = md5($password);

		$sql = "SELECT dealer_login dealer_password, token
				FROM dealers_dealer
				WHERE dealer_login = :login AND dealer_password = :password";

		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->bindParam(':password', $md5password, PDO::PARAM_STR);
		$result->execute();

		$token = $result->fetch();

		return $token ? $token['token'] : false;
	}
	public static function checkUserApi ($dealerId){

		$db = DB::getConnection();

		$sql = "SELECT api FROM dealers_dealer WHERE id = :dealerId";

		$result = $db->prepare($sql);
		$result->bindParam(':dealerId', $dealerId, PDO::PARAM_INT);
		$result->execute();

		$api = $result->fetch();

		return $api ? intval($api['api']) : false;
	}
	public static function checkUserId ($login, $password){

		$db = DB::getConnection();

		$md5password = md5($password);

		$sql = "SELECT id
				FROM dealers_dealer
				WHERE dealer_login = :login AND dealer_password = :password";

		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->bindParam(':password', $md5password, PDO::PARAM_STR);
		$result->execute();

		$id = $result->fetch();

		return $id ? $id['id'] : false;
	}

	public static function auth($user){
		$_SESSION['user'] = $user;
	}

}
