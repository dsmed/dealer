<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Site {
    public static function isJSON($string) {
    	return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
    }
    public static function dealerCheckEmail($dealer_company_email){
        $userGroup = User::getUserGroup();
        /* Rights
        1 - admin
        2 - supplier
        3 - dealer_ds-med
        4 - dealer
        */
        if($userGroup===1||$userGroup===2){
            $db = DB::getConnection();
            $sql = "SELECT dealer_company_email FROM dealers_dealer WHERE dealer_company_email = :dealer_company_email";
            $result = $db->prepare($sql);
            $result->bindValue( ':dealer_company_email', mb_strtolower($dealer_company_email), PDO::PARAM_STR);
            $result->execute();
            return $result->fetch()?true:false;
        }else{
            echo 'Ошибка доступа!';
            error_log("[".date("m.d.y")."-".date("H:i:s")."][Site::checkLogin] Ошибка доступа!", 3, ROOT."/log/rights.log");
        }
    }
    public static function getBronsByPhoenixmed($receiver){
        $db = DB::getConnection();
        $listRecords = array();
        $sql = "SELECT dpt.id, dp.brend_name,
                (SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null' ),', ', a.city,', улица ', a.street,', дом ', a.house,IFNULL(a.office,', офис '),IFNULL( a.office, 'office_null'))SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id =dp.id) AS address,
                dt.tool_name
                FROM dealer_projects AS dp
                INNER JOIN dealer_projects_tools AS dpt ON (dp.id = dpt.project_id)
                INNER JOIN dealer_tools AS dt ON (dt.id = dpt.tools_id)
                WHERE dpt.tools_id in (SELECT id FROM dealer_tools WHERE tool_send_by=$receiver) AND (dpt.status_id=1 OR dpt.status_id=2 OR dpt.status_id=4 OR dpt.status_id=7 OR dpt.status_id=8 OR dpt.status_id=9)
                ORDER BY  dpt.id DESC";
        $result = $db->prepare($sql);
        $result->execute();

        while ($row = $result->fetch()){
            $listRecords[] = [
                'brend_name' => $row['brend_name'],
                'address' => trim(str_replace('  ', ' ', str_replace(', офис office_null', '', str_replace('region_null,', '', $row['address'])))),
                'tool_name' => $row['tool_name']
            ];
        }
        return $listRecords;
    }
    public static function createWordByPhoenixmed($bronsList){
        $phpWord = new \PhpOffice\PhpWord\PhpWord;
        $phpWord->setDefaultFontName('Times New Roman');
        $phpWord->setDefaultFontSize('12');
        $properties = $phpWord->getDocInfo();
        $properties->setCreator('Polyakov Andrey');
        $properties->setCompany('DS-Med ');
        $section = $phpWord->addSection();
        foreach ($bronsList as $id => $record) {
            $text = $record['brend_name'].', '.$record['address'].', '.$record['tool_name'];
            $section->addText($text);
            $section->addTextBreak(1);
            $text = '';
        }
        $objWriter =\PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $url = ROOT."/uploads/reestr-phoenixmed-".time().".docx";
        $objWriter->save($url);
        return $url;
    }
    public static function createTable($data){
        $spreadsheet = new Spreadsheet();
        $page = $spreadsheet->setActiveSheetIndex(0);
        $header_style = [
            'font' => [ 'bold' => false ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [ 'argb' => 'f4b084' ],
                'endColor' => [ 'argb' => 'f4b084' ],
            ],
        ];
        $all_style = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'font' => [
                'size' => 10,
            ],
        ];

        $page->setCellValue('A1', 'Дата')
			    ->setCellValue('B1', 'Наименование учреждения')
			    ->setCellValue('C1', 'ИНН')
				->setCellValue('D1', 'Наименования организации')
				->setCellValue('E1', 'Город поставки, адрес')
				->setCellValue('F1', 'Куратор проекта, контакты')
				->setCellValue('G1', 'Форма сделки')
				->setCellValue('H1', 'Оборудование')
				->setCellValue('I1', 'Кол-во')
                ->setCellValue('J1', 'Сроки поставки (выхода аукциона)')
                ->setCellValue('K1', 'Менеджер')
				->setCellValue('L1', 'Примечания');
        foreach( range ( 'A','L' ) as $columnID ) {
            $page->getStyle($columnID.'1')->applyFromArray($header_style);
        }

        $page->getRowDimension('1')->setRowHeight(39);
        $page->getDefaultColumnDimension()->setWidth(18);
        $page->getStyle('A1:L1')->getAlignment()->setWrapText(true);
        $page->getStyle('A1:L1')->getFont()->getColor()->applyFromArray( ['rgb' => '4c4c4c'] );
        $i=1;
        foreach ( $data as $key => $fields ) {
            $page->setCellValue("A".($i+1), $fields['date']);
            $page->setCellValue("B".($i+1), str_replace('&quot;','"',$fields['brend_name'].'('.$fields['ur_name'].')'));
            $page->setCellValue("C".($i+1), $fields['inn']);
            $page->setCellValue("D".($i+1), $fields['dealer_brend_name'].'('.$fields['dealer_ur_name'].')');
            $page->setCellValue("E".($i+1), $fields['address']);
            $page->setCellValue("F".($i+1), $fields['manager']);
            $page->setCellValue("G".($i+1), $fields['type-deal']);
            $page->setCellValue("H".($i+1), $fields['tools']);
            $page->setCellValue("I".($i+1), '1');
            $page->setCellValue("J".($i+1), '2019-2020');
            $page->setCellValue("K".($i+1), '');
            $page->setCellValue("L".($i+1), '');
            $page->getStyle('A'.($i+1).':L'.($i+1))->applyFromArray($all_style);
            $page->getStyle('A'.($i+1).':L'.($i+1))->getAlignment()->setWrapText(true);
            $i++;
        }

        $file = new Xlsx($spreadsheet);
        $url = ROOT.'/uploads/dsmed-table-'.time().'.xlsx';
        $file->save($url);

        return $url;
    }
    public static function createTableBySearch ( $data ) {

        $spreadsheet = new Spreadsheet();

        $page = $spreadsheet->setActiveSheetIndex(0);

        $header_style = [
            'font' => [ 'bold' => false ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [ 'argb' => '305ca3' ],
                'endColor' => [ 'argb' => '305ca3' ],
            ],
        ];
        $all_style = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'	 => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'font' => [
                'size' => 10,
            ],
        ];

        $page->setCellValue('A1', '№')
             ->setCellValue('B1', 'Дата')
             ->setCellValue('C1', 'Наименования организации')
             ->setCellValue('D1', 'ИНН')
             ->setCellValue('E1', 'Комментарий')
             ->setCellValue('F1', 'Дилер')
             ->setCellValue('G1', 'Менеджер')
             ->setCellValue('H1', 'Адрес')
             ->setCellValue('I1', 'Оборудование - статус');
        foreach( range ( 'A','I' ) as $columnID ) {
            $page->getStyle($columnID.'1')->applyFromArray($header_style);
        }

        $page->getRowDimension('1')->setRowHeight(39);
        $page->getDefaultColumnDimension()->setWidth(18);
        $page->getStyle('A1:I1')->getAlignment()->setWrapText(true);
        $page->getStyle('A1:I1')->getFont()->getColor()->applyFromArray( ['rgb' => 'ffffff'] );
        $i=1;
        foreach ( $data as $key => $fields ) {
            $page->setCellValue("A".($i+1), $fields['p_id'] );
            $page->setCellValue("B".($i+1), $fields['date'] . ' - ' . $fields['time'] );
            $page->setCellValue("C".($i+1), str_replace ( '&quot;', '"', $fields['cl_ur_name'] ) );
            $page->setCellValue("D".($i+1), $fields['inn'] );
            $page->setCellValue("E".($i+1), str_replace ( '&quot;', '"',  $fields['comment'] ) );
            $page->setCellValue("F".($i+1), $fields['dealer'] );
            $page->setCellValue("G".($i+1), $fields['manager'] );
            $page->setCellValue("H".($i+1), $fields['dealer'] );

            $toolStatus = '';

            foreach ( $data[$key]['brons'] as $brone ) {

                $toolStatus .= $brone[1] . ' - ' . $brone[3] . "\n\r";

            }
            $page->setCellValue("I".($i+1), $toolStatus);
            $page->getStyle('A'.($i+1).':I'.($i+1))->applyFromArray($all_style);
            $page->getStyle('A'.($i+1).':I'.($i+1))->getAlignment()->setWrapText(true);
            $i++;
        }

        $file = new Xlsx ( $spreadsheet );
        $path = '/uploads/export/';
        $name = 'export'.time().'.xlsx';
        $url =  ROOT . $path . $name;
        $file->save( $url );

        return $name;
    }
    public static function sendMail ( int $userGroup, $managerMail, $email, $messege, $file=false, $receiver=false){
        # Rights
        #   1 - admin
        #   2 - supplier
        #   3 - dealer_ds-med
        #   4 - dealer
        require ROOT.'/config/mail_config.php';
        $mail = new PHPMailer(true);
        if($userGroup==1||$userGroup==2||$userGroup==3){
            # Если вошел сотрудник Ds-Med, всё отправлять ресиверам
            try {
                $mail->isSMTP();
                $mail->Host = $senders['no-reaply@ds-med.info']['host'];
                $mail->SMTPAuth = true;
                $mail->Username = $senders['no-reaply@ds-med.info']['user'];
                $mail->Password = $senders['no-reaply@ds-med.info']['pass'];
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;
                $mail->CharSet = 'UTF-8';
                $mail->setFrom($senders['no-reaply@ds-med.info']['user'], 'Отдел продаж ООО "ДС-Мед"');
                $mail->addReplyTo($senders['project']['user'], 'Отдел продаж ООО "ДС-Мед"');
                $mail->addAddress($email);
                $mail->addAddress($managerMail);
                $mail->addAddress($senders['project']['user']);
                $mail->addBCC($senders['no-reaply@ds-med.info']['user']);
                $mail->isHTML(true);
                $mail->Subject = 'Запрос авторизации';
                $mail->Body = $messege;
                if(file_exists($file)){
                    $mail->AddAttachment($file);
                }else{
                    error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Отсутствует файл для прикрепления к письму! Файл: ".$file."\n", 3, ROOT."/log/email-error.log" );
                }
                # Для феникса
                if($receiver==5){
                    $phenixFile=ROOT.'/uploads/old-reestr-phoenixmed.docx';
                    if(file_exists($phenixFile)){
                        $mail->AddAttachment($phenixFile);
                    }else{
                        error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Отсутствует файл для прикрепления к письму! Файл: ".$phenixFile."\n", 3, ROOT."/log/email-error.log" );
                    }
                }
                $mail->send();
                error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Письмо отправленно ресиверу: $email", 3, ROOT."/log/success-mail.log");
                return false;
            } catch(Exception $e){
                error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Ошибка отправки почты. PHPMailer: $mail->ErrorInfo \n", 3, ROOT."/log/email-error.log");
                return array('error'=>$mail->ErrorInfo);
            }
        }else{
            # Если вошел дилер, всё отправлять на почту opt@ds-med.ru
            try {
                $mail->isSMTP();
                $mail->Host = $senders['no-reaply@ds-med.info']['host'];
                $mail->SMTPAuth = true;
                $mail->Username = $senders['no-reaply@ds-med.info']['user'];
                $mail->Password = $senders['no-reaply@ds-med.info']['pass'];
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;
                $mail->CharSet = 'UTF-8';
                $mail->setFrom($senders['no-reaply@ds-med.info']['user'], 'Отдел по работе с дилерами ООО "ДС-Мед"' );
                $mail->addReplyTo($senders['opt']['user'], 'Отдел по работе с дилерами ООО "ДС-Мед"' );
                $mail->addAddress($managerMail);
                $mail->addBCC($senders['no-reaply@ds-med.info']['user']);
                $mail->isHTML(true);
                $mail->Subject = 'Запрос авторизации';
                $mail->Body = $messege;
                $mail->send();
                error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Письмо отправленно ресиверу: $email", 3, ROOT."/log/success-mail.log");
            } catch(Exception $e){
                error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Ошибка отправки почты. PHPMailer: $mail->ErrorInfo \n", 3, ROOT."/log/email-error.log");
            }
            try {
                $ODMail = new PHPMailer(true);
                $ODMail->isSMTP();
                $ODMail->Host = $senders['no-reaply@ds-med.info']['host'];
                $ODMail->SMTPAuth = true;
                $ODMail->Username = $senders['no-reaply@ds-med.info']['user'];
                $ODMail->Password = $senders['no-reaply@ds-med.info']['pass'];
                $ODMail->SMTPSecure = 'ssl';
                $ODMail->Port = 465;
                $ODMail->CharSet = 'UTF-8';
                $ODMail->setFrom($senders['no-reaply@ds-med.info']['user'], 'Отдел продаж ООО "ДС-Мед"');
                $ODMail->addReplyTo($senders['opt']['user'], 'Отдел продаж ООО "ДС-Мед"');
                $ODMail->addAddress($senders['opt']['user']);
                $ODMail->isHTML(true);
                $ODMail->Subject = 'Запрос авторизации';
                $ODMail->Body = $messege;
                if(file_exists($file)){
                    $ODMail->AddAttachment($file);
                }else{
                    error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Отсутствует файл для прикрепления к письму! Файл: ".$file."\n", 3, ROOT."/log/email-error.log" );
                }
                $ODMail->send();
                error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Письмо отправленно ресиверу: $email", 3, ROOT."/log/success-mail.log");
                return false;
            } catch(Exception $e){
                error_log("[".date("m.d.y")."-".date("H:i:s")."][SITE::SENDMAIL] Ошибка отправки почты. PHPMailer: $ODMail->ErrorInfo \n", 3, ROOT."/log/email-error.log");
                return array('error'=>$ODMail->ErrorInfo);
            }
        }
    }
    public static function addMailRecord( int $type, int $managerId, $addreses, $theme, $bodyMail, int $FLAGSEND, $file = '',  int $visibility = 1){
		try {

			$date=date('d-m-Y H:i:s');

			$db = DB::getConnection();
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$sql = "INSERT INTO dealer_send_mail(type, d_create, manager_id, addresses, theme, body_mail, file, send, visibility)
					VALUES (:type, :dcreate, :managerId, :addresses, :theme, :bodymail, :file, :flagsend, :visibility)";
			$result = $db->prepare($sql);
			$result->bindParam(':type', $type, PDO::PARAM_INT);
			$result->bindParam(':dcreate', $date, PDO::PARAM_STR);
			$result->bindParam(':managerId', $managerId, PDO::PARAM_INT);
			$result->bindParam(':addresses', $addreses, PDO::PARAM_STR);
			$result->bindParam(':theme',$theme, PDO::PARAM_STR);
			$result->bindParam(':bodymail', $bodyMail, PDO::PARAM_STR);
			$result->bindParam(':file', $file, PDO::PARAM_STR);
			$result->bindParam(':flagsend', $FLAGSEND, PDO::PARAM_INT);
			$result->bindParam(':visibility', $visibility, PDO::PARAM_INT);

			$res = $result->execute();

            return $res;

		} catch ( Exception $e ) {
			error_log("Ошибка в методе addMailRecord".$e->getMessage()."\n",3,ROOT."/log/test.log");
		}
		return false;
	}
    public static function exportDownload ( string $fileName ){

        $path = ROOT . '/uploads/export/';
        $file = $path . $fileName;

        if ( file_exists ( $file ) ) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
              ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            exit;
        }

    }
}
