<?php

/**
* Project
*/

class MyProject {


    public static function getProjectList ( array $projectList, int $dealerId, int $managerId ) {

        $db = DB::getConnection();

        if ( ! Dealer::checkDealerManager( $managerId ) ) throw new Exception('В вашей компании нет такого сотрудника!');

        $sql = "SELECT p.id, p.date, p.brend_name, p.ur_name, p.inn, p.comment, p.responsible
				,dd.id AS d_id,dd.dealer_brend_name,dd.dealer_ur_name,ddm.manager_id,ddm.manager_second_name,ddm.manager_name,ddm.manager_patronymic
				,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
				,(SELECT GROUP_CONCAT(dpt.id, ';',dpt.tools_id,';',dpt.status_id SEPARATOR '+')FROM dealer_projects_tools dpt WHERE dpt.project_id = p.id)AS brons_params
				FROM `dealer_projects` AS p
				INNER JOIN dealers_dealer AS dd ON (dd.id = p.dealer)
				INNER JOIN dealers_dealer_manager AS ddm ON (ddm.manager_id = p.dealer_manager)
				WHERE p.dealer = :dealerId AND p.dealer_manager = :managerId
				ORDER BY p.id DESC";

		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

        $result = $db->prepare($sql);
        $result->bindParam(':dealerId', $dealerId , PDO::PARAM_INT);
        $result->bindParam(':managerId', $managerId , PDO::PARAM_INT);
		$result->execute();

        while ($row = $result->fetch()){
			$projectList[] = [
				'p_id' => $row['id'],
				'date' => date ( 'd-m-Y', strtotime ( $row['date'] ) ),
				'time' => date ( 'H:i', strtotime ( $row['date'] ) ),
				'cl_ur_name' => str_replace(' (  )', '' , $row['brend_name'].' ('.$row['ur_name'].')'),
				'inn' => $row['inn'],
				'comment' => $row['comment'],
				'responsible' => $row['responsible'],
				'd_id' => $row['d_id'],
				'dealer' => $row['dealer_brend_name'].' ('.$row['dealer_ur_name'].')',
				'manager_id' => $row['manager_id'],
				'manager' => $row['manager_second_name'].' '.$row['manager_name'].' '.$row['manager_patronymic'],
				'address' =>  str_replace( ', корпус block_null', '', str_replace(', офис office_null', '', (str_replace('region_null, ', '', $row['address'])))),
				'brons_params' => $row['brons_params']
			];
		}
		return $projectList;
	}
    public static function getBronParamsProjectList($projectList){
		$db = DB::getConnection();
		foreach ($projectList as $key => $projectitem) {
			$brons_params = explode('+', $projectitem['brons_params']);
		 	foreach ($brons_params as $id => $bron_params) {
		 		$projectList[$key]['brons'][$id] = explode(';', $bron_params);
				$sql = "SELECT dt.tool_name, dt.csdsmd, ds.id as status_id, ds.status
						FROM dealer_projects_tools AS dpt
						INNER JOIN dealer_status AS ds ON (ds.id=dpt.status_id)
						INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id)
						WHERE dpt.id = :id";
				$result = $db->prepare($sql);
				$result->bindParam(':id', $projectList[$key]['brons'][$id][0], PDO::PARAM_STR);
				$result->execute();
				while ($row = $result->fetch()){
					$projectList[$key]['brons'][$id][1] = $row['tool_name'];
					$projectList[$key]['brons'][$id][2] = intval($row['status_id']);
					$projectList[$key]['brons'][$id][3] = $row['status'];
					$projectList[$key]['brons'][$id][4] = intval($row['csdsmd']);
				}
		 	}
		}

		return $projectList;
	}
    public static function getCABAE ( int $dealerId, int $managerId ) {
        $db = DB::getConnection();
        $sql = "SELECT COUNT(p.id) AS COUNT FROM dealer_projects AS p WHERE ( p.dealer = :dealerId AND p.dealer_manager = :managerId ) AND p.id IN (SELECT project_id FROM dealer_projects_tools AS dpt WHERE dpt.status_id = 8)";
        $result = $db->prepare($sql);
        $result->bindParam(':dealerId', $dealerId , PDO::PARAM_INT);
        $result->bindParam(':managerId', $managerId , PDO::PARAM_INT);
        $result->execute();
        $id = $result->fetch ();
        return $id ? intval ( $id['COUNT'] ) : false;
    }
    public static function getCABRQ ( int $dealerId, int $managerId ) {
        $db = DB::getConnection();
        $sql = "SELECT COUNT(p.id) AS COUNT FROM dealer_projects AS p WHERE ( p.dealer = :dealerId AND p.dealer_manager = :managerId ) AND p.id IN (SELECT project_id FROM dealer_projects_tools AS dpt WHERE dpt.status_id = 9)";
        $result = $db->prepare($sql);
        $result->bindParam(':dealerId', $dealerId , PDO::PARAM_INT);
        $result->bindParam(':managerId', $managerId , PDO::PARAM_INT);
        $result->execute();
        $id = $result->fetch ();
        return $id ? intval ( $id['COUNT'] ) : false;
    }
    public static function getAttentionProjectList ( array $attentionProjectList, string $type, int $dealerId, int $managerId ) {

        preg_match ( '/[8-9]/', $type, $statusId ) ? $statusId = intval ( $statusId[0] ) : $statusId = false;

        if ( ! $statusId ) throw new Exception('Переданы не корректные данные!');

        $db = DB::getConnection();

        $sql = "SELECT p.id, p.date, p.brend_name, p.ur_name, p.inn, p.comment, p.responsible
                ,dd.id AS d_id,dd.dealer_brend_name,dd.dealer_ur_name,ddm.manager_id,ddm.manager_second_name,ddm.manager_name,ddm.manager_patronymic
                ,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
                ,(SELECT GROUP_CONCAT(dpt.id, ';',dpt.tools_id,';',dpt.status_id SEPARATOR '+')FROM dealer_projects_tools dpt WHERE dpt.project_id = p.id)AS brons_params
                FROM dealer_projects AS p
                INNER JOIN dealers_dealer AS dd ON (dd.id = p.dealer)
                INNER JOIN dealers_dealer_manager AS ddm ON (ddm.manager_id = p.dealer_manager)
                WHERE p.dealer = :dealerId AND p.dealer_manager = :managerId AND p.id IN ( SELECT project_id FROM dealer_projects_tools AS dpt WHERE dpt.status_id = :statusId)
                ORDER BY p.id ASC ";
        // $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $result = $db->prepare($sql);
        $result->bindParam(':dealerId', $dealerId , PDO::PARAM_INT);
        $result->bindParam(':managerId', $managerId , PDO::PARAM_INT);
        $result->bindParam(':statusId', $statusId , PDO::PARAM_INT);
        $result->execute();
        while ($row = $result->fetch()){
            $projectList[] = [
                'p_id' => $row['id'],
                'date' => date ( 'd-m-Y', strtotime ( $row['date'] ) ),
                'time' => date ( 'H:i', strtotime ( $row['date'] ) ),
                'cl_ur_name' => str_replace(' (  )', '' , $row['brend_name'].' ('.$row['ur_name'].')'),
                'inn' => $row['inn'],
                'comment' => $row['comment'],
                'responsible' => $row['responsible'],
                'd_id' => $row['d_id'],
                'dealer' => $row['dealer_brend_name'].' ('.$row['dealer_ur_name'].')',
                'manager_id' => $row['manager_id'],
                'manager' => $row['manager_second_name'].' '.$row['manager_name'].' '.$row['manager_patronymic'],
                'address' =>  str_replace( ', корпус block_null', '', str_replace(', офис office_null', '', (str_replace('region_null, ', '', $row['address'])))),
                'brons_params' => $row['brons_params']
            ];
        }
        return $projectList;
    }

}
