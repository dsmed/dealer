<?php
require_once('vendor/autoload.php');

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class Api {

    public static function checkJWT(){

        $headers = getallheaders();

        if(empty($headers['Content-type']) || trim($headers['Content-type']) !== 'application/json; charset=utf-8') throw new Exception('Не верный Content-type');
        if(empty($headers['Authorization'])) throw new Exception('Отсутствует заголовок Authorization');

        preg_match('/bearer/',$headers['Authorization'],$bearer)?$bearer=$bearer[0]:$bearer=false;

        if(!$bearer) throw new Exception('Не верный Authorization');

        $JWT = trim(substr($headers['Authorization'], 6));
        $JWT = (new Parser())->parse($JWT);

        $tokenParams = include('config/jwt.php');

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer($tokenParams['iss']);
        $data->setAudience($tokenParams['sub']);
        $data->setId($tokenParams['jti']);
        $data->setCurrentTime(time());

        $signer = new Sha256();

        # Если jwt не просрочен, совподают iss, sub, jti и совпадает подпись

        if(!$JWT->validate($data) || !$JWT->verify($signer, $tokenParams['secret'])) throw new Exception('Ошибка валидации токена!');

        $dealerId = $JWT->getClaim('uid');

        return $dealerId;
    }
    public static function getEquipments(){
        return Project::getToolsListByApi();
    }
    public static function getPersonal($dealerId){
        return Dealer::getDealerManagers($dealerId);
    }
    public static function disabledApi($dealerId){
        $db = DB::getConnection();
        $sql = "UPDATE dealers_dealer SET api = 0 WHERE id = :dealerId";
        $result = $db->prepare($sql);
		$result->bindParam(':dealerId', $dealerId, PDO::PARAM_INT);
        $result->execute();
        return true;
    }
    public static function createJWT($dealerId){

        $tokenParams = include(ROOT.'/config/jwt.php');

        $signature = new Sha256();

        $JWT= (new Builder())
            ->setIssuer($tokenParams['iss'])
            ->setAudience($tokenParams['sub'])
            ->setId($tokenParams['jti'], true)
            ->setIssuedAt(time())
            ->setNotBefore(time() + $tokenParams['nbf'])
            ->setExpiration(time() + $tokenParams['exp'])
            ->set('uid', $dealerId)
            ->sign($signature, $tokenParams['secret'])
            ->getToken();

        return $JWT;
    }
}
