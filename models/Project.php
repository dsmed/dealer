<?php

/**
* Project
*/

class Project {

	const SHOW_BY_DEFAULT = 100;

	public static function getProjectAddresses ( int $projectId ) {

		$db = DB::getConnection();

		$addresses = array();

		$sql = "SELECT * FROM dealers_projects_addresses WHERE project_id = :projectId ";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId , PDO::PARAM_INT);
		$result->execute();

		while ( $row = $result->fetch() ) {

			$addresses [] = [
				'region' => $row['region'],
				'city' => $row['city'],
				'street' => $row['street'],
				'house' => $row['house'],
				'block' => $row['block'],
				'office' => $row['office'],
			];

		}

		return $addresses;

	}
	public static function getProjectBrendName ( int $projectId ) {

		$db = DB::getConnection();

		$sql = "SELECT brend_name FROM dealer_projects WHERE id = :id";

		$result = $db->prepare($sql);
		$result->bindParam(':id', $projectId , PDO::PARAM_INT);
		$result->execute();

		$brendName = $result->fetch ();

		return $brendName ? $brendName['brend_name'] : false;

	}
	public static function getProjectUName ( int $projectId ) {

		$db = DB::getConnection();

		$sql = "SELECT ur_name FROM dealer_projects WHERE id = :id";

		$result = $db->prepare($sql);
		$result->bindParam(':id', $projectId , PDO::PARAM_INT);
		$result->execute();

		$uName = $result->fetch ();

		return $uName ? $uName['ur_name'] : false;

	}
	public static function getProjectId ( int $PTID ){
		$db = DB::getConnection();
		$sql = "SELECT project_id FROM dealer_projects_tools WHERE id = :id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $PTID , PDO::PARAM_INT);
		$result->execute();
		$id = $result->fetch ();
		return $id ? $id['project_id'] : false;
	}
	public static function getProjectType ( int $projectId ) {

		$db = DB::getConnection();
		$sql = "SELECT type FROM dealer_projects WHERE id = :id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $projectId , PDO::PARAM_INT);
		$result->execute();
		$id = $result->fetch();

		return $id ? intval($id['type']) : false;

	}
	public static function getProjectsDealerCountByInterval ( int $dealerId ) {

		$now = (new DateTime('now'))->format('Y:m:d H:i:s');
		$last = (new DateTime('-30 minutes'))->format('Y:m:d H:i:s');

		$db = DB::getConnection();
		$sql = "SELECT COUNT(id) AS `count` FROM dealer_projects WHERE dealer = :dealerId AND date BETWEEN :last AND :now";
		$result = $db->prepare($sql);
		$result->bindParam(':dealerId', intval($dealerId), PDO::PARAM_INT);
		$result->bindParam(':now', $now, PDO::PARAM_STR);
		$result->bindParam(':last', $last, PDO::PARAM_STR);
		$result->execute();
		$count = $result->fetch();;

		return $count ? intval($count['count']) : false;

	}
	public static function getProjectByQueryAuth ( int $projectId ) {
		$db = DB::getConnection();
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT
				p.id, p.brend_name, p.ur_name, p.inn, p.dealer, p.dealer_manager, p.date,p.type,
				(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address,
				ddm.manager_second_name, ddm.manager_name, ddm.manager_patronymic, dd.dealer_brend_name, dd.dealer_ur_name
				FROM dealer_projects AS p
				INNER JOIN dealers_dealer AS dd ON (dd.id=p.dealer)
				INNER JOIN dealers_dealer_manager AS ddm ON (ddm.manager_id=p.dealer_manager)
				WHERE p.id = :projectId";
		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId, PDO::PARAM_INT);
		$result->execute();
		while ($row = $result->fetch()){
			$project = [
				'id' => $row['id'],
				'brend_name' => $row['brend_name'],
				'ur_name' => $row['ur_name'],
				'inn' => $row['inn'],
				'address' => str_replace( ', корпус block_null', '', str_replace(', офис office_null', '', (str_replace('region_null, ', '', $row['address'])))),
 				'date' => date('d-m-Y', strtotime($row['date'])),
				'type-deal' => $row['type'] === 1?'Прямая':'Аукцион',
				'manager_second_name' => $row['manager_second_name'],
				'manager_name' => $row['manager_name'],
				'manager_patronymic' => $row['manager_patronymic'],
				'dealer_brend_name' => $row['dealer_brend_name'],
				'dealer_ur_name' => $row['dealer_ur_name'],
				'dealer' => $row['dealer'],
				'dealer_manager' => $row['dealer_manager'],
			];
		}
		return $project;
	}
	public static function getProjectToolByChangeStatus ( int $projectId ) {

		$db = DB::getConnection();
		$sql = "
		SELECT tool.tool_name
		FROM dealer_tools AS tool
		INNER JOIN dealer_projects_tools AS pt ON ( pt.id = :projectId)
		WHERE tool.id = pt.tools_id";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId , PDO::PARAM_STR);
		$result->execute();

		while ($row = $result->fetch()){
			$tool = $row['tool_name'];
		}
		return $tool;
	}
	public static function getProjectList ( int $page = 1, $where ) {
		$page = intval($page);
		$limit = self::SHOW_BY_DEFAULT;
		$offset = ($page - 1) * self::SHOW_BY_DEFAULT;
		$db = DB::getConnection();
		$projectList = array();
		$sql = "SELECT p.id, p.date, p.brend_name, p.ur_name, p.inn, p.comment, p.responsible
				,dd.id AS d_id,dd.dealer_brend_name,dd.dealer_ur_name,ddm.manager_id,ddm.manager_second_name,ddm.manager_name,ddm.manager_patronymic
				,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
				,(SELECT GROUP_CONCAT(dpt.id, ';',dpt.tools_id,';',dpt.status_id SEPARATOR '+')FROM dealer_projects_tools dpt WHERE dpt.project_id = p.id)AS brons_params
				FROM `dealer_projects` AS p
				INNER JOIN dealers_dealer AS dd ON (dd.id = p.dealer)
				INNER JOIN dealers_dealer_manager AS ddm ON (ddm.manager_id = p.dealer_manager)
				$where
				ORDER BY p.id DESC
				LIMIT :limit
				OFFSET :offset";
		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		$result = $db->prepare($sql);
		$result->bindParam(':limit', $limit , PDO::PARAM_INT);
		$result->bindParam(':offset', $offset, PDO::PARAM_INT);
		$result->execute();
		while ($row = $result->fetch()){
			$projectList[] = [
				'p_id' => $row['id'],
				'date' => date ( 'd-m-Y', strtotime ( $row['date'] ) ),
				'time' => date ( 'H:i', strtotime ( $row['date'] ) ),
				'cl_ur_name' => str_replace(' (  )', '' , $row['brend_name'].' ('.$row['ur_name'].')'),
				'inn' => $row['inn'],
				'comment' => $row['comment'],
				'responsible' => $row['responsible'],
				'd_id' => $row['d_id'],
				'dealer' => $row['dealer_brend_name'].' ('.$row['dealer_ur_name'].')',
				'manager_id' => $row['manager_id'],
				'manager' => $row['manager_second_name'].' '.$row['manager_name'].' '.$row['manager_patronymic'],
				'address' =>  str_replace( ', корпус block_null', '', str_replace(', офис office_null', '', (str_replace('region_null, ', '', $row['address'])))),
				'brons_params' => $row['brons_params']
			];
		}
		return $projectList;
	}
	public static function getAttentionProjectList ( $type, int $dealerId ) {

		preg_match ( '/[8-9]/', $type, $statusId ) ? $statusId = intval ( $statusId[0] ) : $statusId = false;

		if ( ! $statusId ) throw new Exception('Переданы не корректные данные!');

		$db = DB::getConnection();
		$attentionProjectList = array();
		$sql = "SELECT p.id, p.date, p.brend_name, p.ur_name, p.inn, p.comment, p.responsible
				,dd.id AS d_id,dd.dealer_brend_name,dd.dealer_ur_name,ddm.manager_id,ddm.manager_second_name,ddm.manager_name,ddm.manager_patronymic
				,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
				,(SELECT GROUP_CONCAT(dpt.id, ';',dpt.tools_id,';',dpt.status_id SEPARATOR '+')FROM dealer_projects_tools dpt WHERE dpt.project_id = p.id)AS brons_params
				FROM dealer_projects AS p
				INNER JOIN dealers_dealer AS dd ON (dd.id = p.dealer)
				INNER JOIN dealers_dealer_manager AS ddm ON (ddm.manager_id = p.dealer_manager)
				WHERE p.dealer = :dealerId AND p.id IN ( SELECT project_id FROM dealer_projects_tools AS dpt WHERE dpt.status_id = :statusId)
				ORDER BY p.id ASC ";
		// $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		$result = $db->prepare($sql);
		$result->bindParam(':dealerId', $dealerId , PDO::PARAM_INT);
		$result->bindParam(':statusId', $statusId , PDO::PARAM_INT);
		$result->execute();
		while ($row = $result->fetch()){
			$projectList[] = [
				'p_id' => $row['id'],
				'date' => date ( 'd-m-Y', strtotime ( $row['date'] ) ),
				'time' => date ( 'H:i', strtotime ( $row['date'] ) ),
				'cl_ur_name' => str_replace(' (  )', '' , $row['brend_name'].' ('.$row['ur_name'].')'),
				'inn' => $row['inn'],
				'comment' => $row['comment'],
				'responsible' => $row['responsible'],
				'd_id' => $row['d_id'],
				'dealer' => $row['dealer_brend_name'].' ('.$row['dealer_ur_name'].')',
				'manager_id' => $row['manager_id'],
				'manager' => $row['manager_second_name'].' '.$row['manager_name'].' '.$row['manager_patronymic'],
				'address' =>  str_replace( ', корпус block_null', '', str_replace(', офис office_null', '', (str_replace('region_null, ', '', $row['address'])))),
				'brons_params' => $row['brons_params']
			];
		}
		return $projectList;
	}
	public static function getBronsByProject ( int $projectId ) {

		$db = DB::getConnection();
		$sql = "SELECT dpt.project_id, dpt.id, dt.tool_name, dt.csdsmd, ds.id as status_id, ds.status
				FROM dealer_projects_tools AS dpt
				INNER JOIN dealer_status AS ds ON (ds.id=dpt.status_id)
				INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id)
				WHERE dpt.project_id=:projectId";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId , PDO::PARAM_STR);
		$bronsByProject = array ();
		$result->execute();

		while ($row = $result->fetch()){
			$bronsByProject[] = [
				0 => $row['id'],
				1 => $row['tool_name'],
				2 => $row['status_id'],
				3 => $row['status'],
				4 => $row['csdsmd'],
				'p-id' => $row['project_id']
			];
		}

		return $bronsByProject;
	}
	public static function getBronComments ( int $bron_id ) {
		$db = DB::getConnection();
		$sql = "SELECT status_id, comment, comment_date FROM dealer_status_comments WHERE tools_id = :bron_id";
		$result = $db->prepare($sql);
		$result->bindParam(':bron_id', $bron_id , PDO::PARAM_STR);
		$comments = array ();
		$result->execute();
		while ( $row = $result->fetch () ) {
			$comments[] = [
				'status_id' 	=> $row['status_id'],
				'comment' 		=> $row['comment'],
				'comment_date'	=> date('d-m-Y',strtotime($row['comment_date']))
			];

		}
		return $comments;
	}
	public static function getBronLastComment ( int $bron_id ) {
		$db = DB::getConnection();
		$sql = "SELECT status_id, comment, comment_date FROM dealer_status_comments WHERE tools_id = :bron_id ORDER BY id DESC";
		$result = $db->prepare($sql);
		$result->bindParam(':bron_id', $bron_id , PDO::PARAM_STR);
		$comments = array ();
		$result->execute();
		$row = $result->fetch();
		$comments[] = [
			'status_id' 	=> $row['status_id'],
			'comment' 		=> $row['comment'],
			'comment_date'	=> date('d-m-Y',strtotime($row['comment_date']))
		];
		return $comments;
	}
	public static function getBronParamsProjectList($projectList){
		$db = DB::getConnection();
		foreach ($projectList as $key => $projectitem) {
			$brons_params = explode('+', $projectitem['brons_params']);
		 	foreach ($brons_params as $id => $bron_params) {
		 		$projectList[$key]['brons'][$id] = explode(';', $bron_params);
				$sql = "SELECT dt.tool_name, dt.csdsmd, ds.id as status_id, ds.status
						FROM dealer_projects_tools AS dpt
						INNER JOIN dealer_status AS ds ON (ds.id=dpt.status_id)
						INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id)
						WHERE dpt.id = :id";
				$result = $db->prepare($sql);
				$result->bindParam(':id', $projectList[$key]['brons'][$id][0], PDO::PARAM_STR);
				$result->execute();
				while ($row = $result->fetch()){
					$projectList[$key]['brons'][$id][1] = $row['tool_name'];
					$projectList[$key]['brons'][$id][2] = intval($row['status_id']);
					$projectList[$key]['brons'][$id][3] = $row['status'];
					$projectList[$key]['brons'][$id][4] = intval($row['csdsmd']);
				}
		 	}
		}

		return $projectList;
	}
	public static function getTotalProject($userGroup){
		if($userGroup==3||$userGroup==4){
			$dealerId=User::getUserId();
			$where="WHERE dealer=$dealerId";
		}else{
			$where="";
		}
		$db = DB::getConnection();
		$result = $db->query("SELECT COUNT(id) AS count FROM dealer_projects $where");
		return $result->fetch()['count'];
	}
	public static function getProjectSelectParams(){
		$projectSelect=[
			1=>['id'=>'id','name'=>'№ Проекта','view_all'=>1,'view_dealer'=>1],
			2=>['id'=>'date','name'=>'Дата','view_all'=>1,'view_dealer'=>1],
			3=>['id'=>'address','name'=>'Адрес','view_all'=>1,'view_dealer'=>1],
			4=>['id'=>'dealer','name'=>'Дилер','view_all'=>1,'view_dealer'=>0],
			5=>['id'=>'manager','name'=>'Менеджер','view_all'=>1,'view_dealer'=>1],
			6=>['id'=>'client','name'=>'Конечный клиент','view_all'=>1,'view_dealer'=>1],
			7=>['id'=>'inn','name'=>'ИНН Конечного клиента','view_all'=>1,'view_dealer'=>1],
			8=>['id'=>'comment','name'=>'Комментарий','view_all'=>1,'view_dealer'=>1]
		];
		return $projectSelect;
	}
	public static function getToolsList(){

		$db = DB::getConnection();

		$toolsList = array();

		$result = $db->query("SELECT id, tool_name, view_all, view_dealer, tool_send_by, tool_view_block, tool_sort FROM dealer_tools ORDER BY tool_sort");

		while ($row = $result->fetch()){
			$toolsList[] = [
				'id' 				=> intval($row['id']),
				'tool_name' 		=> $row['tool_name'],
				'view_all' 			=> intval($row['view_all']),
				'view_dealer' 		=> intval($row['view_dealer']),
				'tool_send_by' 		=> intval($row['tool_send_by']),
				'tool_view_block' 	=> intval($row['tool_view_block']),
				'tool_sort' 		=> intval($row['tool_sort'])
			];
		}

		return $toolsList;
	}
	public static function getToolName ( int $toolId ) {

		$db = DB::getConnection();

		$sql = "SELECT tool_name FROM dealer_tools WHERE id = :id";

		$result = $db->prepare($sql);
		$result->bindParam(':id', $toolId, PDO::PARAM_INT);
		$result->execute();
		$toolName = $result->fetch ();

		return $toolName ? $toolName['tool_name'] : false;
	}
	public static function getToolId ( int $bronId ) {

		$db = DB::getConnection();

		$sql = "SELECT tools_id FROM dealer_projects_tools WHERE id = :id";

		$result = $db->prepare($sql);
		$result->bindParam(':id', $bronId, PDO::PARAM_INT);
		$result->execute();
		$toolId = $result->fetch ();

		return $toolId ? $toolId['tools_id'] : false;
	}
	public static function getToolsListByApi(){

		$db = DB::getConnection();

		$toolsList = array();

		$result = $db->query("SELECT id, tool_name FROM dealer_tools WHERE view_all = 1");

		while ($row = $result->fetch()){
			$toolsList[] = [
				'id' => intval($row['id']),
				'name' => $row['tool_name'],
			];
		}

		return $toolsList;
	}
	public static function getToolsOnBlockList($block){
		$db = DB::getConnection();
		$toolsList = array();
		$sql="SELECT id, tool_name, view_all, view_dealer FROM dealer_tools WHERE tool_view_block=:tool_view_block ORDER BY tool_sort";
		$result = $db->prepare($sql);
		$result->bindParam(':tool_view_block',$block,PDO::PARAM_STR);
		$result->execute();
		while ($row = $result->fetch()){
			$toolsList[] = [
				'id' => $row['id'],
				'tool_name' => $row['tool_name'],
				'view_all' => $row['view_all'],
				'view_dealer' => $row['view_dealer']
			];
		}
		return $toolsList;
	}
	public static function getBlocksList () {

		$db = DB::getConnection();

		$blocksList = array();

		$result = $db->query("SELECT id, block_name, view_all, view_dealer FROM dealer_tools_block");

		while ($row = $result->fetch()){
			$blocksList[] = [
				'id' 			=> $row['id'],
				'block_name' 	=> $row['block_name'],
				'view_all' 		=> $row['view_all'],
				'view_dealer'	=> $row['view_dealer']
			];
		}

		return $blocksList;
	}
	public static function getStatusList(){

		$db = DB::getConnection();

		$statusList = array();

		$result = $db->query("SELECT id, status FROM dealer_status");

		while ($row = $result->fetch()){
			$statusList[] = [
				'id' 		=> $row['id'],
				'status' 	=> $row['status']
			];
		}

		return $statusList;
	}
	public static function getCsDsmed ( int $broneId ) {

		$db = DB::getConnection();

		$sql = "SELECT dpt.id, dt.csdsmd FROM dealer_projects_tools AS dpt
				INNER JOIN dealer_tools AS dt ON (dt.id = dpt.tools_id)
				WHERE dpt.id = :broneId";

		$result = $db->prepare($sql);
		$result->bindParam(':broneId', $broneId, PDO::PARAM_INT);
		$result->execute();
		$csdsmd = $result->fetch ();

		return $csdsmd ? intval ( $csdsmd['csdsmd'] ) : false;
	}
	public static function getNowStatus ( int $bronId ) {

		$db = DB::getConnection();

		$sql = "SELECT status_id FROM dealer_projects_tools WHERE id = :toolId";
		$result = $db->prepare($sql);
		$result->bindParam(':toolId', $bronId , PDO::PARAM_INT);
		$result->execute();
		$statusId = $result->fetch();

		return $statusId ? intval( $statusId['status_id'] ) : false;

	}
	public static function getCABAE ( int $dealerId ) {
		$db = DB::getConnection();
		$sql = "SELECT COUNT(p.id) AS COUNT FROM dealer_projects AS p WHERE p.dealer = :dealerId AND p.id IN (SELECT project_id FROM dealer_projects_tools AS dpt WHERE dpt.status_id = 8)";
		$result = $db->prepare($sql);
		$result->bindParam(':dealerId', $dealerId , PDO::PARAM_INT);
		$result->execute();
		$id = $result->fetch ();
		return $id ? intval ( $id['COUNT'] ) : false;
	}
	public static function getCABRQ ( int $dealerId ) {
		$db = DB::getConnection();
		$sql = "SELECT COUNT(p.id) AS COUNT FROM dealer_projects AS p WHERE p.dealer = :dealerId AND p.id IN (SELECT project_id FROM dealer_projects_tools AS dpt WHERE dpt.status_id = 9)";
		$result = $db->prepare($sql);
		$result->bindParam(':dealerId', $dealerId , PDO::PARAM_INT);
		$result->execute();
		$id = $result->fetch ();
		return $id ? intval ( $id['COUNT'] ) : false;
	}
	public static function checkRTS ( int $set, int $new ) {

		# Rule Transition to Status ( Правила перехода из статуса в статус )

		$RTS = include_once ROOT.'/config/status.php';

		# Разрешен ли переход из текущего статуса в новый ( 1 = разрешено )
		# -1 т.к. отсчёт массива начинается с нуля

		if ( $RTS[$set][$new-1] === 0 ) throw new Exception(' Переход в выбранный вами статус невозможен! ');

		return true;

	}
	public static function checkCorrectStatus ( int $new ) {

		if ( $new === 0 || $new < 0 ||  $new > 9) throw new Exception(' Не допустимое значение! ');

		# Переходы в статусы "Не обработано" и "Авторизация просрочена" разрешены только ситстеме

		if ( $new === 1 || $new === 8 ) throw new Exception(' Переходы в статусы "Не обработано" и "Авторизация просрочена" невозможны! ');

		return true;

	}
	public static function checkTheSame ( int $set, int $new ) {

		if ( $set === $new ) throw new Exception('Вы пытаетесь установить новый статус равный текущему!');

		return true;

	}
	public static function canSetStatus ( int $userRights, int $new ) {

		$can = include_once ROOT.'/config/canSetStatus.php';

		# Разрешено ли пользователю с данными правами менять этот статуст ( 1 = разрешено )
		# -1 т.к. отсчёт массива начинается с нуля

		if ( $can[$userRights][$new-1] === 0 ) throw new Exception(' К сожалению, у вас не достаточно прав для изменения текущего статуса авторизации! ');

		return true;

	}
	public static function canChengeStatus ( int $userRights, int $set ) {

		$can = include_once ROOT.'/config/canChangeStatus.php';

		# Разрешено ли пользователю с данными правами поменять текущий статус на выбранный ( 1 = разрешено )
		# -1 т.к. отсчёт массива начинается с нуля

		if ( $can[$userRights][$set-1] === 0 ) throw new Exception(' К сожалению, вы не можете изменить текущий статус на выбранный!');

		return true;

	}
	public static function getReceiverByTools( int $projectId, $receiverId ) {

		$db = DB::getConnection();

		$receivers = array();

		$sql = "SELECT dr.id, dr.fio, dr.email FROM dealer_projects_tools AS dpt
				INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id)
				INNER JOIN dealers_receivers AS dr ON (dr.id=:receiverId)
				WHERE dpt.project_id = :projectId AND dt.tool_send_by <> 1
				GROUP BY dr.id
				ORDER BY dpt.id";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId, PDO::PARAM_INT);
		$result->bindParam(':receiverId', $receiverId, PDO::PARAM_INT);
		$result->execute();

		while ($row = $result->fetch()){
			$receivers[0]=[
				'id' => $row['id'],
				'fio' => $row['fio'],
				'email' => $row['email']
			];
		}

		return $receivers;
	}
	public static function getReceiversByTools( int $projectId ) {

		$db = DB::getConnection();

		$receivers = array();

		$sql = "SELECT dr.id, dr.fio, dr.email FROM dealer_projects_tools AS dpt
				INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id)
				INNER JOIN dealers_receivers AS dr ON (dr.id=dt.tool_send_by)
				WHERE dpt.project_id = :projectId AND dt.tool_send_by <> 1
				GROUP BY dr.id
				ORDER BY dpt.id";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId, PDO::PARAM_STR);
		$result->execute();

		while ($row = $result->fetch()){
			$receivers[]=[
				'id' => $row['id'],
				'fio' => $row['fio'],
				'email' => $row['email']
			];
		}

		return $receivers;
	}
	public static function getReceiverTools( int $projectId, $receivers ) {

		$db = DB::getConnection();

		foreach ($receivers as $key => $receiver) {
			$sql = "SELECT dt.tool_name FROM dealer_projects_tools AS dpt
					INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id)
					INNER JOIN dealers_receivers AS dr ON (dr.id=dt.tool_send_by)
					WHERE dpt.project_id = :projectId AND dt.tool_send_by = :receiver";

			$result = $db->prepare($sql);
			$result->bindParam(':projectId', $projectId, PDO::PARAM_INT);
			$result->bindParam(':receiver', $receiver['id'], PDO::PARAM_INT);
			$result->execute();
			while ($row = $result->fetch()){
				$receivers[$key]['tools'][]= $row['tool_name'];
			}
		}

		return $receivers;
	}
	public static function getToolsReceiversListByToolsIds($ids){

		$db = DB::getConnection();

		$toolsReceiversList = array();

		$sql = "SELECT t.id, t.tool_name, r.fio, r.email, r.send_table, r.send_word, r.send_email, t.tool_send_by FROM dealer_tools AS t
				INNER JOIN dealers_receivers AS r ON t.tool_send_by = r.id WHERE t.id IN ($ids)";

		$result = $db->prepare($sql);
		$result->execute();

		while ($row = $result->fetch()){
			$toolsReceiversList[] = [
				'id' => intval($row['id']),
				'tool_name' => $row['tool_name'],
				'fio' => $row['fio'],
				'email' => $row['email'],
				'send_table' => intval($row['send_table']),
				'send_word' => intval($row['send_word']),
				'send_email' => intval($row['send_email']),
				'tool_send_by' => intval($row['tool_send_by'])
			];
		}

		return $toolsReceiversList;
	}
	public static function ProjectSearch($sp,$userGroup){
		$projectList = array();
		$required = $sp[0];
		preg_match('/project|toolblock/i',$required[0],$pt)?$pt=$pt[0]:$pt=false;
		# По дефолту сортировка ASC
		preg_match('/ASC|DESC/i',$required[1],$sort)?$sort=$sort[0]:$sort='ASC';
		# По дефолту выводим 30 проектов
		preg_match('/[1-9][0-9]*/',$required[2],$limit)?$limit='LIMIT '.$limit[0]:$limit='';
		$fields = $sp[1];
		!empty($sp[2])?$searchStr = htmlspecialchars(trim($sp[2])):$searchStr=false;
		if($pt){
			$db = DB::getConnection();
			$userId=User::getUserId();
			if($pt=='project'){
				preg_match('/id|date|dealer|manager|client|inn|comment|address/i',$fields[0],$field)?$field=$field[0]:$field=false;
				if($field){
					switch($field){
						case 'id': preg_match('/[0-9]*/',$searchStr,$str)?$searchStr=$str[0]:$searchStr=false;
								   if(!$searchStr){
									   throw new Exception('Поисковой запрос должен содержать цифры!');
								   }
								   $search="p.id = :searchStr";break;
						case 'date':$searchStr="%$searchStr%";$search="p.date LIKE :searchStr";break;
						case 'dealer': if($userGroup==1||$userGroup==2){
										  $searchStr="%$searchStr%";
										  $search="dd.dealer_brend_name LIKE :searchStr OR dd.dealer_ur_name LIKE :searchStr";
										  break;
									  }else{
										throw new Exception('Не достаточно прав!');
									  }
						case 'manager': $searchStr=false;
										$notFound=false;
										preg_match('/[1-9][0-9]*/',$fields[1],$managerId)?$managerId=intval($managerId[0]):$managerId=false;
										if($managerId){
											$sql = "SELECT manager_id FROM dealers_dealer_manager WHERE dealer_id = :userid";
											$result = $db->prepare($sql);
											$result->bindValue(':userid', $userId, PDO::PARAM_INT);
											$result->execute();
											while($row = $result->fetch()){
												if(intval($row['manager_id'])===$managerId){$notFound=false;break;}
												else $notFound=true;
											}
										}
										if(!$managerId||$notFound){throw new Exception('Введены некорректные данные сутрудника.');}
										$search="p.dealer_manager = :managerId";break;
						case 'client':$searchStr="%$searchStr%";$search="p.brend_name LIKE :searchStr OR p.ur_name LIKE :searchStr";break;
						case 'inn': preg_match('/[0-9]*/',$searchStr,$str)?$searchStr=$str[0]:$searchStr=false;
									 if(!$searchStr){
  									   throw new Exception('Поисковой запрос должен содержать цифры!');
  								   	 }
									 $searchStr="%$searchStr%";
									 $search = "p.inn LIKE :searchStr";break;
						case 'comment':$searchStr="%$searchStr%";$search = "p.comment LIKE :searchStr";break;
						case 'address':$searchStr="%$searchStr%";$search = 'p.id in (SELECT project_id
																					  FROM dealers_projects_addresses
																					  WHERE region LIKE :searchStr
																					  OR city LIKE :searchStr
																					  OR street LIKE :searchStr
																					  OR house LIKE :searchStr
																					  OR block LIKE :searchStr
																					  OR office LIKE :searchStr)';break;
					}
				}else{
					throw new Exception('Не выбрано поле поиска!');
				}
			}
			if($pt=='toolblock'){
				# Проверяем $toolBlockId
				$notFound=false;
				preg_match('/[1-9][0-9]*/',$fields[0],$toolBlockId)?$toolBlockId=intval($toolBlockId[0]):$toolBlockId=false;
				if($toolBlockId){
					$blockList=self::getBlocksList();
					foreach($blockList as $id=>$val){
						if(intval($val['id'])===$toolBlockId){
							$notFound=false;
							if($userGroup==1||$userGroup==2){if(!intval($val['view_all']))$toolBlockId=false;}
							else{if(!intval($val['view_all'])&&!intval($val['view_dealer']))$toolBlockId=false;}
							break;
						}else{$notFound=true;}
					}
				}
				# Откидываем варианты отсутствия и пустого $toolBlockId, $toolBlockId==false
				if(!empty($toolBlockId)&&!$notFound){
					# Проверяем $toolId
					$notFound=false;
					$search="p.id IN (SELECT project_id FROM dealer_projects_tools AS dpt INNER JOIN dealer_tools AS dt ON (dt.id=dpt.tools_id) WHERE dt.tool_view_block = :tvb";
					preg_match('/[1-9][0-9]*/',$fields[1],$toolId)?$toolId=intval($toolId[0]):$toolId=false;
					if($toolId){
						$toolsList = self::getToolsOnBlockList($toolBlockId);
						foreach($toolsList as $id=>$val){
							if(intval($val['id'])===$toolId){
								$notFound=false;
								if($userGroup==1||$userGroup==2){if(!intval($val['view_all']))$toolId=false;}
								else{if(!intval($val['view_all'])&&!intval($val['view_dealer']))$toolId=false;}
								break;
							}else{$notFound=true;}
						}
						if($toolId&&!$notFound){$search.=" AND tools_id=:toolId";}
						else{$toolId=false;}
					}
					# Проверяем $statusId
					$notFound=false;
					preg_match('/[1-9][0-9]*/',$fields[2],$statusId)?$statusId=intval($statusId[0]):$statusId=false;
					if($statusId){
						$statusList=Project::getStatusList();
						foreach($statusList as $id=>$val){
							if(intval($val['id'])===$statusId){$notFound=false;break;}
							else{$notFound=true;}
						}
						if($statusId&&!$notFound){
							$search.=" AND status_id=:statusId)";
						}else{
							$statusId=false;
							$search.=")";
						}
					}else{$search.=")";}
				}else{throw new Exception('Не выбран тип оборудования!');}
			}
			if($userGroup==1||$userGroup==2){
				$where = "WHERE ".$search;
			}else{
				$where = "WHERE (".$search.") AND p.dealer=:dealerId";
			}
			$sql = "SELECT p.id, p.date, p.brend_name, p.ur_name, p.inn, p.comment, p.responsible
					,dd.id AS d_id,dd.dealer_brend_name,dd.dealer_ur_name,ddm.manager_id,ddm.manager_second_name,ddm.manager_name,ddm.manager_patronymic
					,(SELECT GROUP_CONCAT(CONCAT(IFNULL(a.region, 'region_null'), ', ', a.city, ', улица ', a.street, ', дом ', a.house, ', корпус ', IFNULL(a.block, 'block_null'), ', офис ', IFNULL(a.office, 'office_null')) SEPARATOR '; ') FROM dealers_projects_addresses a WHERE a.project_id = p.id) AS address
					,(SELECT GROUP_CONCAT(dpt.id, ';',dpt.tools_id,';',dpt.status_id SEPARATOR '+')FROM dealer_projects_tools dpt WHERE dpt.project_id = p.id)AS brons_params
					FROM `dealer_projects` AS p
					INNER JOIN dealers_dealer AS dd ON (dd.id = p.dealer)
					INNER JOIN dealers_dealer_manager AS ddm ON (ddm.manager_id = p.dealer_manager)
					$where
					ORDER BY p.id $sort
					$limit";
			//var_dump($sql);
			//$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
			$result = $db->prepare($sql);
			if(isset($searchStr)&&$searchStr){
				//echo"<br>$searchStr<br>";
				$result->bindValue(':searchStr', $searchStr, PDO::PARAM_STR);
			}
			if(isset($managerId)&&$managerId){
				//echo"$managerId<br>";
				$result->bindValue(':managerId', $managerId, PDO::PARAM_STR);
			}
			if(isset($toolBlockId)&&$toolBlockId){
				//echo"toolBlockId = $toolBlockId<br>";
				$result->bindValue(':tvb', $toolBlockId, PDO::PARAM_INT);
			}
			if(isset($toolId)&&$toolId){
				//echo"toolId - $toolId<br>";
				$result->bindValue(':toolId', $toolId, PDO::PARAM_INT);
			}
			if(isset($statusId)&&$statusId){
				//echo"statusId - $statusId<br>";
				$result->bindValue(':statusId', $statusId, PDO::PARAM_INT);
			}
			if($userGroup!=1&&$userGroup!=2){
				//echo"userGroup- $userGroup<br>";
				$result->bindValue(':dealerId', $userId, PDO::PARAM_INT);
			}
			$result->execute();
			while ($row = $result->fetch()){
				$projectList[] = [
					'p_id' => $row['id'],
					'date' => date ( 'd-m-Y', strtotime ( $row['date'] ) ),
					'time' => date ( 'H:i', strtotime ( $row['date'] ) ),
					'cl_ur_name' => str_replace(' (  )', '' , $row['brend_name'].' ('.$row['ur_name'].')'),
					'inn' => $row['inn'],
					'comment' => $row['comment'],
					'responsible' => $row['responsible'],
					'd_id' => $row['d_id'],
					'dealer' => $row['dealer_brend_name'].' ('.$row['dealer_ur_name'].')',
					'manager_id' => $row['manager_id'],
					'manager' => $row['manager_second_name'].' '.$row['manager_name'].' '.$row['manager_patronymic'],
					'address' =>  str_replace( ', корпус block_null', '', str_replace(', офис office_null', '', (str_replace('region_null, ', '', $row['address'])))),
					'brons_params' => $row['brons_params']
			    ];
			}
		}else{
			throw new Exception('Некорректный поисковой запрос!');
		}
		return $projectList;
	}
	public static function createProject($options){

		$db = DB::getConnection();

		$sql = "INSERT INTO dealer_projects (
			brend_name,
			ur_name,
			inn,
			comment,
			date,
			dealer,
			dealer_manager,
			type
		) VALUES (
			:brend_name,
			:ur_name,
			:inn,
			:comment,
			NOW(),
			:dealer,
			:dealer_manager,
			:type
		)";

		// $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
		$result = $db->prepare($sql);

		$result->bindParam(':brend_name', $options['clinics_name'], PDO::PARAM_STR);
		$result->bindParam(':ur_name', $options['ur_name'], PDO::PARAM_STR);
		$result->bindParam(':inn', $options['inn'], PDO::PARAM_STR);
		$result->bindParam(':comment', $options['comment'], PDO::PARAM_STR);
		$result->bindParam(':dealer', $options['dealer-id'], PDO::PARAM_INT);
		$result->bindParam(':dealer_manager', $options['manager-id'], PDO::PARAM_INT);
		$result->bindParam(':type', $options['ur_name_type'], PDO::PARAM_INT);

		if ($result->execute()) {
			$lastInsertId = $db->lastInsertId();

			$query = "INSERT INTO dealer_projects_tools (project_id, tools_id, status_id) VALUES ";
			$qPart = array_fill(0, count($options['tools']), "(?, ?, ?)");
			$query .=	implode(",",$qPart);
			$stmt = $db->prepare($query);
			$i = 1;
			foreach($options['tools'] as $tool) {
				$stmt->bindValue($i++, $lastInsertId);
				$stmt->bindValue($i++, $tool);
				$stmt->bindValue($i++, 1);
			}
			if($stmt->execute()!==true){
				return false;
			}

			$query = "INSERT INTO dealers_projects_addresses (region, city, street, house, office, project_id) VALUES ";
			$qPart = array_fill(0, count($options['addresses']), "(?, ?, ?, ?, ?, ?)");
			$query .= implode(",",$qPart);
			$stmt = $db->prepare($query);

			$i = 1;
			foreach($options['addresses'] as $address) {
				$stmt->bindValue($i++, $address['region']);
				$stmt->bindValue($i++, $address['city']);
				$stmt->bindValue($i++, $address['street']);
				$stmt->bindValue($i++, $address['house']);
				$stmt->bindValue($i++, $address['office']);
				$stmt->bindValue($i++, $lastInsertId);
			}
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		}

		return false;
	}
	public static function getProjectResponsible (int $projectId ) : string {
		$db = DB::getConnection();

		$responsible = null;

		$sql = "SELECT dealers_dealer_manager.* FROM dealer_projects
						INNER JOIN dealers_dealer_manager ON dealers_dealer_manager.manager_id = dealer_projects.responsible
						WHERE dealer_projects.id = :projectId";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId, PDO::PARAM_INT);
		$result->execute();
		
		$responsible = $result->fetch ();

		return $responsible ? $responsible['manager_second_name'] . ' ' . $responsible['manager_name'] : '';

	}
	public static function getProjectResponsiblePhone (int $projectId ) : string {
		$db = DB::getConnection();

		$responsible = null;

		$sql = "SELECT dealers_dealer_manager.* FROM dealer_projects
						INNER JOIN dealers_dealer_manager ON dealers_dealer_manager.manager_id = dealer_projects.responsible
						WHERE dealer_projects.id = :projectId";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId, PDO::PARAM_INT);
		$result->execute();
		
		$responsible = $result->fetch ();

		return $responsible ? $responsible['manager_phone'] : '';

	}
	public static function changeProjectStatus ( $authorizations ) {

		if ( ! is_array($authorizations) || count($authorizations) !== 4 )
			throw new Exception('Не верный тип данных AUTHORIZATIONS!');

		# Блок PROJECTID -->

		if ( ! isset ( $authorizations['projectId'] ) || ! is_int ( $authorizations['projectId'] ) )
			throw new Exception('Не верный тип данных PROJECT!');

		# Блок PROJECTID <--
		# Блок TOOLS -->

		if ( ! array_key_exists('tools', $authorizations) || ! is_array($authorizations['tools']) || count($authorizations['tools']) === 0 )
			throw new Exception('Не верный тип данных TOOLS!');

		# Блок TOOLS <--
		# Блок STATUS -->

		if ( ! isset ( $authorizations['statusId'] ) || ! is_int ( $authorizations['statusId'] ) )
			throw new Exception('Не верный тип данных STATUS!');

		# Блок STATUS <--
		# Блок COMMENTS -->

		if ( ! isset($authorizations['comment']) )
			throw new Exception('Не верный тип данных COMMENT!');

		$authorizations['comment'] = trim( htmlspecialchars( strip_tags( $authorizations['comment'] ) ) );

		# Блок COMMENTS <--

		$projectType = self::getProjectType( $authorizations['projectId'] );
		$projectResponsible = self::getProjectResponsible( $authorizations['projectId'] );
		$projectResponsiblePhone = self::getProjectResponsiblePhone( $authorizations['projectId'] );

		$commentDate = ( new DateTime )->format( 'd.m.Y H:i:s' );
		$toolStatus  = '';
		$userGroup 	 = User::getUserGroup();
		$manager 	 = Dealer::getManagerFromProject ( $authorizations['projectId'] );
		$addresses 	 = self::getProjectAddresses ( $authorizations['projectId'] );
		$brendName 	 = self::getProjectBrendName ( $authorizations['projectId'] );
		$uName 		 = self::getProjectUName ( $authorizations['projectId'] );

		switch ( $authorizations['statusId'] ) {

			case 2: $statusName = 'авторизовано'; break;
			case 3: $statusName = 'работает другая компания'; break;
			case 4: $statusName = 'реализовано'; break;
			case 5: $statusName = 'не реализовано'; break;
			case 6: $statusName = 'авторизация прекращена'; break;
			case 7: $statusName = 'авторизация продлена'; break;
			case 9: $statusName = 'запрос актуальности'; break;
			default: error_log( "[CHANGEPROJECTSTATUS] - попытка взлома.", 3, ROOT."/log/attack.log"); break;
		}

		$db = DB::getConnection();

		foreach ($authorizations['tools'] as $id => $bronId) {

			# Установленный (текущий) статус
			$set = self::getNowStatus ( $bronId );

			# Проверка корректность переданного статуса
			self::checkCorrectStatus ( $authorizations['statusId'] );

			# Может ли Дилер с данными ему правами менять текущий статус на выбранный
			self::canChengeStatus ( $userGroup, $set );

			# Запрещено устанавливать новый статус равный текущему, кроме статуса "Авторизация продлена"
			if ( $authorizations['statusId'] !== 7 ) self::checkTheSame ( $set, $authorizations['statusId'] );

			# Может ли Дилер с данными ему правами устанавливать выбранный статус
			self::canSetStatus ( $userGroup, $authorizations['statusId'] );

			# Проверка перехода из статуса в статус
			self::checkRTS  ( $set, $authorizations['statusId'] );

			if ( $authorizations['statusId'] !== 2 ) {

				if ( strlen ( $authorizations['comment'] ) < 5 )
					throw new Exception('Слишком короткий комментарий');

			}

			if ( $userGroup === 1 && $userGroup === 2) {

				try {

					$db->beginTransaction();

					$sql = "UPDATE dealer_projects_tools SET status_id = :statusId WHERE id = :bronId";
					$result = $db->prepare($sql);
					$result->bindParam(':bronId', $bronId, PDO::PARAM_INT);
					$result->bindParam(':statusId', $authorizations['statusId'], PDO::PARAM_INT);
					$result->execute();

					$sql = "INSERT INTO dealer_status_comments(tools_id, status_id, comment, comment_date ) VALUES (:tool_id, :statusId, :statusComment, :comment_date )";
					$result = $db->prepare($sql);
					$result->bindParam(':tool_id', $bronId, PDO::PARAM_INT);
					$result->bindParam(':statusId', $authorizations['statusId'], PDO::PARAM_INT);
					$result->bindParam(':statusComment', $authorizations['comment'], PDO::PARAM_STR);
					$result->bindParam(':comment_date', $commentDate, PDO::PARAM_STR);
					$result->execute();

					error_log( "[Method - changeProjectStatus] - успешно выполнен. Дата изменения статуса: $commentDate\n ID Брони: $bronId\n ID статуса: ".$authorizations['statusId']."\n Комментарий: ".$authorizations['comment']."\n", 3, ROOT."/log/project.log");

					$db->commit();

					$toolId = self::getToolId ( $bronId );
					$toolName = self::getToolName ( $toolId );
					$toolStatus .= $toolName . ' - ' . $statusName . ';<br>';

				}

				catch ( Exception $e ) {

					$db->rollBack();

					error_log( "[Method - changeProjectStatus] - не отработал корректно. Дата дата вызова метода: $commentDate\n ID Брони: $bronId\n ID статуса: ".$authorizations['statusId']."\n Комментарий: ".$authorizations['comment']."\n Доп. информация об ошибке: ".$e->getMessage(), 3, ROOT."/log/project.log");
				}

			}

			if ( $userGroup === 3 && self::getCsDsmed( $bronId ) ) {

				try {

					$db->beginTransaction();

					$sql = "UPDATE dealer_projects_tools SET status_id = :statusId WHERE id = :bronId";
					$result = $db->prepare($sql);
					$result->bindParam(':bronId', $bronId, PDO::PARAM_INT);
					$result->bindParam(':statusId', $authorizations['statusId'], PDO::PARAM_INT);
					$result->execute();

					$sql = "INSERT INTO dealer_status_comments(tools_id, status_id, comment, comment_date ) VALUES (:tool_id, :statusId, :statusComment, :comment_date )";
					$result = $db->prepare($sql);
					$result->bindParam(':tool_id', $bronId, PDO::PARAM_INT);
					$result->bindParam(':statusId', $authorizations['statusId'], PDO::PARAM_INT);
					$result->bindParam(':statusComment', $authorizations['comment'], PDO::PARAM_STR);
					$result->bindParam(':comment_date', $commentDate, PDO::PARAM_STR);
					$result->execute();

					error_log( "[Method - changeProjectStatus] - успешно выполнен. Дата изменения статуса: $commentDate\n ID Брони: $bronId\n ID статуса: ".$authorizations['statusId']."\n Комментарий: ".$authorizations['comment']."\n", 3, ROOT."/log/project.log");

					$db->commit();

					$toolId = self::getToolId ( $bronId );
					$toolName = self::getToolName ( $toolId );
					$toolStatus .= $toolName . ' - ' . $statusName . ';<br>';

				}

				catch ( Exception $e ) {

					$db->rollBack();

					error_log( "[Method - changeProjectStatus] - не отработал корректно. Дата дата вызова метода: $commentDate\n ID Брони: $bronId\n ID статуса: ".$authorizations['statusId']."\n Комментарий: ".$authorizations['comment']."\n Доп. информация об ошибке: ".$e->getMessage(), 3, ROOT."/log/project.log");
				}

			} else {

				try {

					$db->beginTransaction();

					$sql = "UPDATE dealer_projects_tools SET status_id = :statusId WHERE id = :bronId";
					$result = $db->prepare($sql);
					$result->bindParam(':bronId', $bronId, PDO::PARAM_INT);
					$result->bindParam(':statusId', $authorizations['statusId'], PDO::PARAM_INT);
					$result->execute();

					$sql = "INSERT INTO dealer_status_comments(tools_id, status_id, comment, comment_date ) VALUES (:tool_id, :statusId, :statusComment, :comment_date )";
					$result = $db->prepare($sql);
					$result->bindParam(':tool_id', $bronId, PDO::PARAM_INT);
					$result->bindParam(':statusId', $authorizations['statusId'], PDO::PARAM_INT);
					$result->bindParam(':statusComment', $authorizations['comment'], PDO::PARAM_STR);
					$result->bindParam(':comment_date', $commentDate, PDO::PARAM_STR);
					$result->execute();

					error_log( "[Method - changeProjectStatus] - успешно выполнен. Дата изменения статуса: $commentDate\n ID Брони: $bronId\n ID статуса: ".$authorizations['statusId']."\n Комментарий: ".$authorizations['comment']."\n", 3, ROOT."/log/project.log");

					$db->commit();

					$toolId = self::getToolId ( $bronId );
					$toolName = self::getToolName ( $toolId );
					$toolStatus .= $toolName . ' - ' . $statusName . ';<br>';

				}

				catch ( Exception $e ) {

					$db->rollBack();

					error_log( "[Method - changeProjectStatus] - не отработал корректно. Дата дата вызова метода: $commentDate\n ID Брони: $bronId\n ID статуса: ".$authorizations['statusId']."\n Комментарий: ".$authorizations['comment']."\n Доп. информация об ошибке: ".$e->getMessage(), 3, ROOT."/log/project.log");
				}

			}

		}

		$rAddr = '';

		foreach ($addresses as $id => $address) {

			$rAddr .= ( !empty ( $address['region'] ) ? ucfirst ( $address['region'] ).', ' : '' );
			$rAddr .= ucfirst ( $address['city'] ).', ';
			$rAddr .= ucfirst ( $address['street'] ).', ';
			$rAddr .= ( !empty ( $address['house'] ) ? ucfirst ( $address['house'] ).', ' : '' );
			$rAddr .= ( !empty ( $address['block'] ) ? ucfirst ( $address['block'] ).', ' : '' );
			$rAddr .= ( !empty ( $address['office'] ) ? ucfirst ( $address['office'] ).', ' : '' );
			$rAddr .= '; ';

			$rAddr = str_replace ( ', ;', ';', $rAddr );

		}

		$TYPE = 2;
		$FROM = '';
		$THEME = 'Смена статуса';
		$bodyMail = 'Уважаемый(ая) '.ucfirst($manager['name']).' '.( !empty( $manager['patronymic'] ) ? ucfirst($manager['patronymic']) : '' ).'!<br>
			По проекту '.$brendName.' ('.$uName.')'.' по адресу(ам) '.$rAddr.' у следующего оборудования изменился статус:<br>
			'.$toolStatus.'
			---<br>
			По всем вопросам и пожеланиям просим обращаться:<br>
			' . $projectResponsible . '<br>
			специалист отдела продаж ООО "ДС-Мед"<br>
			+7(495)230-11-55, ' . $projectResponsiblePhone . ',<br>
			opt@ds-med.ru';

		# Если статуст меняет дилер

		if ( $userGroup ===  4 ) {

			if ( $authorizations['statusId'] !== 7 || $authorizations['statusId'] !== 9 ) Site::addMailRecord($TYPE, $manager['id'], $FROM, $THEME, $bodyMail, 1);
		}
		else {

			if ( $authorizations['statusId'] === 5 || $authorizations['statusId'] == 6 )
			Site::addMailRecord($TYPE, $manager['id'], $FROM, $THEME, $bodyMail, 1);

		}

		return true;
	}
	public static function addToSendmail ($type, $projectId, $mails) {
		# Источник письма. 1 == Запрос авторизации
		$TYPE = 1;
		# По умолчанию все автоматические запросы идут от Брагинец Антон
		$MANAGERID = 2;
		# 1 == Можно отправлять.
		$FLAGSEND = 1;
		# На будущее
		$VISIBILITY = 1;

		if(!preg_match('/[0-9]*/',$projectId,$res)){
			throw new Exception('Не верный формат данных!');
		}
		$projectId = intval($res[0]);
		# Дата формирования письма
		$date = date( 'd.m.y H:i:s' );
		$project = self::getProjectByQueryAuth($projectId);
		$addresses = explode(';',$project['address']);
		$db = DB::getConnection();

		foreach ($mails as $id => $mail) {
			# Формируем письмо
			$body='';
			$toolsForDoc = '';
			$mail['id'] = intval($mail['id']);
			$receiver = self::getReceiverTools($projectId,self::getReceiverByTools($projectId, $mail['id']));
			$body = $mail['header'];
			$body .= '<br>Внесите, пожалуйста, в реестр проектов поставку следующих позиций оборудования:<br>';
			$i = 1;
			foreach ($receiver[0]['tools'] as $id => $tool) {
				$body .=$i.'. '.$tool.';<br>';
				$toolsForDoc .= $tool.", ";
				$i++;
			}
			$body .= 'Оборудование планируется к поставке в '.$project['brend_name'].'('.$project['ur_name'].') по адресу(ам):<br>';
			$i = 1;
			foreach ($addresses as $id => $address) {
				$body .=$i.'. '.$address.';<br>';
				$i++;
			}
			$body .='ИНН: '.$project['inn'];
			$body .=!empty($mail['comment'])?'<br>'.$mail['comment'].'<br>':'';
			$body .='<br>'.$mail['signature'].'<br>';

			#Форирую данные для таблицы или ворда
			$projectForFile[0] = $project;
			$projectForFile[0]['dealer_brend_name'] = 'DS-Med';
			$projectForFile[0]['dealer_ur_name'] = 'ООО ДС-Мед';
			$projectForFile[0]['manager'] = 'Антон Брагинец';
			$projectForFile[0]['tools'] = $toolsForDoc;

			if($mail['id'] !== 5){
				$url = Site::createTable($projectForFile);
			}else{
				# Для Феникса
				$bronsList = Site::getBronsByPhoenixmed($mail['id']);
				$url = Site::createWordByPhoenixmed($bronsList);
			}

			// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$sql = "INSERT INTO dealer_send_mail(type, d_create, manager_id, addresses, theme, body_mail, file, send, visibility)
					VALUES (:type, :dcreate, :managerId, :addresses, :theme, :bodymail, :file, :flagsend, :visibility)";
			$result = $db->prepare($sql);
			$result->bindParam(':type', $TYPE, PDO::PARAM_INT);
			$result->bindParam(':dcreate', $date, PDO::PARAM_STR);
			$result->bindParam(':managerId', $MANAGERID, PDO::PARAM_INT);
			$result->bindParam(':addresses', $receiver[0]['email'], PDO::PARAM_STR);
			$result->bindParam(':theme', $mail['theme'], PDO::PARAM_STR);
			$result->bindParam(':bodymail', $body, PDO::PARAM_STR);
			$result->bindParam(':file', $url, PDO::PARAM_STR);
			$result->bindParam(':flagsend', $FLAGSEND, PDO::PARAM_INT);
			$result->bindParam(':visibility', $VISIBILITY, PDO::PARAM_INT);
			$return = $result->execute();

		}

		return $return ? true : false;
	}
	public static function deleteProject ( $projectId ) {

		$db = DB::getConnection();

		$sql = "DELETE FROM dealer_projects WHERE id = :projectId";

		$result = $db->prepare($sql);
		$result->bindParam(':projectId', $projectId, PDO::PARAM_STR);
		if ($result->execute()) {
			return true;
		}

		return false;
	}
	public static function updateProjectResponsible ( int $projectId, int $responsibleId ) {

		try {

			$db = DB::getConnection();
			$db->beginTransaction();
			$sql = "UPDATE dealer_projects SET responsible = :responsibleId WHERE id = :projectId ";
			$result = $db->prepare($sql);
			$result->bindValue( ':projectId', $projectId, PDO::PARAM_INT);
			$result->bindValue( ':responsibleId', $responsibleId, PDO::PARAM_INT);
			$result->execute();
			$db->commit();

		}

		catch ( Exception $e ) {

			$db->rollBack();

			error_log ( "[Method - updateProjectResponsible] - не отработал корректно. " . $e->getMessage(), 3, ROOT."/log/project.log" );
		}

		return $responsibleId;

	}
	public static function updateProjectManager ( int $projectId, int $dealerId, int $managerId ) {

		try {

			$db = DB::getConnection();
			$db->beginTransaction();
			$sql = "UPDATE dealer_projects SET dealer = :dealerId, dealer_manager = :managerId  WHERE id = :projectId ";
			$result = $db->prepare($sql);
			$result->bindValue( ':projectId', $projectId, PDO::PARAM_INT);
			$result->bindValue( ':dealerId', $dealerId, PDO::PARAM_INT);
			$result->bindValue( ':managerId', $managerId, PDO::PARAM_INT);
			$result->execute();
			$db->commit();

		}

		catch ( Exception $e ) {

			$db->rollBack();

			error_log ( "[Method - updateProjectResponsible] - не отработал корректно. " . $e->getMessage(), 3, ROOT."/log/project.log" );
		}

		return $managerId;

	}
}
