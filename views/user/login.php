<?php include_once ROOT.'/views/layouts/header.php';?>
<main>
    <div class="login-container">
        <form method="post">
            <div class="dsmed_logo"></div>
            <?php if (isset($errors) && is_array($errors)): ?>
                <?php foreach ($errors as $error):?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $error; ?>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <div class="alert alert-warning" role="alert">
                Уважаемые пользователи!<br>
                К сожалению, "Система авторизации проектов" временно не работает в броузере <b><em>Safari</em></b>.<br>
                Приносим свои извинения.<br>
            </div>
            <input class="form-control cfepm" type="text" name="login" value="<?php echo $login;?>" required autofocus placeholder="Логин">
            <input class="form-control cfepm" type="password" name="password" value="<?php echo $password;?>" required placeholder="Пароль">
            <input id="submit-login" type="submit" class="btn btn-lg btn-success btn-block" value="Войти" name="submit" style="margin-bottom:15px;">
            <p class="text-muted">© 2017-<?php echo date("Y");?></p>
        </form>
    </div>
<?php include_once ROOT.'/views/layouts/footer.php';?>
