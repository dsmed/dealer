<?php $title = 'Панель добавления дилера'; ?>
<?php include ROOT . '/views/layouts/header.php'; ?>

	<?php include ROOT.'/views/layouts/top.php';?>

		<div class="d-flex justify-content-center m-0 p-0">
			<?php if ( isset ($_SESSION['add-dealed-succsess']) ) : ?>
				<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
					Дилер успешно добавлен!
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?php unset($_SESSION['add-dealed-succsess']) ?>
			<?php endif ?>
			<?php if ( isset ($_SESSION['add-dealed-error']) ) : ?>
				<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
					К сожалению, что-то пошло не так. Дилер не добавлен.
					<?php foreach ($_SESSION['add-dealed-error'] as $error) : ?>
						<p class="mt-2"><b> - <?=$error?></b></p>
					<?php endforeach ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?php unset($_SESSION['add-dealed-error']) ?>
			<?php endif ?>
		</div>

		<div class="p-3">
			<div class="container rounded p-3" style="border: 1px solid lightgray; background-color: white;">
				<form method="post" class="p-3">
					<div class="row">
						<div class="col-md-4">
							<h4 class="text-color-dsmed">Дилер</h4>
							<div class="form-group">
								<label for="brend" class="mt-1 mb-1">Бренд</label>
								<input id="brend" class="form-control" type="text" name="dealer_brend_name" value="<?php if(isset($options['dealer_brend_name'])){echo $options['dealer_brend_name']; } ?>"  placeholder="DS-Med" autocomplete="off" autofocus>
								<label for="ur-name" class="mt-1 mb-1">Юридическое лицо</label>
								<input id="ur-name" class="form-control" type="text" name="dealer_ur_name" value="<?php if(isset($options['dealer_ur_name'])){ echo $options['dealer_ur_name']; } ?>"  placeholder="ООО ДС-Мед" autocomplete="off" required>
								<label for="inn" class="mt-1 mb-1">ИНН</label>
								<input id="inn" class="form-control dealer_inn-mask" type="text" name="dealer_inn" value="<?php if(isset($options['dealer_inn'])){echo $options['dealer_inn']; } ?>" placeholder="123 456 789 101" autocomplete="off" required>
								<label for="region" class="mt-1 mb-1">Регион</label>
								<input id="region" class="form-control" type="text" name="address[region]" value="<?php if(isset($options['region'])){echo $options['region']; } ?>" placeholder="Московская область" autocomplete="off" required>
								<label for="city" class="mt-1 mb-1">Город</label>
								<input id="city" class="form-control" type="text" name="address[city]" value="<?php if(isset($options['city'])){echo $options['city']; } ?>" placeholder="Серпухов" autocomplete="off" required>
								<label for="street" class="mt-1 mb-1">Улица</label>
								<input id="street" class="form-control" type="text" name="address[street]" value="<?php if(isset($options['street'])){echo $options['street']; } ?>" placeholder="ш. Борисовское"  autocomplete="off" required>
								<label for="house" class="mt-1 mb-1">Дом</label>
								<input id="house" class="form-control" type="text" name="address[house]" value="<?php if(isset($options['house'])){echo $options['house']; } ?>" placeholder="17"  autocomplete="off">
								<label for="block" class="mt-1 mb-1">Корпус</label>
								<input id="block" class="form-control" type="text" name="address[block]" value="<?php if(isset($options['block'])){echo $options['block']; } ?>" placeholder="Нет"  autocomplete="off">
								<label for="office" class="mt-1 mb-1">Офис</label>
								<input id="office" class="form-control" type="text" name="address[office]" value="<?php if(isset($options['office'])){echo $options['office']; } ?>" placeholder="910"  autocomplete="off">
								<label for="type" class="mt-1 mb-1">Тип</label>
								<?php if(!isset($options['dealer_type'])):?>
									<select id="type" class="form-control" name="dealer_type">
										<option value="A" selected>A</option>
										<option value="B">B</option>
										<option value="C">C</option>
									</select>
								<?php else: ?>
								<?php switch ( $options['dealer_type'] ) {
										case 'A': ?>
											<select id="type" class="form-control" name="dealer_type">
												<option value="A" selected>A</option>
												<option value="B">B</option>
												<option value="C">C</option>
											</select> <?php
											break;
										case 'B': ?>
											<select id="type" class="form-control" name="dealer_type">
												<option value="A">A</option>
												<option value="B" selected>B</option>
												<option value="C">C</option>
											</select><?php
											break;
										case 'C': ?>
											<select id="type" class="form-control" name="dealer_type">
												<option value="A">A</option>
												<option value="B">B</option>
												<option value="C" selected>C</option>
											</select><?php
											break;
										default: ?>
											<select id="type" class="form-control" name="dealer_type">
												<option value="A" selected>A</option>
												<option value="B">B</option>
												<option value="C">C</option>
											</select><?php
											break;
									} ?>
								<?php endif ?>
							</div>
						</div>
						<div class="col-md-4">
							<h4 class="text-color-dsmed">Контакты дилера</h4>
							<div class="form-group">
								<label for="phone" class="mt-1 mb-1">Телефон</label>
								<input id="phone" type="text" name="dealer_phone" class="form-control phone-mask" value="<?php if(isset($options['dealer_phone'])){echo $options['dealer_phone'];}?>" placeholder="+7(495)111-1100" autocomplete="off" required>
								<label for="email" class="mt-1 mb-1">Почта</label>
								<input id="email" class="form-control" type="text" name="dealer_email" value="<?php if(isset($options['dealer_email'])){echo $options['dealer_email'];}?>"  placeholder="admin@ds-med.ru" autocomplete="off" required>
								<label for="site" class="mt-1 mb-1">Сайт</label>
								<input id="site" type="text" name="dealer_site" class="form-control" value="<?php if(isset($options['dealer_site'])){echo $options['dealer_site'];}?>" placeholder="Сайт компании" autocomplete="off" required>
								<label for="comment" class="mt-1 mb-1">Комментарий</label>
								<textarea id="comment" class="form-control" name="dealer_comment" placeholder="Например: дилер с хорошей репутацией" autocomplete="off"><?php if(isset( $options['dealer_comment'] ) ) { echo $options['dealer_comment']; } ?></textarea>
							</div>
						</div>
						<div class="col-md-4">
							<h4 class="text-color-dsmed">Менеджер</h4>
							<div class="form-group">
								<label for="p-fam" class="mt-1 mb-1">Фамилия</label>
								<input id="p-fam" class="form-control" type="text" name="dealer_manager_fam" value="<?php if(isset($options['dealer_manager_fam'])){echo $options['dealer_manager_fam'];}?>"  placeholder="Иванов" autocomplete="off" required>
								<label for="p-name" class="mt-1 mb-1">Имя</label>
								<input id="p-name" class="form-control" type="text" name="dealer_manager_name" value="<?php if(isset($options['dealer_manager_name'])){echo $options['dealer_manager_name'];}?>"  placeholder="Иван" autocomplete="off" required>
								<label for="p-patr" class="mt-1 mb-1">Отчество</label>
								<input id="p-patr" class="form-control" type="text" name="dealer_manager_patr" value="<?php if(isset($options['dealer_manager_patr'])){echo $options['dealer_manager_patr'];}?>"  placeholder="Иванович" autocomplete="off">
								<label for="p-phone" class="mt-1 mb-1">Телефон</label>
								<input id="p-phone" class="form-control" type="text" name="dealer_manager_phone" value="<?php if(isset($options['dealer_manager_phone'])){echo $options['dealer_manager_phone'];}?>" placeholder="+7(495)111-1111" autocomplete="off" required>
								<label for="p-email" class="mt-1 mb-1">Почта</label>
								<input id="p-email" class="form-control" type="text" name="dealer_manager_email" placeholder="manager@ds-med.ru" value="<?php if(isset($options['dealer_manager_email'])){echo $options['dealer_manager_email'];}?>" autocomplete="off" required>
							</div>
						</div>
						<div style="margin:auto;">
							<input type="submit" class="btn btn-success btn-lg" value="Добавить дилера" name="submit">
						</div>
					</div>
				</form>
			</div>
		</div>
	</main>
<?php include ROOT . '/views/layouts/footer.php'; ?>
