<?if(isset($successes)&&is_array($successes)):?>
<div class="company-personal">
	<h3 class="text-color-dsmed">Сотрудник</h3>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        Новый сотрудник успешно добавлен!
    </div>
    <div class="form-group personal">
        <select class="form-control" name="select-personal">
            <option value="false" selected="">Выбрать сотрудника</option>
            <?foreach($dealerManagers as $manager):?>
                <? $id = $manager['id'];?>
                <? $value = $manager['second_name'].' '.$manager['name'].' '.$manager['patronymic']; ?>
                    <option value="<? echo $id; ?>"><? echo $value; ?></option>
            <?endforeach?>
        </select>
    </div>
</div>
<?endif?>

<?if(isset($errors)&&is_array($errors)):?>
	<div class="modal" id="error-add-pesonal" tabindex="-1" role="dialog" aria-labelledby="infotmation-header" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="infotmation-header">Ошибка!</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?if(isset($errors) && is_array($errors)):?>
						<div class="alert alert-danger" role="alert">
							<?foreach($errors as $error): ?>
								<p><? echo $error; ?></p>
							<?endforeach?>
						</div>
					<?endif?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal" name="change-status">Понятно</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		$('#error-add-pesonal').modal('show');
	</script>


        <h3 class="text-color-dsmed">Сотрудник</h3>
        <div class="form-group personal">
            <select class="form-control" name="select-personal">
                <option class="option-style" value="false" selected>Выбрать сотрудника</option>
                    <?foreach($dealerManagers as $manager):?>
                        <? $id = $manager['id'];?>
                        <? $value = $manager['second_name'].' '.$manager['name'].' '.$manager['patronymic']; ?>
                        <option value="<? echo $id; ?>"><? echo $value; ?></option>
                    <?endforeach?>
                </select>
            </div>
            <div class="form-group personal-information">
                <div class="personal-data">
                    <label for="fam" class="sr-only">Фамилия</label>
                    <input id="fam" class="fam pd-field form-control mb-2" type="text" name="new_dealer_manager_fam" value="<?=$new_manager['new_dealer_manager_fam'];?>" placeholder="Фамилия" autocomplete="off" required>
                    <label for="name" class="sr-only">Имя</label>
                    <input id="name" class="name pd-field form-control mb-2" type="text" name="new_dealer_manager_name" value="<?=$new_manager['new_dealer_manager_name'];?>" placeholder="Имя" autocomplete="off" required>
                    <label for="pat" class="sr-only">Отчество</label>
                    <input id="pat" class="pat pd-field form-control mb-2" type="text" name="new_dealer_manager_patr" value="<?=$new_manager['new_dealer_manager_patr'];?>" placeholder="Отчество" autocomplete="off">
                    <label for="phone" class="sr-only">Телефон</label>
                    <input id="phone" class="phone pd-field form-control mb-2" type="text" name="new_dealer_manager_phone" value="<?=$new_manager['new_dealer_manager_phone'];?>" placeholder="Телефон" autocomplete="off" required>
                    <label for="email" class="sr-only">Почта</label>
                    <input id="email" class="email pd-field form-control mb-2" type="text" name="new_dealer_manager_email" value="<?=$new_manager['new_dealer_manager_email'];?>" placeholder="Почта" autocomplete="off" required>
                    <div id="add-personal" class="btn btn-success btn-block">Добавить</div>
                </div>
            </div>


<?endif?>
