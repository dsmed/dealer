<h3 class="text-color-dsmed">Сотрудник</h3>

<div class="form-group personal-information">
    <div class="personal-data">
        <select class="form-control" name="select-personal">
            <option value="<?=$manager['id'];?>" selected><?=ucfirst($manager['second_name']);?> <?=ucfirst($manager['name']);?> <?=ucfirst($manager['patronymic']);?></option>
        </select>
        <label for="phone" class="m-1">Телефон</label>
        <input id="phone" class="form-control phone" type="text" name="manager_phone" value="<?=$manager['phone'];?>" placeholder="Телефон" autocomplete="off" readonly required>
        <label for="email" class="m-1">Почта</label>
        <input id="email" class="form-control email" type="text" name="manager_email" value="<?=$manager['email'];?>" placeholder="Почта"  autocomplete="off" readonly required>
    </div>
</div>
