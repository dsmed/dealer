<?php include ROOT.'/views/layouts/header.php';?>

<?php if ( ( isset ( $successes ) && is_array ( $successes ) ) || ( isset ( $errors ) && is_array ( $errors ) ) ) : ?>

	<!-- Chenge Status Modal -->
	<div class="modal" id="infotmation" tabindex="-1" role="dialog" aria-labelledby="infotmation-header" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="infotmation-header">Сообщение</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php if ( isset ( $successes ) && is_array ( $successes ) ) : ?>
						<div class="alert alert-success" role="alert">
							<?php foreach ( $successes as $success ) : ?>
								<p><?=$success?></p>
							<?php endforeach ?>
						</div>
					<?php endif ?>

					<?php if ( isset ( $errors ) && is_array ( $errors ) ) : ?>
						<div class="alert alert-danger" role="alert">
							<?php foreach ( $errors as $error ) : ?>
								<p><?=$error?></p>
							<?php endforeach ?>
						</div>
					<?php endif ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info" data-dismiss="modal" name="change-status">Понятно</button>
				</div>
			</div>
		</div>
	</div>

<?php endif ?>

<?php include ROOT.'/views/layouts/top.php';?>
<form class="project-add-form" method="post">
	<div class="container-fluid project-add-container rounded">
		<div class="row">
			<div class="order-2 col-md-6 order-md-1 col-lg-3 order-lg-1 ">
				<h3 class="text-color-dsmed">Конечный клиент</h3>
				<div class="form-group mb-0">
					<label for="clinics-name" class="mt-1 mb-1">Название клиники</label>
					<input id="clinics-name" class="form-control" type="text" name="clinics-name" value="<?php  if ( isset ( $options['clinics_name'] ) ) { echo $options['clinics_name']; } ?>" placeholder="Введите название клиники"  autocomplete="off" required>
					<label for="ur_name_type" class="mt-1 mb-1">Тип клиники</label>
					<?php if(!isset($options['ur_name_type'])):?>
						<select id="ur-name-type" class="ur-name_type form-control" name="ur_name_type">
							<option class="option-style" value="1" selected>Частная</option>
							<option class="option-style" value="2">Бюджетная (Аукцион)</option>
						</select>
					<?php else:?>
						<?php switch($options['ur_name_type']): case '1': ?>
							<select id="ur-name-type" class="ur-name_type form-control" name="ur_name_type">
								<option class="option-style" value="1" selected>Частная</option>
								<option class="option-style" value="2">Бюджетная (Аукцион)</option>
							</select>
						<?php case '2': ?>
							<select id="ur-name-type" class="ur-name_type form-control" name="ur_name_type">
								<option class="option-style" value="1">Частная</option>
								<option class="option-style" value="2" selected>Бюджетная (Аукцион)</option>
							</select>
						<?php endswitch ?>
					<?php endif ?>
					<label for="inn" class="mt-1 mb-1">ИНН клиники</label>
					<input id="inn" type="text" name="inn" class="inn form-control" value="<?php  if ( isset ( $options['inn'] ) ) { echo $options['inn']; } ?>" placeholder="Введите ИНН юр. лица" autocomplete="off" required>
					<label for="ur-name" class="mt-1 mb-1">Наименование юр.лица клиники</label>
					<input id="ur-name" type="text" name="ur_name" class="ur-name form-control" value="<?php  if ( isset ( $options['ur_name'] ) ) { echo $options['ur_name']; } ?>" placeholder="Введите наименование юр. лица"  autocomplete="off" required>
				</div>

				<?php if(isset($addresses)&&$addresses!=null):?>
					<div class="address-group">
						<h6 class="mt-1 mb-1">Адрес</h6>
						<?php $data_group_index=0; ?>
						<?php foreach ( $addresses as $key => $address ) : ?>
							<div class="address-group-item mb-2" data-group-index="<?=$data_group_index ?>">
								<div class="form-group mb-1">
									<label class="sr-only" for="region">Регион</label>
									<input id="region" type="text" name="address[<?=$data_group_index ?>][region]" class="region form-control mb-2" value="<?=$options['addresses'][$key]['region'];?>" placeholder="Регион"  autocomplete="off">
									<label class="sr-only" for="city">Город</label>
									<input id="city" type="text" name="address[<?=$data_group_index ?>][city]" class="city form-control mb-2" value="<?=$options['addresses'][$key]['city'];?>" placeholder="Город"  autocomplete="off" required>
									<label class="sr-only" for="street">Улица</label>
									<input id="street" type="text" name="address[<?=$data_group_index ?>][street]" class="street form-control mb-2" value="<?=$options['addresses'][$key]['street'];?>" placeholder="Улица"  autocomplete="off" required>
									<label class="sr-only" for="house">Дом</label>
									<input id="house" type="text" name="address[<?=$data_group_index ?>][house]" class="house form-control mb-2" value="<?=$options['addresses'][$key]['house'];?>" placeholder="Дом"  autocomplete="off" required>
									<label class="sr-only" for="office">Офис</label>
									<input id="office" type="text" name="address[<?=$data_group_index ?>][office]" class="office form-control" value="<?=$options['addresses'][$key]['office'];?>" placeholder="Офис/Помещение"  autocomplete="off">
								</div>
								<div class="btn btn-danger btn-block remove-address-group-item"><i class="icon-cancel"></i></div>
							</div>
							<?php $data_group_index++ ?>
						<?php endforeach?>
						<div class="form-inline">
							<div id="add-address-group-item" class="btn btn-service btn-block mb-2"><i class="icon-plus"></i></div>
						</div>
					</div>
				<?php else:?>
					<div class="address-group" data-group-index="0">
						<h6 class="mt-1 mb-1">Адрес</h6>
						<div class="address-group-item mb-2" data-group-index="0">
							<div class="form-group mb-0">
								<label class="sr-only" for="region">Регион</label>
								<input id="region" type="text" name="address[0][region]" class="region form-control mb-2" value="" placeholder="Регион"  autocomplete="off">
								<label class="sr-only" for="city">Город</label>
								<input id="city" type="text" name="address[0][city]" class="city form-control mb-2" value="" placeholder="Город"  autocomplete="off" required>
								<label class="sr-only" for="street">Улица</label>
								<input id="street" type="text" name="address[0][street]" class="street form-control mb-2" value="" placeholder="Улица"  autocomplete="off" required>
								<label class="sr-only" for="house">Дом</label>
								<input id="house" type="text" name="address[0][house]" class="house form-control mb-2" value="" placeholder="Дом"  autocomplete="off" required>
								<label class="sr-only" for="office">Офис</label>
								<input id="office" type="text" name="address[0][office]" class="office form-control" value="" placeholder="Офис/Помещение"  autocomplete="off">
							</div>
							<div class="form-inline">
								<div id="add-address-group-item" class="btn btn-service btn-block mt-2"><i class="icon-plus"></i></div>
							</div>
						</div>
					</div>
				<?php endif ?>
				<div class="form-group mt-2">
				    <label for="comment" class="mt-1 mb-1">Комментарий</label>
				   <textarea id="comment" class="form-control" name="comment" placeholder="Введите комментарий, если требуется" style="min-height:60px;"><?php  if ( isset ( $options['comment'] ) ) { echo $options['comment'];  } ?></textarea>
				</div>
			</div>
			<div class="order-3 col-md-12 order-md-3 col-lg-6 order-lg-2 ">
				<h3 class="text-color-dsmed text-center">Оборудование</h3>
				<div id="accordion">
					<?php $count=0; ?>
					<?php foreach($commonList as $commonListItem): ?>
						<?php if(!empty($commonListItem['cm_title'])): ?>
							<?php if(isset($commonListItem['tools'])&&!empty($commonListItem['tools'])):?>
								<div class="card mb-2 tools-block">
									<div class="card-header" id="heading-<?=$count?>">
										<button class="btn btn-link btn-block" type="button" data-toggle="collapse" data-target="#collapse-<?=$count?>" aria-expanded="true" aria-controls="collapse-<?=$count?>">
				          					<?=$commonListItem['cm_title'] ?>
								        </button>
										<div id="collapse-<?=$count?>" class="collapse tools-block-content" aria-labelledby="heading-<?=$count?>" data-parent="#accordion">
								      		<div class="card-body">
												<?php foreach ( $commonListItem['tools'] as $toolsListItem ) : ?>
													<div class="checkbox"><input type="checkbox" name="tools[]" id="tools-<?=$toolsListItem['id']?>" value="<?=$toolsListItem['id']?>"><label for="tools-<?=$toolsListItem['id']?>"><?=$toolsListItem['tool_name']?></label></div>
												<?php endforeach?>
								      		</div>
								    	</div>
									</div>
								</div>
							<?php endif ?>
						<?php else: ?>
							<div id="collapse-<?=$count?>" class="collapse tools-block-content" aria-labelledby="heading-<?=$count?>" data-parent="#accordion">
								<div class="card-body">
									<?php foreach ( $commonListItem['tools'] as $toolsListItem ) : ?>
										<div class="checkbox"><input type="checkbox" name="tools[]" id="tools-<?=$toolsListItem['id']?>" value="<?=$toolsListItem['id']?>"><label for="tools-<?=$toolsListItem['id']?>"><?=$toolsListItem['tool_name']?></label></div>
									<?php endforeach ?>
								</div>
							</div>
						<?php endif ?>
						<?php $count++ ?>
					<?php endforeach ?>
				</div>
			</div>
			<div class="order-1 col-md-6 order-md-2 col-lg-3 order-lg-3 ">
				<div class="company-information">
					<h3 class="text-color-dsmed">Компания</h3>
					<h6 class="font-weight-bold">Юридическое лицо</h6>
					<p><?=User::getUserBrendName() ?></p>
					<h6 class="font-weight-bold">Телефон компании</h6>
					<p><?=User::getUserPhone() ?></p>
					<h6 class="font-weight-bold">Email компании</h6>
					<p><?=User::getUserEmail() ?></p>
				</div>
				<div class="company-personal">
					<h3 class="text-color-dsmed">Сотрудник</h3>
					<?php if ( ! isset ( $dealer_manager['dealer_managers'] ) || $dealer_manager['dealer_managers'] === NULL ) : ?>
						<div class="form-group personal">
							<select class="form-control" name="select-personal">
								<option value="false" selected>Выбрать сотрудника</option>

								<?php foreach ( $dealerManagers as $manager ) : ?>
									<?php $id = $manager['id'];?>
									<?php $value = $manager['second_name'].' '.$manager['name'].' '.$manager['patronymic']; ?>
										<option value="<?=$id?>"><?=$value?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="form-group personal-information">
							<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    						<span aria-hidden="true">&times;</span>
	  							</button>
							  	Добавьте нового сотрудника, если его нет в списке!
							</div>
							<div class="personal-data">
								<label for="fam" class="sr-only">Фамилия</label>
								<input id="fam" class="fam pd-field form-control mb-2" type="text" name="new_dealer_manager_fam" placeholder="Фамилия" autocomplete="off" required>
								<label for="name" class="sr-only">Имя</label>
								<input id="name" class="name pd-field form-control mb-2" type="text" name="new_dealer_manager_name" placeholder="Имя" autocomplete="off" required>
								<label for="pat" class="sr-only">Отчество</label>
								<input id="pat" class="pat pd-field form-control mb-2" type="text" name="new_dealer_manager_patr" placeholder="Отчество" autocomplete="off">
								<label for="phone" class="sr-only">Телефон</label>
								<input id="phone" class="pd-field form-control mb-2 phone" type="text" name="new_dealer_manager_phone" placeholder="Телефон" autocomplete="off" required>
								<label for="email" class="sr-only">Почта</label>
								<input id="email" class="pd-field form-control mb-2 email" type="text" name="new_dealer_manager_email" placeholder="Почта" autocomplete="off" required>
								<div id="add-personal" class="btn btn-success btn-block">Добавить</div>
							</div>
						</div>
					<?php else:?>
						<div class="form-group personal">
							<select class="form-control" name="select-personal">
								<?php foreach($dealerManagers as $manager):?>
									<?php $id = $manager['id']; ?>
									<?php $value = $manager['second_name'].' '.$manager['name'].' '.$manager['patronymic']; ?>
									<?php if($dealer_manager['dealer_managers'] == $id ):?>
										<option value="<?=$id?>" selected><?=$value?></option>
									<?php else:?>
										<option value="<?=$id?>"><?=$value?></option>
									<?php endif?>
								<?php endforeach?>
							</select>
						</div>
						<div class="form-group personal-information">
							<div class="personal-data">
								<label for="phone" class="sr-only">Телефон</label>
								<input id="phone" class="form-control mb-2 phone" type="text" name="manager_phone" value="<?=$dealer_manager['manager_phone'] ?>" placeholder="Телефон" autocomplete="off" readonly required>
								<label for="email" class="sr-only">Почта</label>
								<input id="email" class="form-control email" type="text" name="manager_email" value="<?=$dealer_manager['manager_email'] ?>" placeholder="Почта"  autocomplete="off" readonly required>
							</div>
						</div>
					<?php endif ?>
				</div>

			</div>
			<!--
				End right container
			-->
		</div>
		<div class="row">
			<div class="container" style="width:auto;">
				<script src='https://www.google.com/recaptcha/api.js'></script>
				<div class="g-recaptcha" data-sitekey="6LelfWYUAAAAAFWZJoc4p8T-lrUvbVXWNTPWwofA"></div>
				<input id="project-add" type="submit" name="submit" class="btn btn-success mt-2" style="width:304px;" value="Отправить авторизации">
			</div>
		</div>
	</div>
</form>

<?php include ROOT.'/views/layouts/footer.php';?>
