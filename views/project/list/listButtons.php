<?php if ( $userGroup ) : ?>
    <div class="form-row mt-2">
        <div class="form-group col-lg-4 mb-1">
            <button id="btn-change-<?=$projectListItem['p_id'] ?>" class="btn btn-block btn-service btn-sm text-white btn-change-status" disabled data-btn-id="<?=$projectListItem['p_id'] ?>" data-toggle="modal" data-target="#change-status">Изменить</button>
        </div>
        <div class="form-group col-lg-4 mb-1">
            <?php if($userGroup==1||$userGroup==2):?>
                <div id="btn-get-comment-<?=$projectListItem['p_id'] ?>" class="btn btn-block btn-service btn-sm text-white btn-get-comment" data-btn-id="<?=$projectListItem['p_id'] ?>">Комментарий</div>
            <?php endif ?>
        </div>
        <div class="form-group col-lg-4 mb-1">
            <?php if($userGroup==1||$userGroup==2):?>
                <div id="btn-get-all-comments-<?=$projectListItem['p_id'] ?>" class="btn btn-block btn-service btn-sm text-white btn-get-all-comments" data-btn-id="<?=$projectListItem['p_id'] ?>">Все комментарии</div>
            <?php endif ?>
        </div>
    </div>
<?php endif ?>
