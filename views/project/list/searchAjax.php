<?php if(!empty($projectList)):?>
    <?php foreach($projectList as $projectListItem):?>
        <div id="project-<?=$projectListItem['p_id'];?>" class="project container-fluid pb-2 mb-3 border border-dsmed-light rounded">
            <div class="row border-bottom border-dsmed-light bg-dsmed-light text-white">
                <div class="col-12 col-sm-8">
                    <h5 id="project-title-<?=$projectListItem['p_id'];?>" class="pt-2 text-color-dsmed  text-center text-sm-left">Проект № <?=$projectListItem['p_id'];?> зарегистрирован <?=$projectListItem['date'];?> (<?=$projectListItem['time'];?>)</h5>
                </div>
                <div class="col-12 col-sm-4">
                    <?php if($userGroup==1||$userGroup==2):?>
                        <?php # Если бронь компании DS-Med не показывать кнопку.?>
                        <?php if($projectListItem['d_id']!=2):?>
                            <div id="btn-query-auth-<?=$projectListItem['p_id'];?>" class="btn btn-block btn-link btn-query-auth text-white text-center text-sm-right" data-btn-id="<?=$projectListItem['p_id'];?>" data-toggle="modal" data-target="#modal-query-auth">Запросить авторизацию</div>
                        <?php endif;?>
                    <?php endif;?>
                </div>
            </div>
            <div class="row pt-2">
                <!--  Project information end-client = pi-ec -->
                <div class="col-lg-6 mb-2 pi-ec">
                    <p class="pc-ec-dealer-name m-0">
                        <span class="text-color-dsmed">Дилер:</span>&nbsp;<em class="dealer-name__text"><?=$projectListItem['dealer']?></em>
                    </p>
                    <p id="dm-<?=$projectListItem['p_id']?>" class="pc-ec-manager-name m-0" data-id-m="<?=$projectListItem['manager_id']?>">
						<span class="text-color-dsmed">Менеджер:</span>
						<a id="manager__text-<?=$projectListItem['p_id'];?>"
							class="btn btn-link inline-block manager__text text-dark m-0 p-0"
							role="button"
							data-toggle="modal"
							data-target="#change-project-manager"
							data-change-project-manager="<?=$projectListItem['p_id'];?>">
						    <em><?=$projectListItem['manager']?></em>
					    </a>
					</p>
                    <h6 class="m-0 mt-1 mb-1">
                        <span class="text-color-dsmed"><b>Конечный клиент:</b></span>
                    </h6>
                    <p id="ur-name-<?=$projectListItem['p_id']?>" class="pc-ec-ur-name m-0">
                        <span class="text-color-dsmed">Наименование:</span><span class="ur-name__text">&nbsp;<?=$projectListItem['cl_ur_name']?></span>
                    </p>
                    <p id="inn-<?=$projectListItem['p_id']?>" class="pc-ec-inn m-0">
                        <span class="text-color-dsmed">ИНН:</span><span class="inn__text">&nbsp;<?=$projectListItem['inn']?></span>
                    </p>
                    <p id="addresses-<?=$projectListItem['p_id']?>" class="pc-ec-addresses m-0">
                        <span class="text-color-dsmed">Адрес(a):</span><span class="addresses__text">&nbsp;<?=$projectListItem['address']?></span>
                    </p>
                    <p id="comment-<?=$projectListItem['p_id']?>" class="pc-ec-comment m-0">
                        <span class="text-color-dsmed">Комментарий:</span><span class="comment__text">&nbsp;<?=$projectListItem['comment']?></span>
                    </p>
                    <?php if ( $userGroup === 1 || $userGroup === 2 ) : ?>
                        <?php if ( empty ( $projectListItem['responsible'] ) ) : ?>
                            <div class="form-row">
                                <div class="col-6">
                                    <select class="form-control form-control-sm" name="select-responsible">
                                        <option value="none">Выбрть ответственного</option>
                                        <?php foreach ($dsmedManagers as $id => $dsmedManager) : ?>
                                            <option value="<?=$dsmedManager['id']?>"><?=$dsmedManager['second_name']?> <?=$dsmedManager['name']?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-6">
                                    <button type="button" class="btn btn-service btn-sm text-white apply-responsible" data-applay-responsible="<?=$projectListItem['p_id'];?>"> Применить </button>
                                </div>
                            </div>
                        <?php else: ?>
                            <p>
                                <span class="text-color-dsmed">Ответственный:</span>
                                <a id="responsible__text-<?=$projectListItem['p_id'];?>" class="btn btn-link inline-block responsible__text text-dark m-0 p-0" role="button" data-toggle="modal" data-target="#change-responsible" data-applay-responsible="<?=$projectListItem['p_id'];?>">
                                    <?php foreach ( $dsmedManagers as $id => $dsmedManager ) : ?>
                                        <?php if ( $dsmedManager['id'] === $projectListItem['responsible'] ) :?>
                                            <?=$dsmedManager['second_name']?>&nbsp;<?=$dsmedManager['name']?>&nbsp;<?=$dsmedManager['patronymic']?></option>
                                            <?php break;?>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </a>
                            </p>
                        <?php endif ?>
                    <?php endif ?>
                </div>
                <div class="use-select-none col-lg-6">
                    <?php include ROOT.'/views/project/list/listHeaderBrons.php'?>
                    <?php include ROOT.'/views/project/list/listBrons.php'?>
                    <?php include ROOT.'/views/project/list/listButtons.php'?>
                </div>
            </div>
        </div>
    <?php endforeach?>
<?php else:?>
    <div class="alert alert-info" role="alert">
        Нет проектов, удовлетворяющих поисковому запросу..
    </div>
<?php endif;?>
