<!-- Chenge Status Modal -->
<div class="modal fade" id="change-status" tabindex="-1" role="dialog" aria-labelledby="change-status-header" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="change-status-header">Смена статуса авторизации(й) у проекта <span class="change-brons-by-project"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<?php if ( ! empty ( $statusList ) ) : ?>
						<select id="new-status" class="form-control" name="select-status">
							<option value="0">Установите статус</option>
							<?php foreach ( $statusList as $status ) : ?>
								<option value="<?=$status['id']?>"><?=$status['status']?></option>
							<?php endforeach ?>
						</select>
					<?php else: ?>
						<div class="alert alert-danger" role="alert">Ошибка! невозможно получить статус.</div>
					<?php endif ?>
				</div>
				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					Комментарий обязателен к заполнению! Минимальная длинна - 11 символов.
				</div>
				<label for="comment-by-status">Комментарий</label>
				<textarea id="comment-by-status" class="form-control" name="status-comment"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-change-status-apply" data-dismiss="modal">Изменить</button>
			</div>
		</div>
	</div>
</div>
