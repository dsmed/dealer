<!-- Search Modal -->
<div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-labelledby="search-modal-header" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content">
    		<div class="modal-header">
    	   		<h5 class="modal-title" id="search-modal-header">Поиск</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
         			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
		    <div class="modal-body">

				<div class="form-group">
                    <input class="form-control" type="text" name="search-input" value="" placeholder="Ваш поисковый запрос" autocomplete="off">
                </div>

				<div class="form-group">
					<select class="form-control search-required" name="project-or-tools">
		  				<option value="0" selected>Где искать</option>
		  				<option value="project">Проект</option>
		  				<option value="toolblock">Оборудование</option>
		  			</select>
			 	</div>

				<div class="form-group project-block d-none" >
                    <?php if(isset($projectParams)&&!empty($projectParams)):?>
                        <select class="form-control search-select" name="project-params">
                            <option value="none" selected>Выбрать поле поиска</option>
                            <?php foreach ($projectParams as $id => $value):?>
                                <?php if($userGroup==1||$userGroup==2):?>
                                    <?php if(intval($value['view_all'])):?>
                                        <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                    <?php endif?>
                                <?php else:?>
                                    <?php if(intval($value['view_all'])):?>
                                        <?php if(intval($value['view_dealer'])):?>
                                            <option value="<?=$value['id']?>"><?=$value['name']?></option>
                                        <?php endif?>
                                    <?php endif?>
                                <?php endif?>
                            <?php endforeach?>
                        </select>
                    <?php else:?>
						<div class="alert alert-danger" role="alert">
							Project params is empty.
						</div>
                    <?php endif?>
                </div>

				<div class="form-group manager-block d-none">
                    <?php if(isset($dealerManagers)&&!empty($dealerManagers)):?>
                        <select class="form-control search-select" name="manager-params">
                            <option value="none" selected>Выбрать сотрудника</option>
                            <?php foreach ($dealerManagers as $id => $value):?>
                                <option value="<?=$value['id']?>"><?=$value['name']?> <?=$value['second_name']?> <?=$value['patronymic']?></option>
                            <?php endforeach?>
                        </select>
                    <?php else:?>
						<div class="alert alert-danger" role="alert">
							Manager params is empty.
						</div>
                    <?php endif?>
                </div>

				<div class="form-group toolblock-block d-none">
                    <?php if(isset($blocksList)&&!empty($blocksList)):?>
                        <select class="form-control search-select" name="toolblock-params">
                            <option value="none" selected>Выбрать тип оборудования</option>
                            <?php foreach ($blocksList as $id => $value):?>
                                <?php if($userGroup==1||$userGroup==2):?>
                                    <?php if(intval($value['view_all'])):?>
                                        <option value="<?=$value['id']?>"><?=$value['block_name']?></option>
                                    <?php endif?>
                                <?php else:?>
                                    <?php if(intval($value['view_all'])):?>
                                        <?php if(intval($value['view_dealer'])):?>
                                            <option value="<?=$value['id']?>"><?=$value['block_name']?></option>
                                        <?php endif?>
                                    <?php endif?>
                                <?php endif?>
                            <?php endforeach?>
                        </select>
                    <?php else:?>
						<div class="alert alert-danger" role="alert">
							Type tool params is empty.
						</div>
                    <?php endif?>
                </div>

				<div class="form-group tool-block d-none">
                    <select class="form-control search-select" name="tool-params">
                            <option value="none" selected>Выбрать поле поиска</option>
                    </select>
                </div>

				<div class="form-group status-block d-none">
                    <?php if(isset($statusList)&&!empty($statusList)):?>
                        <select class="form-control search-select" name="status-params">
                            <option value="0" selected>Не учитывать статус</option>
                            <?php foreach ($statusList as $key => $status):?>
                                <option value="<?=$status['id']?>"><?=$status['status']?></option>
                            <?php endforeach?>
                        </select>
                    <?php else:?>
                        Status tool params is empty.
                    <?php endif?>
                </div>
                <div class="form-group sort-block">
                    <select class="form-control search-required" name="sort-params">
                            <option value="ASC" selected>По возрастанию</option>
                            <option value="DESC">По убыванию</option>
                    </select>
                </div>
                <div class="form-group count-block">
                    <select class="form-control search-required" name="count-params">
                        <option value="10">10</option>
                        <option value="30" selected>30</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="infinity">Все</option>
                    </select>
                </div>

		    </div>
      		<div class="modal-footer">
				<button id="search" type="button" class="btn btn-primary">Поиск</button>
        		<a id="exportxlsx" data-file="" class="btn btn-success text-white">Экспорт</a>
      		</div>
    	</div>
  	</div>
</div>
