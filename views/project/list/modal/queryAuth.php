<!-- Query Auth -->
<div class="modal fade bd-example-modal-xl" id="modal-query-auth" tabindex="-1" role="dialog" aria-labelledby="modal-query-auth-header" aria-hidden="true">
	<div class="modal-dialog modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-query-auth-header">Запрос авторизации</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="modal-body-query-auth" class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success btn-block btn-modal-query-auth" data-dismiss="modal" name="modal-query-auth">Запросить</button>
			</div>
		</div>
	</div>
</div>
