<?php if ( $userGroup ) : ?>
    <div class="row ml-0 mr-0 pt-2 brons-header">
        <div class="col-8 border-bottom border-dsmed-light pl-0 pr-0">
            <span class="p-1 m-0 text-color-dsmed">Оборудование</span>
        </div>
        <div class="col-3 border-bottom border-dsmed-light pl-0 pr-0">
            <span class="p-1 m-0 text-color-dsmed">Статус</span>
        </div>
        <div class="col-1 text-right border-bottom border-dsmed-light pl-0 pr-0">
            <label class="p-1 m-0">
                <input type="checkbox" class="check-all" name="check-all" data-check-all="<?=$projectListItem['p_id']?>">
            </label>
        </div>
    </div>
<?php endif ?>
