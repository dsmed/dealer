<?php foreach ($comments as $id => $comment) : ?>
    <div class="alert alert-info alert-dismissible comment-block mt-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <b>Дата:&nbsp;</b>
        <i><?=$comment['comment_date']?></i>.&nbsp;
        <b>Установленный статус:&nbsp;</b>
        <i>
            <?php switch($comment['status_id']){
                case 1: echo 'Не обработано'; break;
                case 2: echo 'Авторизовано'; break;
                case 3: echo 'Не авторизовано'; break;
                case 4: echo 'Реализовано'; break;
                case 5: echo 'Не реализовано'; break;
                case 6: echo 'Авторизация прекращена'; break;
                case 7: echo 'Авторизация продлена'; break;
                case 8: echo 'Авторизация просрочена'; break;
                case 9: echo 'Запрос актуальности'; break;
                default: echo 'Не обработано'; break;
            }?>
        </i>
        <br>
        <b>Комментарий:&nbsp;</b><i><?=$comment['comment']?></i>
    </div>
<?php endforeach ?>
