<?php $count = 1 ?>

<?php if ( ! empty ( $bronsByProject ) ) : ?>

    <?php foreach ( $bronsByProject as $brone ) : ?>

            <div class="row ml-0 mr-0 brons-container" data-projectId="<?=$brone['p-id']?>" data-brone="<?=$brone[0]?>">
                <div class="col-8 pl-0 pr-0 color-status-<?=$brone[2]?>">
                    <label class="p-1 m-0 tool_name" for="input-bron-<?=$brone[0]?>" data-count="<?=$count?>"><?=$brone[1]?></label>
                </div>
                <div class="col-3 pl-0 pr-0 color-status-<?=$brone[2]?>">
                    <label class="p-1 m-0 status_name" for="input-bron-<?=$brone[0]?>" data-count="<?=$count?>"><?=$brone[3]?></label>
                </div>
                <div class="col-1 pl-0 pr-0 color-status-<?=$brone[2]?> text-right">
                    <label class="p-1 m-0 color-status-<?=$brone[2]?>">
                        <input type="checkbox" id="input-bron-<?=$brone[0]?>" class="input-bron" name="input-bron-<?=$brone[0]?>" data-count="<?=$count?>" data-check-brone="<?=$brone[0]?>">
                    </label>
                </div>
            </div>

        <?php $count++ ?>

    <?php endforeach ?>

<?php endif ?>
