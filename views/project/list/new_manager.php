<p id="" class="pc-ec-manager-name m-0" data-id-m="<?=$dealerManager['id']?>">
		<span class="text-color-dsmed">Менеджер:</span>
		<a id=""
			class="btn btn-link inline-block manager__text text-dark m-0 p-0"
			role="button"
			data-toggle="modal"
			data-target="#change-project-manager"
			data-change-project-manager="">
			<em><?=$dealerManager['second_name']?> <?=( !empty( $dealerManager['name'] ) ? ucfirst($dealerManager['name']) : '' )?> <?=( !empty( $dealerManager['patronymic'] ) ? ucfirst($dealerManager['patronymic']) : '' )?></em>
    </a>
</p>