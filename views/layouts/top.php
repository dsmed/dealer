<header class="header"></header>
<main class="main">

<?php $rurl=$_SERVER['REQUEST_URI'];?>
<?php preg_match('/[1-9][0-9]*/i',$rurl,$page)?$page=$page[0]:$page='';?>

<nav class="navbar navbar-expand-md sticky-top" style="background:#2b0089;">
	<button class="navbar-toggler" style="padding-left: 5px !important;border:none;outline:none;" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon" style="background-image:url('../template/dest/images/3strip.svg')"></span>
	</button>
  	<div class="collapse navbar-collapse" id="navbarMain" style="justify-content: space-between;">
    	<ul class="navbar-nav mr-auto">
      		<li class="nav-item"><a class="nav-link" href="/"><i class="icon-home"></i>Главная</a></li>
      		<li class="nav-item"><a class="nav-link" href="/projects"><i class="icon-order"></i>Проектный лист</a></li>
      		<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-plus"></i>Добавить</a>
        		<div class="dropdown-menu" aria-labelledby="navbarDropdown" style="padding: 0.5rem 1rem;border:none;">
          			<a class="nav-link" href="/project/add">Проект</a>
		  			<?php if($userGroup==1||$userGroup==2):?>
          				<a class="nav-link" href="/dealer">Дилера</a>
					<?php endif?>
        		</div>
      		</li>
			<?php if ( $rurl == "/project/manager/$page" ) : $btmyProjects = 'success'?>
			<?php else : $btmyProjects = 'light' ?>
			<?php endif ?>
			<li class="nav-item mr-1"><button type="button" class="btn btn-<?=$btmyProjects?>"  data-toggle="modal" data-target="#my-projects">Мои проекты</button></li>
			<?php if ( $userGroup === 1 || $userGroup === 2  || $userGroup === 3 ) : ?>
				<?php if ( $rurl == "/project/responsible/$page" ) : $btmyDealer = 'success'?>
				<?php else : $btmyDealer = 'light' ?>
				<?php endif ?>
				<li class="nav-item"><button type="button" class="btn btn-<?=$btmyDealer?>"  data-toggle="modal" data-target="#my-dealers">Мои дилеры</button></li>
			<?php endif ?>
		</ul>
		<div class="search-and-out">
			<?php if($rurl=='/projects' || $rurl=="/projects/page-$page"):?>
				<?php if($userGroup==1||$userGroup==2||$userGroup==3):?>
					<div class="btn-search"  data-toggle="modal" data-target="#search-modal" style="display:inline-block;padding: 0.5rem 1rem; cursor: pointer;">
						<i style="color:white;" class="fas fa-search fa-1x"></i>
						<span style="display:inline-block;color:white;">Поиск</span>
					</div>
				<?php endif?>
			<?php endif?>
    		<a class="nav-link" href="/logout" style="padding-left:0;"><i class="icon-logout"></i>Выход</a>
		</div>
  	</div>
</nav>

<div class="modal fade" id="my-projects" tabindex="-1" role="dialog" aria-labelledby="myProjectsLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мои проекты</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php foreach ( $dealerManagers as $manager ) : ?>
			<a class="btn btn-link btn-block text-left p-0 m-0" href="/project/manager/<?=$manager['id']?>">  <?=$manager['second_name']?> <?=( !empty( $manager['name'] ) ? ucfirst($manager['name']) : '' )?> <?=( !empty( $manager['patronymic'] ) ? ucfirst($manager['patronymic']) : '' )?></a>
		<?php endforeach ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="my-dealers" tabindex="-1" role="dialog" aria-labelledby="myDealersLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Мои дилеры</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php foreach ( $dsmedManagers as $dm ) : ?>
			<a class="btn btn-link btn-block text-left p-0 m-0" href="/project/responsible/<?=$dm['id']?>">  <?=$dm['second_name']?> <?=( !empty( $dm['name'] ) ? ucfirst($dm['name']) : '' )?> <?=( !empty( $dm['patronymic'] ) ? ucfirst($dm['patronymic']) : '' )?></a>
		<?php endforeach ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

<!-- Смена ответственного -->

<div class="modal fade" id="change-responsible" tabindex="-1" role="dialog" aria-labelledby="changeResponsible" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title-change-responsible" data-project="">Смена ответственного у проекта № <b id="mtcr-span"></b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
		    <select class="form-control" id="modal-select-change-responsible">
		      <option value="none">Выбрать ответственного</option>
			  <?php foreach ( $dsmedManagers as $dm ) : ?>
	  				<option value="<?=$dm['id']?>"><?=$dm['second_name']?> <?=( !empty( $dm['name'] ) ? ucfirst($dm['name']) : '' )?> <?=( !empty( $dm['patronymic'] ) ? ucfirst($dm['patronymic']) : '' )?></option>
		  	  <?php endforeach ?>
		    </select>
		  </div>
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-success apply-change-responsible" data-dismiss="modal">Применить</button>
      </div>
    </div>
  </div>
</div>

<!-- Смена менеджера -->

<div class="modal fade" id="change-project-manager" tabindex="-1" role="dialog" aria-labelledby="changeProjectManager" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title-change-project-manager" data-project="">Смена менеджера у проекта № <b id="mtcr-span"></b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
		    <select class="form-control" id="modal-select-change-project-manager">
		      <option value="none">Выбрать менеджера</option>
						<?php foreach ( $dealerManagers as $manager ) : ?>
							<option value="<?=$manager['id']?>"><?=$manager['second_name']?> <?=( !empty( $manager['name'] ) ? ucfirst($manager['name']) : '' )?> <?=( !empty( $manager['patronymic'] ) ? ucfirst($manager['patronymic']) : '' )?></option>
						<?php endforeach ?>
		    </select>
		  </div>
      </div>
      <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-success apply-change-project-manager" data-dismiss="modal">Применить</button>
      </div>
    </div>
  </div>
</div>