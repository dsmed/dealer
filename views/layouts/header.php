<!DOCTYPE html>
    <html lang="ru">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title><?php if ( isset ( $title )  ) { echo $title; } else { echo 'Система авторизаций ООО "ДС-Мед"'; } ?></title>
            <link rel="shortcut icon" href="/template/dest/images/favicon.ico">
            <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
            <link rel="stylesheet" href="/template/dest/css/bootstrap.min.css?version=<?=APPVERSION?>">
            <link rel="stylesheet" href="/template/dest/css/dealer.min.css?version=<?=APPVERSION?>">
        </head>
        <body>
