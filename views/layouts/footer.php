</main>
<footer class="footer"></footer>

<?php if($userGroup):?>
	<?php require_once(ROOT.'/views/project/list/modal/changeStatus.php')?>
<?php endif?>
<?php if($userGroup==1||$userGroup==2||$userGroup==3):?>
	<?php require_once(ROOT.'/views/project/list/modal/queryAuth.php')?>
	<?php require_once(ROOT.'/views/project/list/modal/search.php')?>
<?php endif?>

<div class="author" style="display:none">
    <p class="author_p">Developed by <a href="https://vk.com/doox911" title="Разработчик портала">Polyakov A.S.</a></p>
</div>

<script defer src="/template/dest/js/jquery.min.js?version=<?=APPVERSION?>"></script>
<script defer src="/template/dest/js/jquery.inputmask.bundle.min.js?version=<?=APPVERSION?>"></script>
<script defer src="/template/dest/js/bootstrap.min.js?version=<?=APPVERSION?>"></script>
<script defer src="/template/dest/js/dealer.min.js?version=<?=APPVERSION?>"></script>

</body></html>
