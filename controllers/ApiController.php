<?php

class ApiController {

    public function actionEquipments(){

        try {

            Api::checkJWT();

            echo json_encode( Api::getEquipments(), JSON_UNESCAPED_UNICODE );

        } catch (Exception $e) {

            echo json_encode( array('error' => $e->getMessage()), JSON_UNESCAPED_UNICODE );

        }

        return true;
    }
    public function actionPersonal(){

        try {

            $dealerId = Api::checkJWT();

            echo json_encode( Api::getPersonal($dealerId), JSON_UNESCAPED_UNICODE );

        } catch (Exception $e) {

            echo json_encode( array('error' => $e->getMessage()), JSON_UNESCAPED_UNICODE );

        }

        return true;
    }
    public function actionAddProject(){

        try {

            $dealerId = Api::checkJWT();

            $canUseApi = User::checkUserApi($dealerId);

            if(!$canUseApi) throw new Exception('API запрещён!');

            error_log("[".date("m.d.y")."-".date("H:i:s")."][APICONTROLLER/ACTIONADDPROJECT] canUseApi = $canUseApi \n", 3, ROOT."/log/api.log");

            if(Project::getProjectsDealerCountByInterval($dealerId) > 10 ) {
                Api::disabledApi($dealerId);

                error_log("[".date("m.d.y")."-".date("H:i:s")."][APICONTROLLER/ACTIONADDPROJECT] dealerId = $dealerId \n", 3, ROOT."/log/api.log");
                error_log("[".date("m.d.y")."-".date("H:i:s")."][APICONTROLLER/ACTIONADDPROJECT] disabledApi intalled \n", 3, ROOT."/log/api.log");

                throw new Exception('Превышен лимит добавления проектов!');
            }

            $inputJSON = file_get_contents("php://input");

            if(empty($inputJSON) || !Site::isJSON($inputJSON)) throw new Exception('Не верный формат данных!');

            $userProject = json_decode($inputJSON, true);

            if(!array_key_exists('project', $userProject) || !is_array($userProject['project']) || count($userProject['project']) !== 6) throw new Exception('Отсутствует ключ UP-P или не корректные данные по ключу!');
            if(!array_key_exists('addresses', $userProject) || !is_array($userProject['addresses']) || count($userProject['addresses']) === 0) throw new Exception('Отсутствует ключ UP-A или не корректные данные по ключу!');
            if(!array_key_exists('equipments', $userProject) || !is_array($userProject['equipments']) || count($userProject['equipments']) === 0) throw new Exception('Отсутствует ключ UP-E или не корректные данные по ключу!');

            if(!array_key_exists('name', $userProject['project'])) throw new Exception('Отсутствует или не содержит данные ключ UP-P-Name');
            if(!array_key_exists('urname', $userProject['project'])) throw new Exception('Отсутствует или не содержит данные ключ UP-P-Urname');
            if(!array_key_exists('type', $userProject['project'])) throw new Exception('Отсутствует или не содержит данные ключ UP-P-Type');
            if(!array_key_exists('inn', $userProject['project'])) throw new Exception('Отсутствует или не содержит данные ключ UP-P-Inn');
            if(!array_key_exists('comment', $userProject['project'])) throw new Exception('Отсутствует или не содержит данные ключ UP-P-Comment');
            if(!array_key_exists('personal', $userProject['project'])) throw new Exception('Отсутствует или не содержит данные ключ UP-P-Personal');

            if(!is_string($userProject['project']['name']) || strlen($userProject['project']['name'])>255) throw new Exception('Не верный тип данных UP-P-Name-Type');
            if(!is_string($userProject['project']['urname']) || strlen($userProject['project']['urname'])>255) throw new Exception('Не верный тип данных UP-P-Urname-Type');
            if(!is_string($userProject['project']['inn']) || strlen($userProject['project']['inn'])>12 || strlen($userProject['project']['inn']<10 )) throw new Exception('Не верный тип данных UP-P-Inn-Type');
            if(!is_string($userProject['project']['comment']) || strlen($userProject['project']['comment'])>255) throw new Exception('Не верный тип данных UP-P-Comment-Type');

            if(!is_numeric($userProject['project']['type']) || !is_int($userProject['project']['type'])) throw new Exception('Не верный тип данных UP-P-Type-Type');
            if(!is_numeric($userProject['project']['personal']) || !is_int($userProject['project']['personal'])) throw new Exception('Не верный тип данных UP-P-Personal-Type');

            $userProject['project']['name'] = trim(htmlspecialchars(strip_tags($userProject['project']['name'])));
            $userProject['project']['urname'] = trim(htmlspecialchars(strip_tags($userProject['project']['urname'])));
            preg_match('/[0-9]*/',$userProject['project']['inn'],$res)?$userProject['project']['inn']=$res[0]:$userProject['project']['inn']='';
            $userProject['project']['comment'] = trim(htmlspecialchars(strip_tags($userProject['project']['comment'])));

            if(empty($userProject['project']['name']) || empty($userProject['project']['urname']) || empty($userProject['project']['inn'])) throw new Exception('Не корректные данные по проекту');

            foreach ($userProject['addresses'] as $mas) {

                if(!is_array($mas)) throw new Exception('Отсутствует ключ UP-A-Item или не корректные данные по ключу!');

            }

            foreach ($userProject['addresses'] as $id => $value) {

                if(!array_key_exists('region', $userProject['addresses'][$id])) throw new Exception('Отсутствует ключ UP-A-Region');
                if(!array_key_exists('city', $userProject['addresses'][$id])) throw new Exception('Отсутствует ключ UP-A-City');
                if(!array_key_exists('street', $userProject['addresses'][$id])) throw new Exception('Отсутствует ключ UP-A-Street');
                if(!array_key_exists('house', $userProject['addresses'][$id])) throw new Exception('Отсутствует ключ UP-A-House');
                if(!array_key_exists('office', $userProject['addresses'][$id])) throw new Exception('Отсутствует ключ UP-A-Office');

                $userProject['addresses'][$id]['region'] = trim(htmlspecialchars(strip_tags($value['region'])));
                $userProject['addresses'][$id]['city'] = trim(htmlspecialchars(strip_tags($value['city'])));
                $userProject['addresses'][$id]['street'] = trim(htmlspecialchars(strip_tags($value['street'])));
                $userProject['addresses'][$id]['house'] = trim(htmlspecialchars(strip_tags($value['house'])));
                $userProject['addresses'][$id]['office'] = trim(htmlspecialchars(strip_tags($value['office'])));

                if(empty($userProject['addresses'][$id]['city'])) throw new Exception('Не корректные данные по проекту');
                if(empty($userProject['addresses'][$id]['street'])) throw new Exception('Не корректные данные по проекту');
                if(empty($userProject['addresses'][$id]['house'])) throw new Exception('Не корректные данные по проекту');

            }
            foreach ($userProject['equipments'] as $tool) {
                if(!is_numeric($tool) || !is_int($tool)) throw new Exception('Не верный тип данных UP-P-Tools-Type');
            }

            $options = [
                'clinics_name' => $userProject['project']['name'],
                'ur_name' => $userProject['project']['urname'],
                'inn' => $userProject['project']['inn'],
                'comment' => $userProject['project']['comment'],
                'dealer-id'=> $dealerId,
                'manager-id' => $userProject['project']['personal'],
                'ur_name_type' => $userProject['project']['type'],
                'addresses' => $userProject['addresses'],
                'tools' => $userProject['equipments'],
            ];

            $project = Project::createProject($options);

            echo json_encode( array('success' => 'added'), JSON_UNESCAPED_UNICODE );

        } catch (Exception $e) {

            echo json_encode(array('error' => $e->getMessage()), JSON_UNESCAPED_UNICODE);

        }

        return true;
    }

}
