<?php

require_once ROOT.'/vendor/autoload.php';

class ProjectController {
	public function actionList($page=1){
		try {

			$title = 'Проектный лист';
			$userGroup= User::getUserGroup();
			$dealerId = User::getUserId();
			$dealerManagers = Dealer::getDealerManagers( $dealerId );
			$projectList = array();

			# Количество авторизация со статусом "Авторизация просрочена"
			# Count authorizations by authorization expired = CABAE
			$CABAE = Project::getCABAE( User::getUserId() );

			# Количество авторизация со статусом "Запрос актуальности"
			# Count authorizations by relevance query = CABRQ
			$CABRQ = Project::getCABRQ( User::getUserId() );


			if ( ! $userGroup ) throw new Exception('Не достаточно прав!');

			if ( $userGroup === 1 || $userGroup === 2 ) {

				# Выбрать все проекты
				$where="";

			}
			else {

				# Выбрать все проекты конкретного дилера
				$dealerId = User::getUserId();
				$where = "WHERE p.dealer=$dealerId";

			}

			$projectParams = Project::getProjectSelectParams();
			$dealerManagers = Dealer::getDealerManagers( User::getUserId() );
			$blocksList = Project::getBlocksList();

			$projectList = Project::getProjectList($page,$where);
			$projectList = Project::getBronParamsProjectList($projectList);
			$statusList = Project::getStatusList();
			$total = Project::getTotalProject($userGroup);
			$dsmedManagers = Dealer::getDsmedManagers();
			$pagination = new Pagination($total, $page, Project::SHOW_BY_DEFAULT, 'page-');
			require_once ROOT . '/views/project/list/list.php';

		} catch(Exception $e){
			require_once ROOT.'/views/site/exceptions.php';
			return true;
		}
		return true;
	}
	public function actionManager( $managerId ) {

		try {

			$title = 'Мои проекты';
			$managerId = intval ( $managerId );
			$dealerId = User::getUserId();
			$userGroup = User::getUserGroup();
			$myProjectCount = User::getUserMyProjectCount( $managerId );
			$projectList = array();
			$dealerManagers = Dealer::getDealerManagers( $dealerId );

			# Количество авторизация со статусом "Авторизация просрочена"
			# Count authorizations by authorization expired = CABAE
			$CABAE = MyProject::getCABAE( $dealerId, $managerId );

			# Количество авторизация со статусом "Запрос актуальности"
			# Count authorizations by relevance query = CABRQ
			$CABRQ = MyProject::getCABRQ( $dealerId, $managerId );

			if ( ! $userGroup ) throw new Exception('Не достаточно прав!');

			$projectList = MyProject::getProjectList( $projectList, $dealerId, $managerId );
			$projectList = MyProject::getBronParamsProjectList( $projectList );
			$statusList = Project::getStatusList();
			$blocksList = Project::getBlocksList();
			$dsmedManagers = Dealer::getDsmedManagers();


			require_once ROOT . '/views/project/list/myProjectList.php';

			return true;

		} catch ( Exception $e ) {

			require_once ROOT.'/views/site/exceptions.php';

			return true;
		}

	}
	public function actionResponsible( $responsibleId ) {

		try {

			$userGroup = User::getUserGroup();
			if ( $userGroup !== 1 && $userGroup !== 2  && $userGroup !== 3 ) throw new Exception('Не достаточно прав!');

			$title = 'Мои дилеры';
			$responsibleId = intval ( $responsibleId );
			$dealerId = User::getUserId();
			$myProjectResponsibleCount = MyResponsible::getUserMyResponsible( $responsibleId );
			$projectList = array();
			$dealerManagers = Dealer::getDealerManagers( $dealerId );

			# Количество авторизация со статусом "Авторизация просрочена"
			# Count authorizations by authorization expired = CABAE
			$CABAE = MyResponsible::getCABAE( $dealerId, $responsibleId );

			# Количество авторизация со статусом "Запрос актуальности"
			# Count authorizations by relevance query = CABRQ
			$CABRQ = MyResponsible::getCABRQ( $dealerId, $responsibleId );

			if ( ! $userGroup ) throw new Exception('Не достаточно прав!');

			$projectList = MyResponsible::getProjectList( $projectList, $responsibleId );
			$projectList = Project::getBronParamsProjectList( $projectList );
			$statusList = Project::getStatusList();
			$blocksList = Project::getBlocksList();
			$dsmedManagers = Dealer::getDsmedManagers();


			require_once ROOT . '/views/project/list/myResponsible.php';

			return true;

		} catch ( Exception $e ) {

			require_once ROOT.'/views/site/exceptions.php';

			return true;
		}


	}
	public function actionGetComment(){
		try{
			$userGroup = User::getUserGroup();
			if($userGroup !== 1 && $userGroup !== 2) throw new Exception('Не достаточно прав!');
			if(empty($_POST['bron_id']) || empty($_POST['flagCount'])) throw new Exception('Данные не переданы!');

			$bron_id = preg_replace('/[^0-9]/', '',$_POST['bron_id']);
			preg_match('/one|all/i',$_POST['flagCount'],$flagCount)?$flagCount=$flagCount[0]:$flagCount=false;

			if(!$flagCount || $flagCount === 'one') $comments = Project::getBronLastComment($bron_id);
			else $comments = Project::getBronComments($bron_id);

			require_once ROOT . '/views/project/list/getBronsComments.php';

		}catch(Exception $e){
			require_once ROOT.'/views/site/exceptions.php';
		}
		return true;
	}
	public function actionSearchAjax(){
		try{
			$userGroup= User::getUserGroup();

			if($userGroup !==1 && $userGroup !==2 && $userGroup !==3) throw new Exception('Не достаточно прав!');

			$projectList = array();

			if(empty($_POST['ps'])) throw new Exception('Данные не переданы!');

			$ps = $_POST['ps'];
			$projectList=Project::ProjectSearch($ps,$userGroup);
			$projectList=Project::getBronParamsProjectList($projectList);
			$dsmedManagers = Dealer::getDsmedManagers();
			require_once ROOT . '/views/project/list/searchAjax.php';
		}
		catch(Exception $e){
			require_once ROOT.'/views/site/exceptions.php';
		}
		return true;
	}
	public function actionGetAttentions () {

		try {

			$userGroup= User::getUserGroup();

			if ( ! $userGroup ) throw new Exception('Не достаточно прав!');

			if ( empty ( $_POST['attention-type'] ) ) throw new Exception('Данные не переданы!');

			$type =  $_POST['attention-type'];

			$attentionProjectList = Project::getAttentionProjectList ( $type, User::getUserId() );
			$attentionProjectList = Project::getBronParamsProjectList ( $attentionProjectList );

			require_once ROOT . '/views/project/list/attention-project-list.php';
		}

		catch ( Exception $e ) {

			require_once ROOT.'/views/site/exceptions.php';

		}

		return true;
	}
	public function actionGetMyAttentions () {

		try {

			$userGroup= User::getUserGroup();
			$dealerId = User::getUserId();

			if ( ! $userGroup ) throw new Exception('Не достаточно прав!');

			if ( empty ( $_POST['attention-type'] || $_POST['managerid']) ) throw new Exception('Данные не переданы!');

			$type =  $_POST['attention-type'];
			$managerId = intval ( $_POST['managerid'] );

			$attentionProjectList = array();

			$attentionProjectList = MyProject::getAttentionProjectList ( $attentionProjectList, $type, $dealerId, $managerId);
			$attentionProjectList = MyProject::getBronParamsProjectList ( $attentionProjectList );

			require_once ROOT . '/views/project/list/attention-project-list.php';
		}

		catch ( Exception $e ) {

			require_once ROOT.'/views/site/exceptions.php';

		}

		return true;
	}
	public function actionGetMyResponsible () {

		try {

			$userGroup= User::getUserGroup();
			$dealerId = User::getUserId();

			if ( $userGroup !== 1 && $userGroup !== 2  && $userGroup !== 3 ) throw new Exception('Не достаточно прав!');

			if ( empty ( $_POST['responsible-type'] || $_POST['responsibleid']) ) throw new Exception('Данные не переданы!');

			$type =  $_POST['responsible-type'];
			$responsibleId = intval ( $_POST['responsibleid'] );

			$projectList = array();

			$projectList = MyResponsible::getResponsibleProjectList ( $projectList, $type, $responsibleId);
			$projectList = Project::getBronParamsProjectList ( $projectList );
			$statusList = Project::getStatusList();
			$blocksList = Project::getBlocksList();
			$dsmedManagers = Dealer::getDsmedManagers();

			require_once ROOT . '/views/project/list/searchAjax.php';
		}

		catch ( Exception $e ) {

			require_once ROOT.'/views/site/exceptions.php';

		}

		return true;
	}
	public function actionExportSearchTable() {

		try {

			$userGroup= User::getUserGroup();

			if ( ! $userGroup ) throw new Exception('Не достаточно прав!');

			$projectList = array();

			if ( empty ( $_POST['ps'] ) ) throw new Exception('Данные не переданы!');

			$ps = $_POST['ps'];

			$projectList = Project::ProjectSearch( $ps, $userGroup );

			$projectList = Project::getBronParamsProjectList( $projectList );

			$name = Site::createTableBySearch( $projectList );

			echo $name;

		}
		catch ( Exception $e ) {

			require_once ROOT.'/views/site/exceptions.php';

		}

		return true;
	}
	public function actionExportDownload( $fileName ) {

		try {

			$userGroup= User::getUserGroup();

			if ( ! $userGroup ) throw new Exception('Не достаточно прав!');

			Site::exportDownload($fileName);

		}
		catch ( Exception $e ) {

			require_once ROOT.'/views/site/exceptions.php';

		}

		return true;
	}
	public function actionSearchGetParams(){
		try{
			$userGroup= User::getUserGroup();

			if($userGroup !==1 && $userGroup !==2 && $userGroup !==3) throw new Exception('Не достаточно прав!');
			if(empty($_POST['toolblock'])) throw new Exception('Данные не переданы!');

			$toolBlock = $_POST['toolblock'];
			$toolsList=Project::getToolsOnBlockList($toolBlock);
			require_once ROOT.'/views/project/list/popupSearchSelectProjectOrTool.php';

		}catch(Exception $e){
			require_once ROOT.'/views/site/exceptions.php';
			return true;
		}

		return true;
	}
	public function actionGettingManager(){
		try{
			$userGroup = User::getUserGroup();

			if(!$userGroup) throw new Exception('Не достаточно прав!');
			$managerId = $_POST['manager_id'];
			if(!Dealer::checkDealerManager($managerId)) throw new Exception('В компании нет такого сотрудника!');

			$manager = Dealer::getDealerOneManager($managerId);
			require_once ROOT.'/views/project/add/getDealerManagerPhoneAndEmail.php';

		}catch(Exception $e){
			require_once ROOT.'/views/site/exceptions.php';
		}

		return true;
	}
	public function actionCreateManager(){
		try{
			$userGroup = User::getUserGroup();

			if(!$userGroup) throw new Exception('Не достаточно прав!');

			$dealerId = User::getUserId();
			$dealerManagers = Dealer::getDealerManagers( $dealerId );
			$userGroup= User::getUserGroup();

			$errors = false;

			// required
			if(isset($_POST["new_dealer_manager_fam"])&&!empty($_POST["new_dealer_manager_fam"])){
				if(preg_match('/^[а-яё\s]*/musi',$_POST["new_dealer_manager_fam"],$res)){
					$new_manager['new_dealer_manager_fam']=$res[0];
				}else{$errors[]="&#34Фамилия&#34 должна состоять только из кириллических символов!"; $new_manager['new_dealer_manager_fam']='';}
			}else{$errors[]="Вы не заполнили поле &#34Фимилия&#34! Обязательное поле!"; $new_manager['new_dealer_manager_fam']='';}

			// required
			if(isset($_POST["new_dealer_manager_name"])&&!empty($_POST["new_dealer_manager_name"])){
				if(preg_match('/^[а-яё\s]*/musi',$_POST["new_dealer_manager_name"],$res)){
					$new_manager['new_dealer_manager_name']=$res[0];
				}else{$errors[]="&#34Имя&#34 должно состоять только из кириллических символов!";$new_manager['new_dealer_manager_name']='';}
			}else{$errors[]="Вы не заполнили поле &#34Имя&#34! Обязательное поле!";$new_manager['new_dealer_manager_name']='';}

			// required
			if(isset($_POST["new_dealer_manager_patr"])&&!empty($_POST["new_dealer_manager_patr"])){
				if(preg_match('/^[а-яё\s]*/musi',$_POST["new_dealer_manager_patr"],$res)){
					$new_manager['new_dealer_manager_patr']=$res[0];
				}else{$errors[]="&#34Отчество&#34 должно состоять только из кириллических символов!";$new_manager['new_dealer_manager_patr']='';}
			}else{$new_manager['new_dealer_manager_patr']=null;}

			// required
			if(isset($_POST["new_dealer_manager_phone"])&&!empty($_POST["new_dealer_manager_phone"])){
				$new_manager['new_dealer_manager_phone'] = preg_replace("/[^0-9]/", '', $_POST["new_dealer_manager_phone"]);
				if(empty($new_manager['new_dealer_manager_phone'])){$errors[] = "Поле &#34Телефон&#34 должен состоять из цифр!";}
			} else { $errors[] = "Вы не заполнили поле &#34Телефон&#34! Обязательное поле!"; $new_manager['new_dealer_manager_phone'] = NULL; }

			// required
			if ( isset ( $_POST["new_dealer_manager_email"] ) ) {
				if ( !empty ( $_POST["new_dealer_manager_email"] ) ) {
					$new_manager['new_dealer_manager_email'] = htmlspecialchars ( trim ( $_POST["new_dealer_manager_email"] ) );
					if ( ! filter_var ( $new_manager['new_dealer_manager_email'], FILTER_VALIDATE_EMAIL ) ) { $errors[] = "Введён не корректный электронный электронный адрес &#34Нового сотрудника&#34"; }
				} else { $errors[] = "Вы не заполнили поле &#34Почта&#34! Обязательное поле!"; $new_manager['new_dealer_manager_email'] = NULL; }
			} else { $errors[] = "Вы не заполнили поле &#34Почта&#34! Обязательное поле!"; $new_manager['new_dealer_manager_email'] = NULL; }

			 if ( $errors == false ) {

				$create_manager = Dealer::createDealerNewManager( $new_manager );
				$dealerId = User::getUserId();
				$dealerManagers = Dealer::getDealerManagers( $dealerId );

				 if ( $create_manager ) {
					$successes [] = 'Новый сотрудник успешно добавлен!';
					$new_manager = array ();
				 } else {
					$errors [] = 'Ошибка! Новый сотрудник не добавлен!';
				 }
			}

			require_once ROOT . '/views/project/add/addPersonal.php';

		}catch(Exception $e){
			require_once ROOT.'/views/site/exceptions.php';
		}

		return true;
	}
	public function actionBeforeQueryAuth(){
		$userGroup = User::getUserGroup();
		try {
			if($userGroup===1||$userGroup===2){
				if( isset($_POST['projectId']) && !empty($_POST['projectId'])){
					if(Site::isJSON($_POST['projectId'])){
						$projectId = json_decode($_POST['projectId'], true);
						$project['project'] = Project::getProjectByQueryAuth($projectId['projectId']);
						$project['receivers'] = Project::getReceiverTools($projectId['projectId'],Project::getReceiversByTools($projectId['projectId']));
						$signature['title'] = "По всем вопросам и пожеланиям просим обращаться:";
						$signature['author'] = "Антон Брагинец";
						$signature['post'] = "к.э.н., руководитель отдела по работе с дилерами ООО &#34;ДС-Мед&#34;";
						$signature['numbers'] = "+7(495)230-11-55, +7(926)637-11-55,";
						$signature['emails'] = "opt@ds-med.ru";
						$project['signature'] = $signature;
						echo json_encode($project,JSON_UNESCAPED_UNICODE);
					} else {
						throw new Exception('Не верный формат данных!');
					}
				} else {
					throw new Exception('Ничего не переданно!');
				}
			} else {
				throw new Exception('Не достаточно прав!');
			}
		}
		catch ( Exception $e) {
			require_once ROOT.'/views/site/exceptions.php';
		}

		return true;
	}
	public function actionQueryAuth(){
		$userGroup = User::getUserGroup();
		try {
			if($userGroup===1||$userGroup===2){
				if(isset($_POST['mails']) && !empty($_POST['mails']) && isset($_POST['projectId']) && !empty($_POST['projectId'])){
					if(Site::isJSON($_POST['mails'])){
						Project::addToSendmail(1, $_POST['projectId'], json_decode($_POST['mails'], true));
					} else {
						throw new Exception('Не верный формат данных!');
					}
				} else {
					throw new Exception('Ничего не переданно!');
				}
			} else {
				throw new Exception('Не достаточно прав!');
			}
		}
		catch ( Exception $e) {
			require_once ROOT.'/views/site/exceptions.php';
		}
		return true;
	}
	public function actionAdd(){
		try {

			$title = 'Добавить проект';

			$userGroup = User::getUserGroup();
			if(!$userGroup) throw new Exception('Не достаточно прав!');

			$toolsList = Project::getToolsList();
			$blocksList = Project::getBlocksList();
			$dealerId = User::getUserId();
			$dealerManagers = Dealer::getDealerManagers( $dealerId );
			$dsmedManagers = Dealer::getDsmedManagers();

			#Белые списоки
			$mas_m_id 		= array();
			$mas_m_phone 	= array();
			$mas_m_email 	= array();

			foreach ( $dealerManagers as $key) {
				$mas_m_id [] = $key['id'];
				$mas_m_phone  [] = preg_replace( '/[^0-9]/', '', $key['phone'] );
				$mas_m_email [] = $key['email'];
			}

			foreach ( $blocksList as $block ) {
				$commonList[$block['id']]['cm_title'] = $block['block_name'];
				#$commonList[$block['id']]['cm_view_diler'] = $block['view'];
			}

			foreach ( $toolsList as $tool ) {
				if($userGroup==1||$userGroup==2||$userGroup==3){
					if($tool['view_all']==1){
						$commonList[$tool['tool_view_block']]['tools'][] = $tool;
						if ( !isset( $commonList[$tool['tool_view_block']]['cm_title'] ) ) {
							$commonList[$tool['tool_view_block']]['cm_title'] = '';
						}
					}
				}else{
					if($tool['view_all']==1){
						if($tool['view_dealer']==1){
							$commonList[$tool['tool_view_block']]['tools'][] = $tool;
							if ( !isset( $commonList[$tool['tool_view_block']]['cm_title'] ) ) {
								$commonList[$tool['tool_view_block']]['cm_title'] = '';
							}
						}
					}
				}

			}

			usort( $commonList, function ( $a, $b ) {
				if ( $a['cm_title'] == $b['cm_title'] ) return 0;
				return ( $a['cm_title'] > $b['cm_title'] ) ? 1 : -1;
			} );

			if ( isset ( $_POST['submit'] ) ) {

				$errors = false;
				$successes = false;

				// required
				if ( isset ( $_POST["clinics-name"] ) ) {
					if ( !empty( $_POST["clinics-name"] ) ) {
						$options['clinics_name'] = htmlspecialchars (trim ( $_POST["clinics-name"] ) );
					} else { $errors[] = "Вы не ввели &#34 Название клиники &#34"; $options['clinics_name'] = NULL; }
				} else { $errors[] = "Вы не ввели &#34 Название клиники &#34"; $options['clinics_name'] = NULL; }

				// no required (select)
				if (isset($_POST["ur_name_type"])){
					if (preg_match('/1|2/',$_POST["ur_name_type"],$res)){
						$options['ur_name_type']=intval($res[0]);
					}else{$options['ur_name_type'] = 1;}
				}else{$errors[] = "Тип ЮЛ: Вы ввели не корректные данные!"; $options['ur_name_type'] = NULL;}


				// required
				if( isset ( $_POST["inn"] ) ) {
					$options['inn'] = preg_replace( '/[^0-9]/', '', $_POST["inn"] );
					if ( empty ( $options['inn'] ) ) { $errors[] = "Вы не ввели &#34 ИНН Юр.лица &#34 или ввели не число!"; $options['inn'] = NULL; }
					else {
						if ( strlen ( $options['inn'] ) < 10 ) {
							$errors[] = "Вы ввели не корректное ИНН, минимальная длина ИНН равна 10 символов!";
						}
					}
				}

				// required
				if ( isset ( $_POST["ur_name"] ) ) {
					if ( !empty ( $_POST["ur_name"] ) ) {
						 $options['ur_name'] = htmlspecialchars ( trim ( $_POST["ur_name"] ) );
					} else { $errors[] = "Вы не ввели &#34 Наименование ЮЛ &#34"; $options['ur_name']= NULL; }
				} else { $errors[] = "Вы не ввели &#34 Наименование ЮЛ &#34"; $options['ur_name'] = NULL; }

				$addresses = $_POST["address"];

				foreach ( $addresses as $key => $address ) {

					// no required
					if ( isset ( $address['region'] ) ) {
						if ( !empty ( $address['region'] ) ) {
							$options['addresses'][$key]['region'] = htmlspecialchars ( trim ( $address['region'] ) );
						} else { $options['addresses'][$key]['region'] = NULL; }
					} else { $options['addresses'][$key]['region'] = NULL; }

					//required
					if ( isset ( $address['city'] ) ) {
						if ( !empty ( $address['city'] ) ) {
							$options['addresses'][$key]['city']  = htmlspecialchars ( trim ( $address['city'] ) );
						} else { $errors[] = "Вы не ввели &#34 Название города &#34"; $options['addresses'][$key]['city'] = NULL; }
					} else { $errors[] = "Вы не ввели &#34 Название города &#34"; $options['addresses'][$key]['city'] = NULL; }

					//required
					if ( isset ( $address['street'] ) ) {
						if ( !empty ( $address['street'] ) ) {
							$options['addresses'][$key]['street']  = htmlspecialchars ( trim ( $address['street'] ) );
						} else { $errors[] = "Вы не ввели &#34 Название улицы &#34"; $options['addresses'][$key]['street'] = NULL; }
					} else { $errors[] = "Вы не ввели &#34 Название улицы &#34"; $options['addresses'][$key]['street'] = NULL; }

					//required
					if ( isset ( $address['house'] ) ) {
						if ( !empty ( $address['house'] ) ) {
							$options['addresses'][$key]['house']  = htmlspecialchars ( trim ( $address['house'] ) );
						} else { $errors[] = "Вы не ввели &#34 Номер дома &#34"; $options['addresses'][$key]['house'] = NULL; }
					} else { $errors[] = "Вы не ввели &#34 Номер дома &#34"; $options['addresses'][$key]['house'] = NULL; }

					// no required
					if ( isset ( $address['office'] ) ) {
						if ( !empty ( $address['office'] ) ) {
							$options['addresses'][$key]['office'] = htmlspecialchars ( trim ( $address['office'] ) );
						} else { $options['addresses'][$key]['office'] = NULL; }
					} else { $options['addresses'][$key]['office'] = NULL; }

				}

				// no required
				if ( isset ( $_POST["comment"] ) ) {
					if ( !empty ( $_POST["comment"] ) ) {
						$options['comment'] = htmlspecialchars ( trim ( $_POST["comment"] ) );
					} else { $options['comment'] = NULL; }
				} else { $options['comment'] = NULL; }

				// required
				if( isset ( $_POST["tools"] ) ) {
					if ( empty ( $_POST["tools"] ) ) {
						$errors[] = 'Не выбрано ни одного оборудования'; $options['tools'] = NULL;
					} else { $options['tools'] = $_POST["tools"]; }
				} else { $errors[] = 'Не выбрано ни одного оборудования'; $options['tools'] = NULL; }

				//
				// DEALER
				//

				// no required (select)
				if ( isset ( $_POST["select-personal"] ) ) {
					if ( !empty ( $_POST["select-personal"] ) ) {
						$options['manager-id'] = preg_replace('/[^0-9]/', '', $_POST["select-personal"]);
						if ( ! in_array ( $options['manager-id'], $mas_m_id ) ) {
						$errors[] = "Вы заполнили поле &#34ФИО Сотрудника&#34 не корректно!"; $options['manager-id'] = NULL; }
					} else { $options['manager-id'] = NULL; }
				} else { $errors[] = "Вы не заполнили  поле &#34ФИО Сотрудника&#34!"; $options['manager-id'] = NULL; }

				 // required
				 if ( isset ( $_POST["manager_phone"] ) ) {
					 $dealer_manager['manager_phone'] = preg_replace('/[^0-9]/', '', $_POST["manager_phone"]);
					 if ( ! empty ( $dealer_manager['manager_phone'] ) ) {
						 if ( ! in_array ( $dealer_manager['manager_phone'], $mas_m_phone ) ) {
							 $errors[] = "Вы заполнили поле &#34Телефон&#34 не корректно!"; $dealer_manager['manager_phone'] = NULL; }
					 } else { $errors[] = "Вы не заполнили поле &#34Телефон&#34! Обязательное поле!"; $dealer_manager['manager_phone'] = NULL; }
				 } else { $errors[] = "Вы не заполнили поле &#34Телефон&#34! Обязательное поле!"; $dealer_manager['manager_phone'] = NULL; }

				 // required
				 if ( isset ( $_POST["manager_email"] ) ) {
					 $dealer_manager['manager_email'] = htmlspecialchars ( trim ( $_POST["manager_email"] ) );
					 if ( ! empty ( $dealer_manager['manager_email'] ) ) {
						 if ( ! in_array ( $dealer_manager['manager_email'], $mas_m_email ) ) {
						 $errors[] = "Вы заполнили поле &#34Почта&#34 не корректно!"; $dealer_manager['manager_email'] = NULL; }
					 } else { $errors[] = "Вы не заполнили поле &#34Почта&#34! Обязательное поле!"; $dealer_manager['manager_email'] = NULL; }
				 } else { $errors[] = "Вы не заполнили поле &#34Почта&#34! Обязательное поле!"; $dealer_manager['manager_email'] = NULL; }

				$options['dealer-id'] = User::getUserId();

				 $secret = "6LelfWYUAAAAAAOlC17gCXWev-feLqK1YAn9HXZy";
				 $response = null;

				 if(isset($_POST['g-recaptcha-response'])){

					$recaptcha = new \ReCaptcha\ReCaptcha ( $secret );
					$response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

					if($errors == false){
						if ($response->isSuccess()){
							if($dealerId){
								$project = Project::createProject($options);
								$manager = Dealer::getDealerOneManager( $options['manager-id'] );
								if($project){
									$successes [] = 'Авторизация успешно добавлена!';
									$ids = implode(',', $options['tools']);
									$trList = Project::getToolsReceiversListByToolsIds( $ids );
									$receivers = [];
									foreach($trList as $trItem){
										if (!array_key_exists($trItem['email'], $receivers)){
											$receivers[$trItem['email']]['fio'] = $trItem['fio'];
											$receivers[$trItem['email']]['send_table'] = $trItem['send_table'];
											$receivers[$trItem['email']]['send_word'] = $trItem['send_word'];
											$receivers[$trItem['email']]['send_email'] = $trItem['send_email'];
										}
										$receivers[$trItem['email']]['tools'][] = $trItem['tool_name'];
										$receivers[$trItem['email']]['receivers_id'] = $trItem['tool_send_by'];
									}
									if(!empty($receivers)){
										$adr_string = $tool_string = '';
										foreach ( $options['addresses'] as $address ){
											$adr_string .= ( !empty ( $adr_string ) ? "<br>" : "" ).
														   ( empty ( $address['region'] ) ? '' : $address['region'] ).
														   ( empty ( $address['region'] ) ? '' : ',' ).
														   ' '.$address['city'].
														   ( empty ( $address['city'] ) ? ' ' : ', улица ' ).
														   $address['street'].', дом '.$address['house'].
														   ( !empty ( $address['office'] ) ? ', офис '. $address['office'] : '' );
										}
										foreach($receivers as $email => $receiver){
											$tool_string = '';
											if($receiver['send_table']){
												foreach ($receiver['tools'] as $tool){
													$tool_string .= (!empty($tool_string) ? "\n" : "").$tool;
											   	}
												$attachment[0] = [
													'date'=>date('d.m.Y'),
													'brend_name' => str_replace( '&quot;', '"', $options['clinics_name']),
													'ur_name'=>$options['ur_name'],
													'inn' => $options['inn'],
													'dealer_brend_name'=> (User::getCurrentUser()['dealer_brend_name']?:''),
													'dealer_ur_name' => (User::getCurrentUser()['dealer_ur_name']?:''),
													'address'=>str_replace('<br>', "\n", $adr_string),
													'manager' => $manager['name'].(!empty($manager['second_name'])?$manager['second_name']:''),
													'type-deal' => $options['ur_name_type'],
													'tools'=>$tool_string,
												];
												$file = Site::createTable($attachment);
										   	}
										   if($receiver['send_word']){
											   # ID Феникса (У этой компании мы бронируем оборудование)
											   if($receiver['receivers_id']==5){
												   $bronsList = Site::getBronsByPhoenixmed($receiver['receivers_id']);
												   $file = Site::createWordByPhoenixmed($bronsList);
											   }
										   }

										   // if($userGroup==1||$userGroup==2||$userGroup==3){
											//    # Выводить имя ресивера если бронирует сотрудник Ds-Med
											//    $header_messege = (!empty($receiver['fio']) ? $receiver['fio'].', з':'З').'дравствуйте!<br><br>';
										   // }else{
											//    # Дилер
											//    $header_messege = 'Антон Николаевич, здравствуйте!<br><br>';
										   // }
										   
										   $messege = 'Доброго времени суток, '.
										   		( ! empty ( $manager['second_name'] ) ? ucfirst ( $manager['second_name'] ) . ' ' : '' ) . ucfirst ( $manager['name'] ).
										   		'!<br>Ваше письмо с запросом авторизации на проект поставки оборудования  отправлен в компанию ООО ДС-Мед.<br>
												Еще раз проверьте список оборудования и данные ЛПУ:<br><br>'
											   .implode( '<br>', $receiver['tools'] ).'<br><br>
											   Оборудование планируется к поставке в '.str_replace(' ()', '' , $options['clinics_name'].' '.'('.$options['ur_name'].')').', адрес(а):<br>
											   '.$adr_string.'<br>
											   ИНН Юр. лица: '.$options['inn'].'<br><br>
											   Ответ на запрос Вы получите в течение 24 часов!<br>
											   Если Вы увидели ошибку в запросе или по прошествии 24 часов Вы не получили ответ - просьба позвонить по телефону: +7(926)637-11-55.<br>
											   <br>
											   --<br>
											   С уважением к Вашей работе, компания ООО ДС-Мед!<br>
											   Отзывы врачей, технические обзоры оборудования и многое другое Вы можете посмотреть на нашем канале <a href="https://www.youtube.com/channel/UCRlotceWpoJYIBjkdJWZRmA">YouTube</a>.';

										   $send = Site::sendMail($userGroup,$dealer_manager['manager_email'],$email,$messege,$file,$receiver['receivers_id']);
										   # Если не false то метод вернул массив с ошибками.
										   if($send){
											   $errors[]='Письмо, с оборудованием ('.$attachment[0]['tools'].') не было отправлено!<br> Код ошибки: PC::Add(S::Sendmail)';
										   }
									   }
									}
									$options = array ();
									$dealer_manager = array ();
									$addresses = null;
								}else{$errors[]='Не удалось добавить проект';}
							}else{$errors[]='Не удалось определить пользователя';}
						}else{
							$errors[]='Не верная ReCaptcha';
							error_log("[".date("m.d.y")."-".date("H:i:s")."][PROJECT::ADD->ReCaptcha] Ошибка валидации ReCaptcha!\nGoogleReCaptchaERROR: ".implode($response->getErrorCodes())."\n\n", 3, ROOT."/log/ReCaptcha.log");
						}
					}
				 }else{
				   $errors[]='ReCaptch global problem!';
				   error_log("[".date("m.d.y")."-".date("H:i:s")."][PROJECT/ADD->ReCaptcha] Ошибка валидации ReCaptcha!\nGoogleReCaptchaERROR: ".implode($response->getErrorCodes())."\n\n", 3, ROOT."/log/ReCaptcha.log");
				 }
			}
			require_once ROOT . '/views/project/add/add.php';

		}catch(Exception $e){
			require_once ROOT.'/views/site/exceptions.php';
		}

		return true;
	}
	public function actionChangeStatus(){

		$userGroup= User::getUserGroup();

		try {

			if ( isset($_POST['authorizations']) && !empty($_POST['authorizations']) ) {

				if ( empty($_POST['authorizations']) || ! Site::isJSON($_POST['authorizations']) )
					throw new Exception('Не верный формат данных!' );

				$authorizations = json_decode($_POST['authorizations'], true);

				Project::changeProjectStatus( $authorizations );

				$bronsByProject = Project::getBronsByProject( $authorizations['projectId'] );
				require_once ROOT.'/views/project/list/listBronsAfterChangeStatus.php';
			}
		}
		catch ( Exception $e) {

			$bronsByProject = Project::getBronsByProject( $authorizations['projectId'] );
			require_once ROOT.'/views/project/list/listBronsAfterChangeStatus.php';
			require_once ROOT.'/views/site/exceptions.php';
		}

		return true;
	}
	public function actionChangeResponsible () {
		$userGroup = User::getUserGroup();

		try {

			if ( $userGroup === 1 || $userGroup === 2 ) {
				if( isset ( $_POST['responsible'] ) && ! empty ( $_POST['responsible'] ) && isset ( $_POST['projectId'] ) && ! empty ( $_POST['projectId'] ) ) {

					preg_match ( '/[0-9]*/',$_POST['responsible'], $responsibleId ) ? intval ( $responsibleId = $responsibleId[0] ) : $responsibleId = false;
					preg_match ( '/[0-9]*/',$_POST['projectId'], $projectId ) ? intval ( $projectId = $projectId[0] ) : $projectId = false;

					if ( ! $responsibleId || ! $projectId ) throw new Exception('Переданы не корректные данные!');

						$responsibleId = Project::updateProjectResponsible ( $projectId, $responsibleId );
						$dsmedManager = Dealer::getDsmedManager ( $responsibleId );

					require_once ROOT.'/views/project/list/responsible.php';

				} else {
					throw new Exception('Ничего не переданно!');
				}
			} else {
				throw new Exception('Не достаточно прав!');
			}
		}
		catch ( Exception $e) {
			header("HTTP/1.0 415 Unsupported Media Type");
			require_once ROOT.'/views/site/exceptions.php';
		}
		return true;
	}
	# Смена менеджера проекта
	public function actionChangeProjectManager () {
		$userGroup = User::getUserGroup();

		try {

			if ( $userGroup ) {
				if( isset ( $_POST['selected_new_manager'] ) && ! empty ( $_POST['selected_new_manager'] )  && isset ( $_POST['projectId'] ) && ! empty ( $_POST['projectId'] ) ) {

					preg_match ( '/[0-9]*/',$_POST['selected_new_manager'], $newManagerId ) ? intval ( $newManagerId = $newManagerId[0] ) : $newManagerId = false;
					preg_match ( '/[0-9]*/',$_POST['projectId'], $projectId ) ? intval ( $projectId = $projectId[0] ) : $projectId = false;

					if ( ! $newManagerId || ! $projectId) throw new Exception('Переданы не корректные данные!');
						
						$dealerId = User::getUserId();
						$managerId = Project::updateProjectManager ( $projectId, $dealerId, $newManagerId );
						$dealerManager = Dealer::getDealerOneManager ( $managerId );

					require_once ROOT.'/views/project/list/new_manager.php';

				} else {
					throw new Exception('Ничего не переданно!');
				}
			} else {
				throw new Exception('Не достаточно прав!');
			}
		}
		catch ( Exception $e) {
			header("HTTP/1.0 415 Unsupported Media Type");
			require_once ROOT.'/views/site/exceptions.php';
		}
		return true;
	}

}
