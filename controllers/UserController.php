<?php

/**
* UserController
*/

class UserController {

	public function actionAuthorize(){
		$login = '';
		$password = '';
		$passwordRequired = false;
		$userGroup = false;

		if (isset($_POST['submit'])) {

			$login = $_POST['login'];
			$password = $_POST['password'];

			$errors = false;

			if (!empty($login)) {
				if (!empty($password)) {

					$user = User::checkUserData($login, $password);
					if ($user == false) {

						$errors[] = 'Неверные учетные данные';
					}
				}else{
					$errors[] = 'Введите пароль';
					$passwordRequired = true;
				}
				if ($errors == false) {
					User::auth($user);
					$userGroup = User::getUserGroup();
					if ( ! DEVELOPE_MODE || ( $userGroup == 1 ? true : false ) ) {

						$dealerId = User::getUserId();
						$token = Api::createJWT($dealerId);
						header("Authorization: bearer $token");
						header("Location: /");
					} else {
						$errors[] = 'Идёт обноаление сайта!';
						unset($_SESSION['user']);
					}
				}
			}else{
				$errors[] = 'Введите логин';
			}
		}
		$title='Система авторизаций: авторизация';
		require_once ROOT . '/views/user/login.php';
		return true;
	}

	public function actionLogout(){

		unset($_SESSION['user']);
		header("Location: /");
		return true;
	}
}
