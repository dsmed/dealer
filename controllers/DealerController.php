<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
* DealerController
*/
class DealerController{
    # Ajax проверка введённого e-mail/логина
    public function actionDealerCheckEmail(){
        $userGroup = User::getUserGroup();
        if($userGroup===1||$userGroup===2){
            if(isset($_POST['login'])&&!empty($_POST['login'])){
                $checkLogin = htmlspecialchars(trim($_POST['login']));
                if(Site::dealerCheckEmail($checkLogin)){

                    echo '<div class="dealer-email-error">Компания с таким e-mail уже существует!</div>';
                }
            }
        }else{
            echo'Ошибка доступа!';
            error_log("[".date("m.d.y")."-".date("H:i:s")."][DealerController::actionCheckLogin] Ошибка доступа!", 3, ROOT."/log/rights.log");
        }
        return true;
    }
    public function actionAdd(){
        try{
            $userGroup = User::getUserGroup();
            if($userGroup !== 1 && $userGroup !== 2) throw new Exception('Не достаточно прав!');
            $dealerId = User::getUserId();
			$dealerManagers = Dealer::getDealerManagers( $dealerId );
            $dsmedManagers = Dealer::getDsmedManagers();

            if(isset( $_POST['submit'])){

                $errors = false;

                //
                // DEALER
                //

                // No required
                if ( isset ( $_POST["dealer_brend_name"] ) ) {
                    if ( !empty ( $_POST["dealer_brend_name"] ) ) {
                        $options['dealer_brend_name'] = htmlspecialchars( strip_tags ( trim ( $_POST["dealer_brend_name"] ) ) );
                    } else{ $options['dealer_brend_name'] = NULL; }
                } else { $options['dealer_brend_name'] = NULL; }

                // required
                if ( isset ( $_POST["dealer_ur_name"] ) ) {
                    if ( !empty ( $_POST["dealer_ur_name"] ) ) {
                        $options['dealer_ur_name'] = htmlspecialchars( strip_tags ( trim ( $_POST["dealer_ur_name"] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Юридическое лицо&#34! Обязательное поле!"; $options['dealer_ur_name'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Юридическое лицо&#34! Обязательное поле!"; $options['dealer_ur_name'] = NULL; }

                // required
                if( isset( $_POST["dealer_inn"] ) ) {
                    $options['dealer_inn'] = preg_replace('/[^0-9]/', '', $_POST["dealer_inn"]);
                    if ( empty ( $options['dealer_inn'] ) ) { $errors[] = "Вы не ввели &#34 ИНН Юр.лица &#34 или ввели не число!"; $options['dealer_inn'] = NULL; }
                    else {
                        if ( strlen ( $options['dealer_inn'] ) < 10 ) {
                            $errors[] = "Вы ввели не корректное ИНН, минимальная длина ИНН равна 10 символов!";
                        }
                    }
                }

                $address = $_POST["address"];

                // required
                if ( isset( $address['region'] ) ) {
                    if ( !empty ( $address['region'] ) ) {
                        $options['region'] = htmlspecialchars( strip_tags ( trim ( $address['region'] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Регион&#34! Обязательное поле!"; $options['region'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Регион&#34! Обязательное поле!"; $options['region'] = NULL; }

                // required
                if ( isset ( $address['city'] ) ) {
                    if ( !empty ( $address['city'] ) ) {
                        $options['city'] = htmlspecialchars( strip_tags ( trim ( $address['city'] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Город&#34! Обязательное поле!"; $options['city'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Город&#34! Обязательное поле!"; $options['city'] = NULL; }

                // required
                if ( isset ( $address['street'] ) ) {
                    if ( !empty ( $address['street'] ) ) {
                        $options['street'] = htmlspecialchars( strip_tags ( trim ( $address['street'] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Улица&#34! Обязательное поле!"; $options['street'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Улица&#34! Обязательное поле!"; $options['street'] = NULL; }

                // no required
                if ( isset ( $address['house'] ) ) {
                    if ( !empty ( $address['house'] ) ) {
                        $options['house'] = htmlspecialchars( strip_tags ( trim ( $address['house'] ) ) );
                    } else { $options['house'] = NULL; }
                } else { $options['house'] = NULL; }

                // no required
                if ( isset ( $address['block'] ) ) {
                    if ( !empty ( $address['block'] ) ) {
                        $options['block'] = htmlspecialchars( strip_tags ( trim ( $address['block'] ) ) );
                    } else { $options['block'] = NULL; }
                } else { $options['block'] = NULL; }

                // no required
                if ( isset ( $address['office'] ) ) {
                    if ( !empty ( $address['office'] ) ) {
                        $options['office'] = htmlspecialchars( strip_tags ( trim ( $address['office'] ) ) );
                    } else { $options['office'] = NULL; }
                } else { $options['office'] = NULL; }

                // no required
                if ( isset ( $_POST["dealer_type"] ) ) {
                    if ( !empty ( $_POST["dealer_type"] ) ) {
                        $options['dealer_type'] = htmlspecialchars( strip_tags ( trim ( $_POST["dealer_type"] ) ) );
                    } else { $options['dealer_type'] = NULL; }
                } else { $options['dealer_type'] = NULL; }


                //
                // DEALER CONTACTS
                //

                // required
                if ( isset ( $_POST["dealer_phone"] ) ) {
                    if ( !empty ( $_POST["dealer_phone"] ) ) {
                        $options['dealer_phone'] = preg_replace("/[^0-9]/", '', $_POST["dealer_phone"]);
                    } else { $errors[] = "Вы не заполнили поле &#34Телефон телефон&#34! Обязательное поле!"; }
                } else { $errors[] = "Вы не заполнили поле &#34Телефон телефон&#34! Обязательное поле!"; }

                // required
                if(isset($_POST["dealer_email"])&&!empty($_POST["dealer_email"])){
                    $options['dealer_email'] = htmlspecialchars(trim($_POST["dealer_email"]));
                    if(Site::dealerCheckEmail($options['dealer_email'])){
                        $errors [] = "Компания с таким e-mail уже существует!";
                    }
                }else{ $errors[] = "Вы не заполнили поле &#34Почта&#34! Обязательное поле!"; $options['dealer_email'] = NULL; }

                // no required
                if ( isset ( $_POST["dealer_site"] ) ) {
                    if ( !empty ( $_POST["dealer_site"] ) ) {
                        $options['dealer_site'] = htmlspecialchars(trim($_POST["dealer_site"]));
                    } else { $options['dealer_site'] = NULL; }
                } else { $options['dealer_site'] = NULL; }

                // no required
                if ( isset ( $_POST['dealer_comment'] ) ) {
                    if ( !empty ( $_POST['dealer_comment'] ) ) {
                        $options['dealer_comment'] = htmlspecialchars ( trim ( $_POST['dealer_comment'] )  );
                    } else { $options['dealer_comment'] = NULL; }
                } else { $options['dealer_comment'] = NULL; }

                //
                // MANAGER
                //

                // required
                if ( isset ( $_POST["dealer_manager_fam"] ) ) {
                    if ( !empty ( $_POST["dealer_manager_fam"] ) ) {
                        $options['dealer_manager_fam'] = htmlspecialchars ( strip_tags ( trim ( $_POST["dealer_manager_fam"] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Фамилия(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_fam'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Фамилия(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_fam'] = NULL; }

                // required
                if ( isset ( $_POST["dealer_manager_name"] ) ) {
                    if ( !empty ( $_POST["dealer_manager_name"] ) ) {
                        $options['dealer_manager_name'] = htmlspecialchars ( strip_tags ( trim ( $_POST["dealer_manager_name"] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Имя(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_name'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Имя(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_name'] = NULL; }

                // no required
                if ( isset ( $_POST["dealer_manager_patr"] ) ) {
                    if ( !empty ( $_POST["dealer_manager_patr"] ) ) {
                        $options['dealer_manager_patr'] = htmlspecialchars ( strip_tags ( trim ( $_POST["dealer_manager_patr"] ) ) );
                    } else { $options['dealer_manager_patr'] = NULL; }
                } else { $options['dealer_manager_patr'] = NULL; }

                // required
                if ( isset ( $_POST["dealer_manager_phone"] ) ) {
                    if ( !empty ( $_POST["dealer_manager_phone"] ) ) {
                        $options['dealer_manager_phone'] = htmlspecialchars ( strip_tags ( trim ( $_POST["dealer_manager_phone"] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Телефон(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_phone'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Телефон(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_phone'] = NULL; }

                // required
                if ( isset ( $_POST["dealer_manager_email"] ) ) {
                    if ( !empty ( $_POST["dealer_manager_email"] ) ) {
                        $options['dealer_manager_email'] = htmlspecialchars ( strip_tags ( trim ( $_POST["dealer_manager_email"] ) ) );
                    } else { $errors[] = "Вы не заполнили поле &#34Почта(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_email'] = NULL; }
                } else { $errors[] = "Вы не заполнили поле &#34Почта(Менеджер)&#34! Обязательное поле!"; $options['dealer_manager_email'] = NULL; }

                /*
                * Конец проверки
                */

                if ( $errors === false ) {
                    
                    $_SESSION['add-dealed-succsess'] = true;
                    // Передаётся почта для формирования логина

                    $login = $options['dealer_email'];
                    // Передаётся длинна Password
                    $password = Dealer::createPassword ( 10 );
                    error_log("Пароль - $password"."\n\n", 3, ROOT."/log/proverka-password.log");
                    $create_dealer = Dealer::createDealer( $login, $password, $options );

                    if ( $create_dealer ) {

                        $successes [] = 'Дилер и Менеджер успешно добавлены в базу!';

                        $text = '
                        <h2>Добрый день, уважаемые партнеры!</h2>
                        <p>
                            Для упрощения добавления проектов по поставляемому нашей компанией оборудованию предлагаем воспользоваться новой <b>«Системой авторизации проектов»</b>. Надеемся, это сэкономит Ваше драгоценное время.
                        </p>
                        <p>
                            <ul>
                                <li>Для внесения проекта в базу авторизации просим перейти в <a href="https://dealer.dsmed.ru" title="Система авторизации проектов">систему</a></li>
                                <li>Для входа в систему необходимо ввести логин и пароль, приведенные ниже</li>
                                <li>Затем в верхней навигационной панели нажать кнопку <i>"Добавить"</i> и выбрать <i>"Проект"</i></li>
                                <li>После чего необходимо выбрать сотрудника Вашей компании, который будет работать с проектом (<i>справа Компания -> Сотрудник</i>), внести необходимые данные по проекту и отметить оборудование</li>
                                <li>По завершении вышеуказанного проходим проверку <i>"reCAPTCHA"</i> и нажимаем кнопку <i>"Отправить авторизации"</i></li>
                            </ul>
                        </p>
                        <p>
                            Вам на почту придет письмо-подтверждение о том, что Вы отправили проект на проверку.
                        </p>
                        <p>
                            В течение 24 часов наш сотрудник пришлет информацию о статусе проекта, а также коммерческое предложение на запрошенное оборудование.
                        </p>
                        <p>
                            <b>Логин:</b>'.$login.'<br>
                            <b>Пароль:</b>'.$password.'<br>
                        </p>
                        <p>
                            ---<br>
                            По всем вопросам и пожеланиям просим обращаться:<br>
                            Антон Брагинец<br>
                            к.э.н., руководитель отдела по работе с дилерами ООО "ДС-Мед"<br>
                            +7(495)230-11-55, +7(926)637-11-55,<br>
                            opt@ds-med.ru
                        </p>';

                        require_once ( ROOT.'/vendor/autoload.php' );
                        require_once ( ROOT.'/config/mail_config.php' );

                        $mail = new PHPMailer ( true );

                        try {
                            $mail->isSMTP();
                            $mail->Host = $senders['beget']['host'];
                            $mail->SMTPAuth = true;
                            $mail->Username = $senders['beget']['user'];
                            $mail->Password = $senders['beget']['pass'];
                            $mail->SMTPSecure = 'ssl';
                            $mail->Port = 465;
                            $mail->CharSet = 'UTF-8';
                            $mail->setFrom ( $senders['beget']['user'], 'Отдел по работе с дилерами ООО "ДС-Мед"' );
                            $mail->addReplyTo( $senders['opt']['user'] , 'Отдел по работе с дилерами ООО "ДС-Мед"' );
                            $mail->addBCC ( $senders['beget']['user'] );
                            $mail->addBCC ( $senders['opt']['user'] );
                            $mail->isHTML ( true );
                            $mail->Subject = 'Добавление в систему бронирования ООО "ДС-Мед"';
                            $mail->Body = $text;
                            $mail->send();

                            $successes [] = 'Письмо отправлено дилеру!';
                            // Обнуляем для формы.
                            $options = array ();
                        } catch (  Exception $e  ) {
                            $errors [] = 'Письмо не отправлено дилеру! Код ошибки: D_Send_Email.';
                            error_log( "[ ".date("m.d.y")."-".date("H:i:s")." ]
                                        [PROJECT/ADD] Ошибка отправки почты!\n
                                        Письмо не отправлено!\n
                                        PHPMailerERROR: ".$mail->ErrorInfo."\n\n", 3, ROOT."/log/email-error.log");
                        }
                    } else {
                        $errors [] = 'Дилер или Менеджер не добавлены в базу! Код ошибки: D_M_Insert_Dealer.';
                    }
                }
                else {
                    $_SESSION['add-dealed-error'] = $errors;
                }
            }
            require_once ROOT . '/views/dealer/add.php';

        }catch(Exception $e){
            require_once ROOT.'/views/site/exceptions.php';
        }

        return true;
    }
}
