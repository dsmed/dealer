<?php

/**
* SiteController
*/

class SiteController {

	function actionIndex(){

		$userGroup= User::getUserGroup();
		$dealerId = User::getUserId();
		$dealerManagers = Dealer::getDealerManagers( $dealerId );
		$dsmedManagers = Dealer::getDsmedManagers();

		if ( ! DEVELOPE_MODE || ( $userGroup === 1? true : false ) ) {
			require_once ROOT . '/views/site/index.php';
		} else {
			require_once ROOT . '/develop/index.php';
		}

		return true;
	}

	function actionNotfound(){

		if ( ! DEVELOPE_MODE || ( $userGroup === 1 ? true : false ) ) {
			require_once ROOT . '/views/site/404.php';
		} else {
			require_once ROOT . '/develop/index.php';
		}

		return true;
	}
}
