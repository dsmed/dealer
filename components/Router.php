<?php

/**
* Router
*/

class Router {
	private $routes;
	function __construct(){
		$routesPath = ROOT.'/config/routes.php';
		$this->routes = include($routesPath);
	}
	/**
	 * Returns request string
	 */
	private function getURI(){
		if (!empty($_SERVER['REQUEST_URI'])) {
			return trim($_SERVER['REQUEST_URI'], '/');
		}
	}
	function run(){
		$uri = $this->getURI();
		$userGroup= User::getUserGroup();
		foreach ($this->routes as $uriPattern => $path) {
			if (preg_match("~$uriPattern~", $uri)) {
				$internalRoute = preg_replace("~$uriPattern~", $path, $uri);
				// Масссив разбитой ссылки Пример: из user/authorize
				// Параметры для передачи их в метод.
				$segments = explode('/', $internalRoute);
				// Хранит имя контроллера
				$controllerName = array_shift($segments).'Controller';
				// Устанавливает верхний регистр первой букве в названии контроллера
				$controllerName = ucfirst($controllerName);
				// Хранит имя вызываемой функции в контроллере
				$actionName = 'action'.ucfirst(array_shift($segments));

				$parameters = $segments;
				$controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

				if (file_exists($controllerFile)) {
					include_once($controllerFile);
				}

				$controllerObject = new $controllerName;
				if (!method_exists($controllerName, $actionName)) {
					header("HTTP/1.0 404 Not Found");
					include ROOT . '/views/site/404.php';
					return false;
				}

				// session_start();
				if (!isset($_SESSION['user'])) {
					if ($actionName!='actionAuthorize'&&$actionName!='actionLogout') {
						header("Location: /login");
					
					}
				}
				if ( DEVELOPE_MODE ) {
					if ( $userGroup == 1 ){
						$result = call_user_func_array(array($controllerObject, $actionName), $parameters);
					} else {
						$result = true;
						require_once ROOT.'/develop/index.php';
					}
				} else {
					$result = call_user_func_array(array($controllerObject, $actionName), $parameters);
				}
				if ($result != null) {
					break;
				}
			}
		}
	}
}
